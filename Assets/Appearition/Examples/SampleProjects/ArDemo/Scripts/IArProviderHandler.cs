﻿using System.Collections;
using UnityEngine;

namespace Appearition.Example
{
    /// <summary>
    /// Template for an Ar Handler, in charge of interfacing the basic utilities for an Ar Provider's SDK.
    /// </summary>
    public interface IArProviderHandler
    {
        #region Initialization 

        /// <summary>
        /// Setup the license of the ArProvider, if need be.
        /// </summary>
        /// <returns></returns>
        IEnumerator SetupLicense();

        #endregion

        #region Functionality 

        /// <summary>
        /// Initialize or De-Initialize.
        /// </summary>
        /// <param name="shouldBeEnabled"></param>
        void ChangeArProviderActiveState(bool shouldBeEnabled);

        /// <summary>
        /// Start or stop scanning.
        /// </summary>
        /// <param name="shouldScan"></param>
        void ChangeScanningState(bool shouldScan);

        /// <summary>
        /// If any target is currently being tracked, removes it and clears it. Additionally, can start scanning again.
        /// </summary>
        /// <param name="shouldRestartScanning">If set to <c>true</c> should restart scanning.</param>
        void ClearTargetsBeingTracked(bool shouldRestartScanning = true);

        /// <summary>
        /// Refocus the camera manually. Do note that most ArProviders achieve this automatically.
        /// </summary>
        void RefocusCamera();

        #endregion

        #region Accessibility

        Camera ProviderCamera { get; }

        float ScaleMultiplier { get; }

        #endregion

        #region Flags

        /// <summary>
        /// Defines whether or not the ArProvider is currently scanning.
        /// </summary>
        bool IsScanning { get; }

        /// <summary>
        /// Defines whether the ArProvider is currently initialized or not.
        /// </summary>
        bool IsArProviderInitialized { get; }

        /// <summary>
        /// Whether or not the current ArProvider is enabled.
        /// </summary>
        bool IsArProviderActive { get; }

        bool DisableScanningOnTargetFound { get; }

        bool UseExtendedTracking { get; }

        #endregion
    }
}