﻿using Appearition.API;

namespace Appearition.ArTargetImageAndMedia.API
{
    public class ArTarget_UnlinkArImage : BaseApiPost
    {
        //Variables
        public string Data;

        //Request Params
        [System.Serializable]
        public class RequestContent : BaseRequestContent
        {
            public int arTargetId;
            public int arImageId;
        }
    }
}