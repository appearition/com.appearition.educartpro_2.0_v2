﻿#pragma warning disable 0649

using System.Collections;
using System.Linq;
using Vuforia;
using UnityEngine;
using Appearition.ArTargetImageAndMedia;
using UnityEditor;
using Appearition.ArTargetImageAndMedia.API;
//using System.Diagnostics;

namespace Appearition.Example
{
    #if UNITY_2017
    [RequireComponent(typeof(CloudRecoBehaviour))]
    public class VuforiaCloudProviderHandler : MonoBehaviour, ICloudRecoEventHandler, IArProviderHandler
    {
        //Internal Variables
        public bool alwaysCheckForInitStateChange = true;

        /// <summary>
        /// Timer keeping track of when the Scanning mode should be disabled, if feature toggled in the AppearitionArHandler.
        /// </summary>
        float disableScanTimer;

        //Handy Properties
        private CloudRecoBehaviour _cloudRecoB;

        public CloudRecoBehaviour CloudRecoB
        {
            get
            {
                if (_cloudRecoB == null)
                    _cloudRecoB = GetComponent<CloudRecoBehaviour>();
                return _cloudRecoB;
            }
        }


        private ObjectTracker _objectTracker;

        public ObjectTracker ObjectTracker
        {
            get { return _objectTracker ?? (_objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>()); }
        }

        private bool _isScanning;

        /// <summary>
        /// Whether Vuforia is currently scanning a target or not.
        /// </summary>
        public bool IsScanning
        {
            get { return _isScanning; }
            protected set
            {
                _isScanning = value;

                if (AppearitionArHandler.Instance.disableScanningModeOverTime)
                    disableScanTimer = AppearitionArHandler.Instance.disableScanningModeDelay;

                //Tell the AppearitionArHandler about it!
                AppearitionArHandler.Instance.ArProviderScanStateChanged(_isScanning);
            }
        }

        private bool _isArProviderInitialized;

        /// <summary>
        /// Whether Vuforia, along with its relevant components, is initialized.
        /// </summary>
        public bool IsArProviderInitialized
        {
            get
            {
                bool oldValue = _isArProviderInitialized;

                //Check the new value
                _isArProviderInitialized = VuforiaManager.Instance.Initialized;

                if (oldValue != _isArProviderInitialized)
                    AppearitionArHandler.Instance.ArProviderInitializationStateChanged(_isArProviderInitialized);

                return _isArProviderInitialized;
            }
        }

        /// <summary>
        /// Whether Vuforia's engine is active or not.
        /// </summary>
        public bool IsArProviderActive
        {
            get { return VuforiaBehaviour.Instance != null && VuforiaBehaviour.Instance.enabled; }
        }

        void Awake()
        {
            CloudRecoB.RegisterEventHandler(this);
        }

        void Update()
        {
            if (IsScanning && AppearitionArHandler.Instance.disableScanningModeOverTime)
            {
                if (disableScanTimer <= 0)
                    ChangeScanningState(false);
                else
                    disableScanTimer -= Time.deltaTime;
            }
        }

        #region Init 

        public void OnInitError(TargetFinder.InitState initError)
        {
            Debug.LogError("Init error: " + initError);
        }

        public void OnInitialized()
        {
            Debug.Log("Cloud Handler initialized !");
            AppearitionArHandler.Instance.ArProviderInitializationStateChanged(true);

            //If initialized, start scanning!
            ChangeScanningState(AppearitionArHandler.Instance.IsInitialized);
        }

        #endregion


        #region Event Handling

        public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
        {
            AppearitionLogger.LogInfo(string.Format("Target found, AssetID:{0}", targetSearchResult.MetaData));
            AppearitionExperience targetObject = AppearitionArHandler.Instance.InitializeSingleTarget(targetSearchResult.MetaData);

            if (targetObject != null)
            {
                //Start tracking, and add an event handler if none exist.
                ImageTargetBehaviour imageTarget = ObjectTracker.TargetFinder.EnableTracking(targetSearchResult, targetObject.gameObject);

                if (targetObject.GetComponent<VuforiaTargetEventHandler>() == null)
                    targetObject.gameObject.AddComponent<VuforiaTargetEventHandler>().Setup(targetObject, imageTarget);

                //Disable tracking on target found.
                if (AppearitionArDemoConstants.DISABLE_TRACKING_ON_TARGET_FOUND)
                    ChangeScanningState(false);
            }
        }

        public void OnStateChanged(bool scanning)
        {
            AppearitionLogger.LogInfo("Scan state changed to : " + scanning);
            IsScanning = scanning;
        }

        public void OnUpdateError(TargetFinder.UpdateState updateError)
        {
        }

        #endregion

        /// <summary>
        /// Initializes or de-initializes the ArProvider.
        /// </summary>
        /// <param name="shouldBeEnabled"></param>
        public void ChangeArProviderActiveState(bool shouldBeEnabled)
        {
            if (VuforiaBehaviour.Instance != null && VuforiaBehaviour.Instance.enabled != shouldBeEnabled)
                VuforiaBehaviour.Instance.enabled = shouldBeEnabled;
        }

        /// <summary>
        /// Whether Vuforia should be doing cloud scan or not.
        /// </summary>
        /// <param name="shouldScan"></param>
        public void ChangeScanningState(bool shouldScan)
        {
            ChangeScanningState(shouldScan, AppearitionArDemoConstants.CLEAR_TRACKER_ON_TARGET_LOST);
        }

        /// <summary>
        /// Whether Vuforia should be doing cloud scan or not.
        /// </summary>
        /// <param name="shouldScan"></param>
        /// <param name="clearTracker"></param>
        public void ChangeScanningState(bool shouldScan, bool clearTracker)
        {
            if (shouldScan)
                ObjectTracker.TargetFinder.StartRecognition();
            else
                ObjectTracker.TargetFinder.Stop();

            if (clearTracker)
                ObjectTracker.TargetFinder.ClearTrackables(false);

            IsScanning = shouldScan;
        }

        /// <summary>
        /// If any target is currently being tracked, removes it and clears it. Additionally, can start scanning again.
        /// </summary>
        /// <param name="shouldRestartScanning">If set to <c>true</c> should restart scanning.</param>
        public void ClearTargetsBeingTracked(bool shouldRestartScanning = true)
        {
            if (ObjectTracker.TargetFinder != null)
                ObjectTracker.TargetFinder.ClearTrackables();

            if (shouldRestartScanning)
                ChangeScanningState(true, false);

            //Let the ArHandler know about the new Object's state.
            AppearitionArHandler.Instance.ArProviderTargetStateChanged(null, AppearitionArHandler.TargetState.None);
        }
    }
    #else
    [RequireComponent(typeof(CloudRecoBehaviour))]
    public class VuforiaCloudProviderHandler : MonoBehaviour,  IArProviderHandler
    {
        /// <summary>
        /// Timer keeping track of when the Scanning mode should be disabled, if feature toggled in the AppearitionArHandler.
        /// </summary>
        float _disableScanTimer;

        //Handy Properties
        private CloudRecoBehaviour _cloudRecoB;

        public CloudRecoBehaviour CloudRecoB
        {
            get
            {
                if (_cloudRecoB == null)
                    _cloudRecoB
                        = GetComponent<CloudRecoBehaviour>();
                return _cloudRecoB;
            }
        }


        private ObjectTracker _objectTracker;

        public ObjectTracker ObjectTracker
        {
            get { return _objectTracker ?? (_objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>()); }
        }

        private ImageTargetFinder _currentTargetFinder;

        public ImageTargetFinder CurrentTargetFinder
        {
            get
            {
                if (_currentTargetFinder == null)
                    _currentTargetFinder = (ImageTargetFinder) ObjectTracker?.GetTargetFinder<ImageTargetFinder>();
                return _currentTargetFinder;
            }
            private set => _currentTargetFinder = value;
        }

        private bool _isScanning;

        /// <summary>
        /// Whether Vuforia is currently scanning a target or not.
        /// </summary>
        public bool IsScanning
        {
            get { return _isScanning; }
            protected set
            {
                _isScanning = value;


                if (AppearitionArHandler.Instance.disableScanningModeOverTime)
                    _disableScanTimer = AppearitionArHandler.Instance.disableScanningModeDelay;

                ////Tell the AppearitionArHandler about it!
                AppearitionArHandler.Instance.ArProviderScanStateChanged(_isScanning);
            }
        }

        private bool _isArProviderInitialized;

        /// <summary>
        /// Whether Vuforia, along with its relevant components, is initialized.
        /// </summary>
        public bool IsArProviderInitialized
        {
            get
            {
                bool oldValue
                    = _isArProviderInitialized;

                //Check the new value
                _isArProviderInitialized = VuforiaManager.Instance.Initialized;

                if (oldValue != _isArProviderInitialized)
                    AppearitionArHandler.Instance.ArProviderInitializationStateChanged(_isArProviderInitialized);

                return _isArProviderInitialized;
            }
        }

        /// <summary>
        /// Whether Vuforia's engine is active or not.
        /// </summary>
        public bool IsArProviderActive
        {
            get { return VuforiaBehaviour.Instance != null && VuforiaBehaviour.Instance.enabled && _isActive; }
        }

        public bool DisableScanningOnTargetFound => true;

        public bool UseExtendedTracking => false;

        bool _isActive;

        public float ScaleMultiplier => 640f;

        Camera _providerCamera;

        public Camera ProviderCamera
        {
            get
            {
                if (_providerCamera == null)
                    _providerCamera = VuforiaBehaviour.Instance.GetComponent<Camera>();
                return _providerCamera;
            }
        }

        void Awake()
        {
            CloudRecoB.RegisterOnInitializedEventHandler(OnInitialized);
            CloudRecoB.RegisterOnInitErrorEventHandler(OnInitError);
            CloudRecoB.RegisterOnNewSearchResultEventHandler(OnNewSearchResult);
            CloudRecoB.RegisterOnStateChangedEventHandler(OnStateChanged);
            CloudRecoB.RegisterOnUpdateErrorEventHandler(OnUpdateError);
            //CloudRecoB.RegisterEventHandler(this);
        }

        public void InitializeVuforia()
        {
            _isActive = true;
            VuforiaBehaviour.Instance.enabled = true;
            VuforiaRuntime.Instance.InitVuforia();
        }

        void Update()
        {
            if (IsScanning && AppearitionArHandler.Instance.disableScanningModeOverTime)
            {
                if (_disableScanTimer <= 0)
                    ChangeScanningState(false);
                else
                    _disableScanTimer
                        -= Time.deltaTime;
            }
        }


        #region Init 

        public void OnInitError(TargetFinder.InitState initError)
        {
            Debug.LogError("Init error: " + initError);
        }

        /// <summary>
        /// Occurs when the tracker is initialized.
        /// </summary>
        /// <param name="targetFinder">Target finder.</param>
        public void OnInitialized(TargetFinder targetFinder)
        {
            if (!_isActive)
                return;
            
            Debug.Log("Cloud Handler initialized !");

            CurrentTargetFinder = targetFinder as ImageTargetFinder;
//            AppearitionArHandler.Instance.ArProviderInitializationStateChanged(true);
//           ChangeScanningState(AppearitionArHandler.Instance.IsInitialized);
            ChangeScanningState(true);
        }

        /// <summary>
        /// Step only required if the cloud reco was initialized, then de-initialized using DeInitTargetFinder to swap the credentials.
        /// </summary>
        public void InitTargetFinder()
        {
            if (CurrentTargetFinder != null &&
                CurrentTargetFinder.GetInitState() != TargetFinder.InitState.INIT_SUCCESS &&
                CurrentTargetFinder.GetInitState() != TargetFinder.InitState.INIT_RUNNING)
                CurrentTargetFinder.StartInit(CloudRecoB.AccessKey, CloudRecoB.SecretKey);
        }

        /// <summary>
        /// Step required before changing the Cloud reco credentials.
        /// </summary>
        /// <returns></returns>
        public bool DeInitTargetFinder()
        {
            if (CurrentTargetFinder != null && CurrentTargetFinder.GetInitState() == TargetFinder.InitState.INIT_SUCCESS)
            {
                CurrentTargetFinder.Deinit();
                return true;
            }

            return false;
        }

        /// <summary>
        /// Get the license set up for Vuforia.
        /// </summary>
        /// <returns></returns>
        public IEnumerator SetupLicense()
        {
            yield return VuforiaLicenseFetcherB.FetchVuforiaLicense();
        }

        #endregion


        #region Event Handling

        //public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
        //{
        //    StartCoroutine(ProcessResult(targetSearchResult));
        //}

        Coroutine _onNewSearchProcess = null;

        public void OnNewSearchResult(TargetFinder.TargetSearchResult targetSearchResult)
        {
            if (!IsArProviderActive)
                return;
            
            Debug.Log("Called_Processing");
            TargetFinder.CloudRecoSearchResult cloudResult
                = (TargetFinder.CloudRecoSearchResult) targetSearchResult;
            AppearitionLogger.LogInfo(string.Format("Target found, AssetID:{0}", cloudResult.MetaData));
            Debug.Log(string.Format("Target found, AssetID:{0}", cloudResult.MetaData));

            Asset outcome = new Asset();
            outcome.assetId = cloudResult.MetaData;
            //yield return ArTargetHandler.GetSpecificMediasInAssetByQueryProcess(outcome.assetId, false, null, success => outcome.mediaFiles = success.ToArray());

            if(_onNewSearchProcess != null)
                StopCoroutine(_onNewSearchProcess);
            
            AppearitionExperience targetObject
                  //= AppearitionArHandler.Instance.InitializeSingleTarget(cloudResult.MetaData);
                  = AppearitionArHandler.CreateExperienceFromAsset(outcome, OnValidateAppearitionExperience);
            
            if (targetObject != null && CurrentTargetFinder != null)
            {
                //Debug.Log("Called_Processing 1");

                ImageTargetBehaviour imageTarget
                    = (ImageTargetBehaviour) CurrentTargetFinder.EnableTracking(targetSearchResult, targetObject.gameObject);

                //Debug.Log("Called_Processing 2");
                VuforiaTargetEventHandler eventHandler = targetObject.GetComponent<VuforiaTargetEventHandler>();

                //Debug.Log("Called_Processing 3");
                if (eventHandler == null)
                    targetObject.gameObject.AddComponent<VuforiaTargetEventHandler>().Setup(targetObject, imageTarget);
                else
                    eventHandler.Setup(targetObject, imageTarget);

                //Debug.Log("Called_Processing 4");
                //Disable tracking on target found.
                if (AppearitionArDemoConstants.DISABLE_TRACKING_ON_TARGET_FOUND)
                    ChangeScanningState(false);

                AppearitionArHandler.Instance.ArProviderTargetStateChanged(targetObject, AppearitionArHandler.TargetState.TargetFound);

                //Debug.Log("Called_Processing 5");
            }
            else
            {
                Debug.Log(string.Format("Unable to find the experience linked to the target of id {0}",
                    cloudResult.MetaData));
            }
        }

        /// <summary>
        /// Occurs after the experience is loaded; contains whether it was successful or not.
        /// </summary>
        /// <param name="isValid"></param>
        void OnValidateAppearitionExperience(bool isValid)
        { 
            if(!isValid)
                ClearTargetsBeingTracked(true);
        }

        public void OnStateChanged(bool scanning)
        {
            if (VuforiaManager.Instance != null && IsArProviderInitialized)
                AppearitionLogger.LogInfo("Scan state changed to : " + scanning);
            IsScanning = scanning;
        }

        public void OnUpdateError(TargetFinder.UpdateState updateError)
        {
        }

        #endregion

        /// <summary>
        /// Initializes or de-initializes the ArProvider.
        /// </summary>
        /// <param name="shouldBeEnabled"></param>
        public void ChangeArProviderActiveState(bool shouldBeEnabled)
        {
            if (VuforiaManager.Instance != null)
            {
                if (!VuforiaManager.Instance.Initialized)
                    InitializeVuforia();
                else if (VuforiaBehaviour.Instance.enabled != shouldBeEnabled && VuforiaManager.Instance.Initialized)
                    VuforiaBehaviour.Instance.enabled = shouldBeEnabled;
                _isActive = shouldBeEnabled;
            }
        }

        /// <summary>
        /// Whether Vuforia should be doing cloud scan or not.
        /// </summary>
        /// <param name="shouldScan"></param>
        public void ChangeScanningState(bool shouldScan)
        {
            ChangeScanningState(shouldScan, AppearitionArDemoConstants.CLEAR_TRACKER_ON_TARGET_LOST);
        }

        /// <summary>
        /// Whether Vuforia should be doing cloud scan or not.
        /// </summary>
        /// <param name="shouldScan"></param>
        /// <param name="clearTracker"></param>
        public void ChangeScanningState(bool shouldScan, bool clearTracker)
        {
            if (CurrentTargetFinder == null)
                return;

            if (shouldScan)
                CurrentTargetFinder.StartRecognition();
            else
                CurrentTargetFinder.Stop();

            if (clearTracker)
                CurrentTargetFinder.ClearTrackables(false);

            IsScanning = shouldScan;
        }

        /// <summary>
        /// If any target is currently being tracked, removes it and clears it. Additionally, can start scanning again.
        /// </summary>
        /// <param name="shouldRestartScanning">If set to <c>true</c> should restart scanning.</param>
        public void ClearTargetsBeingTracked(bool shouldRestartScanning = true)
        {
            if (CurrentTargetFinder != null)
                CurrentTargetFinder.ClearTrackables();

            if (shouldRestartScanning)
                ChangeScanningState(true, false);

            //Let the ArHandler know about the new Object's state.
            AppearitionArHandler.Instance.ArProviderTargetStateChanged(null, AppearitionArHandler.TargetState.None);
        }

        public void RefocusCamera()
        {
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }
    }

    #endif
}