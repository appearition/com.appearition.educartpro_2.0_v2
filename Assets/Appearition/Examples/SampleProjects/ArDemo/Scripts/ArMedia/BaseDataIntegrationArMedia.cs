﻿using System.Collections;
using System.Collections.Generic;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using Appearition.Common.TypeExtensions;
using UnityEngine;
using UnityEngine.UI;

namespace Appearition.Example
{
    public abstract class BaseDataIntegrationArMedia : BaseCanvasArMedia
    {
        //Consts
        const string BG_WIDTH_KEY = "canvas-width";
        const string BG_HEIGHT_KEY = "canvas-height";
        const string BG_COLOR_KEY = "background-color";
        const string FONT_COLOR_KEY = "font-color";
        const string FONT_SIZE_KEY = "font-size";
        const string FONT_WEIGHT_KEY = "font-weight";
        const string FONT_STYLE_KEY = "font-style";

        const string DATA_REFRESH_KEY = "dataRefreshSeconds";


        //References
        protected Image backgroundImage;
        protected Text queryText;

        //Data
        protected Dictionary<string, string> customParams = new Dictionary<string, string>();
        public override bool IsMediaReadyAndDownloaded => true;
        protected virtual bool UseBackgroundImage => true;
        protected virtual bool UseQueryText => true;
        Coroutine _refreshProcess;

        #region Default Customs

        protected virtual Color BackgroundColor
        {
            get
            {
                Color? bgCol = customParams.ContainsKey(BG_COLOR_KEY) ? customParams[BG_COLOR_KEY].TryConvertToColor() : default;
                return bgCol ?? Color.white;
            }
        }

        protected virtual float BackgroundWidth
        {
            get
            {
                if (customParams.ContainsKey(BG_WIDTH_KEY) && float.TryParse(customParams[BG_WIDTH_KEY], out float newValue))
                    return newValue;
                return 512;
            }
        }

        protected virtual float BackgroundHeight
        {
            get
            {
                if (customParams.ContainsKey(BG_HEIGHT_KEY) && float.TryParse(customParams[BG_HEIGHT_KEY], out float newValue))
                    return newValue;
                return 512;
            }
        }

        protected virtual Color FontColor
        {
            get
            {
                Color? fontCol = customParams.ContainsKey(FONT_COLOR_KEY) ? customParams[FONT_COLOR_KEY].TryConvertToColor() : default;
                return fontCol ?? Color.black;
            }
        }

        protected virtual int FontSize
        {
            get
            {
                if (customParams.ContainsKey(FONT_SIZE_KEY) && int.TryParse(customParams[FONT_SIZE_KEY], out int newValue))
                    return newValue;
                return 25;
            }
        }

        protected virtual FontStyle FontStyleAndWeight
        {
            get
            {
                string fontStyle = customParams.ContainsKey(FONT_STYLE_KEY) ? customParams[FONT_STYLE_KEY].ToLower() : "";
                string fontWeight = customParams.ContainsKey(FONT_WEIGHT_KEY) ? customParams[FONT_WEIGHT_KEY].ToLower() : "";

                if (fontWeight.Equals("bold") && !fontStyle.Equals("italic"))
                    return FontStyle.Bold;
                if (!fontWeight.Equals("bold") && fontStyle.Equals("italic"))
                    return FontStyle.Italic;
                if (fontWeight.Equals("bold") && fontStyle.Equals("italic"))
                    return FontStyle.BoldAndItalic;

                return FontStyle.Normal;
            }
        }


        protected virtual float? RefreshTimer
        {
            get
            {
                if (customParams.ContainsKey(DATA_REFRESH_KEY) && float.TryParse(customParams[DATA_REFRESH_KEY], out float newValue))
                    return newValue;
                return default;
            }
        }

        #endregion

        float _refreshTimer = 0;
        protected bool IsRefreshProcessOngoing => _refreshProcess != null;

        public override void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            base.Setup(associatedExperience, media);

            //Get custom params, if any
            if (!string.IsNullOrEmpty(media.custom))
                customParams = media.custom.DeserializeDictionary();

            foreach (var kvp in customParams)
            {
                Debug.Log($"{kvp.Key} - {kvp.Value}");
            }

            //BackgroundImage
            if (UseBackgroundImage)
            {
                backgroundImage = new GameObject("Background").AddComponent<Image>();
                backgroundImage.transform.SetParent(transform);
                SetMediaRectPosition(backgroundImage.rectTransform, true);

                backgroundImage.color = BackgroundColor;
                //backgroundImage.rectTransform.sizeDelta = new Vector2(BackgroundWidth, BackgroundHeight);
            }

            //Font
            if (UseQueryText)
            {
                queryText = new GameObject("Query Text").AddComponent<RectTransform>().gameObject.AddComponent<Text>();
                queryText.transform.SetParent(backgroundImage.transform);
                queryText.transform.localPosition = UseBackgroundImage ? Vector3.zero : Data.GetPosition;
                queryText.transform.localRotation = UseBackgroundImage ? Quaternion.identity : Data.GetRotation;
                queryText.transform.localScale = UseBackgroundImage ? Vector3.one : Data.GetScale;
                queryText.rectTransform.pivot = Vector2.one / 2;
                queryText.rectTransform.anchorMin = Vector2.zero;
                queryText.rectTransform.anchorMax = Vector2.one;
                queryText.rectTransform.sizeDelta = Vector2.zero;

                queryText.fontStyle = FontStyleAndWeight;
                queryText.color = FontColor;
                queryText.fontSize = FontSize;
                queryText.alignment = TextAnchor.MiddleCenter;
                queryText.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            }
        }

        public override void ChangeDisplayState(bool state)
        {
            //Upon opening, always refresh.
            if (state)
            {
                if (_refreshProcess != null)
                    StopCoroutine(_refreshProcess);
                _refreshProcess = StartCoroutine(RefreshDataProcess());
            }

            base.ChangeDisplayState(state);
        }

        void Update()
        {
            if (RefreshTimer.HasValue && !IsRefreshProcessOngoing)
            {
                if (_refreshTimer < 0)
                {
                    _refreshProcess = StartCoroutine(RefreshDataProcess());
                    _refreshTimer = RefreshTimer.GetValueOrDefault();
                }
                else
                    _refreshTimer -= Time.deltaTime;
            }

            //In case subclasses need update for input
            OnUpdateOccured();
        }

        protected virtual void OnUpdateOccured()
        {
        }

        /// <summary>
        /// Get the latest query content.
        /// </summary>
        /// <returns></returns>
        protected virtual IEnumerator RefreshDataProcess()
        {
            string data = "";
            yield return ArTargetHandler.GetDataIntegrationResultProcess(ExperienceRef.Data, Data, success => data = success);
            ApplyDataToVisuals(data);
            _refreshTimer = RefreshTimer.GetValueOrDefault();
            _refreshProcess = null;
        }

        protected virtual void ApplyDataToVisuals(string data)
        {
            //Just apply text
            queryText.text = data;
        }

        void OnDisable()
        {
            _refreshProcess = null;
            OnDisableOccured();
        }

        protected virtual void OnDisableOccured()
        {
        }
    }
}