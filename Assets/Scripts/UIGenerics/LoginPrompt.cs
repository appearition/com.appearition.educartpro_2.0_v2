﻿using UnityEngine;

public class LoginPrompt : MonoBehaviour
{

    public GameObject loginPanel;
    public GameObject homeScreen;
    public void NotNow()
    {
        gameObject.SetActive(false);
    }

    public void GoToLogin()
    {
        loginPanel.SetActive(true);
        homeScreen.SetActive(false);
        gameObject.SetActive(false);
    }

}
