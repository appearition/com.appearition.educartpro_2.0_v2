using System.Collections;
using System.Collections.Generic;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

namespace Appearition.Example
{
    [RequireComponent(typeof(VideoPlayer))]
    [RequireComponent(typeof(RectTransform))]
    public class VideoArMedia : BaseCanvasArMedia
    {
        public delegate void VideoLoaded(string videoMediaId);

        /// <summary>
        /// Occurs whenever the video has done loading. Can be caught for debugging, analytic or UI.
        /// </summary>
        public event VideoLoaded OnVideoLoaded;

        //References
        public VideoPlayer videoPlayer;
        public RawImage displayImage;
        public RenderTexture templateRenderTexture;
        public RectTransform panelControlHolder;
        public Button playButton;
        public AspectRatioFitter ratioFitter;

        //Internal Variables
        public override bool IsMediaReadyAndDownloaded => _isVideoReadyToPlay;

        private bool? _hasVideoLoaded;
        private bool _isVideoReadyToPlay;

        public override void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            //Let the rest of the setup happen
            base.Setup(associatedExperience, media);

            //Setup the video object
            videoPlayer.targetTexture = new RenderTexture(templateRenderTexture);
            panelControlHolder.localScale = Vector3.one * (Data.isTracking ? 0.1f : 1);
            if (displayImage.material != null)
            {
                displayImage.material = new Material(displayImage.material);
                
                Sprite placeholderSprite = AppearitionArDemoConstants.GetImagePlaceholder();
                
                if (placeholderSprite != null)
                    displayImage.material.mainTexture = placeholderSprite.texture;
            }

            ////Position the video
            //SetMediaRectPosition(displayImage.rectTransform, Data.isTracking);
            //displayImage.rectTransform.localPosition =
            //    new Vector3(displayImage.rectTransform.localPosition.x, displayImage.rectTransform.localPosition.z, displayImage.rectTransform.localPosition.y);
            //panelControlHolder.localRotation = displayImage.rectTransform.localRotation;

            ////Adjust the Playback panel
            //if (Data.isTracking)
            //{
            //    //Adapting the panel control based on the video's size and position
            //    panelControlHolder.transform.localPosition = displayImage.transform.localPosition;
            //    panelControlHolder.transform.localScale *= displayImage.transform.localScale.y;
            //}

            var rectTransform = transform as RectTransform;
            rectTransform.localPosition += Data.GetPosition * (AppearitionArHandler.Instance.ProviderHandler?.ScaleMultiplier ?? 1f);
            SetMediaRectPosition(ratioFitter.GetComponent<RectTransform>(), Data.isTracking);


            if(FindObjectOfType<AudioListener>() == null)
                FindObjectOfType<Camera>().gameObject.AddComponent<AudioListener>();
            
            Debug.Log("Video setup !");
        }

        public override void ChangeDisplayState(bool state)
        {
            base.ChangeDisplayState(state);

            if (state)
            {
                //Check if the video has been loaded or not.
                if (!_hasVideoLoaded.HasValue)
                {
                    _hasVideoLoaded = false;
                    //Download it if it hasn't been downloaded
                    ArTargetHandler.LoadMediaFileContent(ExperienceRef.Data, Data, OnVideoDownloaded);
                }
                else if (_hasVideoLoaded.Value)
                {
                    //Launch the video
                    videoPlayer.Play();
                }

                //Disable the display either way
                panelControlHolder.gameObject.SetActive(false);
            }
            else
            {
                //If disabling the object, stop the video player.
                videoPlayer.Stop();
            }
        }

        /// <summary>
        /// Occurs whenever the video has finished downloading. Just plug the url in the video player, and play if object active.
        /// </summary>
        /// <param name="obj"></param>
        private void OnVideoDownloaded(Dictionary<string, byte[]> obj)
        {
            _hasVideoLoaded = true;

            if (ExperienceRef == null || ExperienceRef.Data == null)
                return;

            if (obj != null && obj.Count > 0)
            {
                try
                {
                    videoPlayer.url = ArTargetHandler.GetPathToMedia(ExperienceRef.Data, Data);
                }
                catch (MissingReferenceException)
                {
                    return;
                }

                //Handle the new image's content and ratio
                displayImage.texture = videoPlayer.targetTexture;

                //Handle scaling once the video is prepared
                videoPlayer.prepareCompleted += RescaleVideoForGrid;


                //If the object is currently visible, start playing (if autoplay)
                if (Data.isAutoPlay && gameObject.activeInHierarchy)
                    ChangeVideoPlayState(true);

                if (OnVideoLoaded != null)
                    OnVideoLoaded(Data.arMediaId.ToString());

                ChangeVideoPlayState(true);
            }
            else
            {
                //Display error while loading video
                AppearitionLogger.LogError(string.Format("Error while loading the video of id {0} and of filename {1}", Data.arMediaId, Data.fileName));
            }
        }

        public void ChangeVideoPlayState(bool shouldBePlaying)
        {
            if (!_hasVideoLoaded.HasValue)
                return;

            if (shouldBePlaying)
            {
                _isVideoReadyToPlay = true;
                videoPlayer.Play();
            }
            else
            {
                videoPlayer.Pause();
            }

            panelControlHolder.gameObject.SetActive(!shouldBePlaying);
        }

        /// <summary>
        /// Called once the video is prepared. Uses the video's data to rescale based on the grid.
        /// Only applies to tracking.
        /// </summary>
        /// <param name="source">Source.</param>
        void RescaleVideoForGrid(VideoPlayer source)
        {
            if (!Data.isTracking || _isVideoReadyToPlay)
                return;

            float refWidth = source.texture.width;
            float refHeight = source.texture.height;

            ratioFitter.aspectRatio = (float)refWidth / (float)refHeight;

            //The videoplayer uses the video's size instead of a square for the texture. This affects the way videos are deformed, 
            //and applications like 8AR have their videos transformed twice.
            //Reversing the transformation so that it fits the way the video is layed out on the grid.

            Vector3 tmpDisplayImageLocalScale = displayImage.rectTransform.localScale;
            tmpDisplayImageLocalScale.y *= refWidth / refHeight;
            displayImage.transform.localScale = tmpDisplayImageLocalScale;

            _isVideoReadyToPlay = true;
        }

        void OnDisable()
        {
            videoPlayer.Stop();
        }
    }
}