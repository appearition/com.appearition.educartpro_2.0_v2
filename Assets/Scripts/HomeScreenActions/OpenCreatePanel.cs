﻿using Appearition;
using UnityEngine;

public class OpenCreatePanel : MonoBehaviour
{
    public GameObject CreatePanel;
    public GameObject loginPrompt;
    public void SwitchToCreatePanel()
    {
        if (AppearitionGate.Instance.CurrentUser.IsUserLoggedIn)
        {
            CreatePanel.SetActive(true);
            gameObject.SetActive(false);
        }
        else
        {
            loginPrompt.SetActive(true);
        }
    }

}
