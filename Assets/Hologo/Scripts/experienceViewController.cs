﻿using Appearition.Example;
using Appearition.Rmitilp;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Hologo2
{
    /// <summary>
    /// Created by: Hamid (hamidibrahim@gmail.com) - 16 july 2019
    /// Modified by: Shariz - 20th September 2020
    /// The Main Experience Viewer Script
    /// This script displays the experience from the Experience Data and Localized Data from the Hologo Assetbundle
    /// </summary>
    public class experienceViewController : MonoBehaviour
    {
        //public MarkerModeUiHandler markerUiHandler;
        //public MarkerlessUiHandlerB markerlessUiHandler;

        [Header("Experience Data")]
        [SerializeField]
        private experienceData_SO currentExperience; // This is where the Experience Data Scriptable Object from the Experience Assetbundle goes in
        [SerializeField]
        private experienceLocalizationData_SO experienceLocalizedData;// This is where the Experience Localization Data Scriptable Object from the Experience Assetbundle goes in

        [Header("UI Elements")]
        [Header("Scene Objects")]
        [SerializeField]
        private AudioSource audioSource;

        public AssetBundle myLoadedLocalAssetBundle;
        public AssetBundle myLoadedAssetBundle;


        [Header("MONO BEHAVIORS")]

        // current active slide gameobject
        private GameObject currentSlide;
        // index of current playing narrations slides index in list
        private int learningIndex = 0;
        // a check if the user has clicked play and its already in playing mode
        // if so we just puase and play instead of starting from new
        private bool isInPlay = false;
        //script of experienc ui keeping store here
        private float audioCutOff = 0.1f;
        private bool isLabelsOn = true; //Shariz 26th Feb 2020 v2.0.4 // Shariz 13th Sept 2020 making Experience Viewer

        public Canvas mUI;
        // Starting the scene

        public void Init(float scale)
        {
            if (Camera.allCameras.Length > 1)
                MainCamera = Camera.allCameras[1];
            else
                MainCamera = Camera.allCameras[0];
            //myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "Heart.bin"));
            if (myLoadedAssetBundle == null)
            {
                Debug.Log("Failed to load AssetBundle!");
                return;
            }

            for (int i = 0; i < myLoadedAssetBundle.GetAllAssetNames().Length; i++)
            {
                if (myLoadedAssetBundle.GetAllAssetNames()[i].Contains(".asset"))
                {
                    currentExperience = myLoadedAssetBundle.LoadAsset(myLoadedAssetBundle.GetAllAssetNames()[i]) as experienceData_SO;
                    break;
                    //var prefab = myLoadedAssetBundle.LoadAsset<GameObject>(myLoadedAssetBundle.GetAllAssetNames()[i]);
                    //Instantiate(prefab);
                }
            }
            // myLoadedLocalAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "HeartLocal.bin"));
            if (myLoadedLocalAssetBundle == null)
            {
                Debug.Log("Failed to load AssetBundle!");
                return;
            }

            for (int i = 0; i < myLoadedLocalAssetBundle.GetAllAssetNames().Length; i++)
            {
                if (myLoadedLocalAssetBundle.GetAllAssetNames()[i].Contains(".asset"))
                {
                    experienceLocalizedData = myLoadedLocalAssetBundle.LoadAsset(myLoadedLocalAssetBundle.GetAllAssetNames()[i]) as experienceLocalizationData_SO;
                    break;
                    //var prefab = myLoadedAssetBundle.LoadAsset<GameObject>(myLoadedAssetBundle.GetAllAssetNames()[i]);
                    //Instantiate(prefab);
                }
            }

            //var prefab = myLoadedAssetBundle.LoadAsset<GameObject>(@"assets/ar experiences/chemistry/batch 5/experience 7-10 flame tests/experience 7/flametest_copper_backup.prefab");
            //Instantiate(prefab);
            //myLoadedAssetBundle.Unload(false);
            //myLoadedLocalAssetBundle.Unload(false);
            if (Camera.allCameras.Length > 1)
                scale = 0.5f;
            else
                scale = 350f;
            initiateExperienceViewController(scale);
            mUI.enabled = true;
            //markerUiHandler.DisplayMessage(null);
        }


        public void initiateExperienceViewController(float scale)
        {
            //instantiate experience if its a normal experience
            if (currentExperience.modelType == 1)
            {
                // instantiating slide models and localizing them
                makeNormalExperience(currentExperience, currentExperience.Models, experienceLocalizedData.giveSlides(), true, scale);
                // creating explore and learn buttons

                makeLearnAndExploreStepButtons(currentExperience.Models, experienceLocalizedData.giveSlides(), clickStepSlideButton);
            }
            else
            {
                // journey/etc instantiate code comes here
            }
        }

        // assigned to the step button to advance to next slide
        // also if in learn mode it will start the narration;
        void clickStepSlideButton(int id)
        {

            // when user clicks a step if its in learn mode
            // narration start to play for that slide
            advanceStep(id);
            learningIndex = id;
            StopAllCoroutines();
            isInPlay = false;
            startLearning();

        }

        // toggle label visibility
        public void toggleLabels()
        {
            if (isLabelsOn)
            {
                isLabelsOn = false;

            }
            else
            {
                isLabelsOn = true;
            }
            setLabelVisibility();
        }

        // set label visibility
        private void setLabelVisibility()
        {
            experienceLabeler el = currentSlide.GetComponent<experienceLabeler>();
            if (el != null)
            {
                el.MyLabelsEnable(isLabelsOn);
            }


        }

        // stop audio
        public void stopAudio()
        {
            togglePlayMode(false);
            audioSource.Stop();
        }


        // for advancing a step;
        void advanceStep(int id)
        {
            activateCurrentSlideButton(id);
            //Debug.Log("Current active slide ID is "+id);
            currentSlide = changeActiveSlide(id);
            //setCollider.setCollider(currentSlide);
            setLabelVisibility();
        }



        // assigned to the play button for learning 
        public void startLearning()
        {
            // if already playing
            if (isInPlay)
            {
                if (audioSource.isPlaying)
                {
                    audioSource.Pause();
                    togglePlayMode(false);
                }
                else
                {
                    if (audioSource.time != 0)
                    {
                        audioSource.Play();
                        togglePlayMode(true);
                    }
                }
            }
            else //if not playing
            {
                learnExecute();
                togglePlayMode(true);
            }
        }

        // topggle play and pause mode
        private void togglePlayMode(bool play)
        {
            playPauseToggle(play);
        }

        // calculating the next step to play
        void learnExecute()
        {

            if (learningIndex >= currentExperience.Models.Count)
            {
                learningIndex = 0;
                // check to see if experience is in learn loop mode.
                if (!currentExperience.learnLoopMode)
                {
                    return;
                }
            }

            slideDetails sd = currentExperience.Models[learningIndex];

            if (sd.isInLearnMode)
            {
                // advancing to next slide and play narration
                advanceStep(learningIndex);
                StartCoroutine(playStep());
            }
            else
            {
                // if this step is not in learn we increase the count and run this method again
                learningIndex++;
                learnExecute();
            }
        }


        // audio narration player will advance to next step after finishing current slides audio
        IEnumerator playStep()
        {
            isInPlay = true;
            slideDetails sd = currentExperience.Models[learningIndex];
            audioSource.clip = sd.narration;
            audioSliderStart(sd.narration.length);
            audioSource.Play();
            // awaiting while the audio clip is not done
            while (audioSource.time < audioSource.clip.length - audioCutOff)
            {
                audioSliderUpdate(audioSource.time);
                yield return null;
            }
            //go to next step
            learningIndex++;
            learnExecute();
            yield return null;
        }



        // UI for Slide step
        [Header("UI PREFABS")]
        [SerializeField]
        private GameObject slideStepButton;
        [Header("UI ELEMENTS")]

        [SerializeField]
        private Transform learnStepsParent;


        [SerializeField]
        private List<GameObject> learnStepButtons;



        // make all learn buttons
        // change slide steps
        // in learn mode auto advance to next step
        public void makeLearnAndExploreStepButtons(List<slideDetails> Models, List<experienceLabelNarrationDataClass> localize, CallbackForId callBackId)
        {
            for (int i = 0; i < Models.Count; i++)
            {

                if (Models[i].isInLearnMode)
                {
                    learnStepButtons.Add(makeStepButton(Models[i], localize[i], callBackId, i, learnStepsParent));
                }
            }
        }

        // Activate the current slide button
        public void activateCurrentSlideButton(int id)
        {

            for (int i = 0; i < learnStepButtons.Count; i++)
            {

                slide_step_button_ui_element ssb = learnStepButtons[i].GetComponent<slide_step_button_ui_element>();
                ssb.amIActive(id);
            }

        }

        // Create the step button
        private GameObject makeStepButton(slideDetails model, experienceLabelNarrationDataClass localize, CallbackForId callBackId, int i, Transform parent)
        {
            GameObject go = Instantiate(slideStepButton);
            slide_step_button_ui_element ssb = go.GetComponent<slide_step_button_ui_element>();
            ssb.fillUpButton(i, localize.localizedName, callBackId);
            go.transform.SetParent(parent);
            go.transform.localScale = new Vector3(1, 1, 1);
            return go;
        }

        void ChangeDisplayState()
        {

        }

        // hide/show game objects
        void hideOrShowGameObjects(bool state, List<GameObject> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                list[i].SetActive(state);
            }
        }


        [Header("SCENE ELEMENTS")]
        [SerializeField]
        private List<GameObject> experienceModelList; // list of experience models
        private List<GameObject> allLabelsObjects; // list of labels

        [SerializeField]
        private List<GameObject> descriptionList; //list of descriptions


        [Header("SCENE ELEMENTS")]
        [SerializeField]
        private Transform scaleAndMoveParent;
        [SerializeField]
        private Transform exprienceSlidesParent; // slides parents
        [SerializeField]
        private Transform labelsParent; // labels parent
        [SerializeField]
        private Camera MainCamera; // main camera
        [SerializeField]
        private Material lineMaterialCommon; // label line material
        [Header("UI ELEMENTS")]
        [SerializeField]
        private GameObject labelPrefab; // label prefab
        [SerializeField]
        private GameObject descriptionPrefab; // description prefab
        [SerializeField]
        private GameObject originMarkerPrefab; // label origin marker prefab



        // active experience slide
        private GameObject currentExperienceSlide;


        // set state of slide (active/inactive)
        public void setSlidesActiveState(bool state)
        {
            exprienceSlidesParent.gameObject.SetActive(state);
        }

        // this makes a typical experience
        public void makeNormalExperience(experienceData_SO cExp, List<slideDetails> Models, List<experienceLabelNarrationDataClass> slideLocalizationData, bool alwaysShowLabels, float scaleMultiplier)
        {

            if (currentExperienceSlide != null)
                Destroy(currentExperienceSlide);


            allLabelsObjects = new List<GameObject>();

            if (Models.Count >= 1)
            {
                experienceModelList = new List<GameObject>();
                descriptionList = new List<GameObject>();
                for (int i = 0; i < Models.Count; i++)
                {
                    // instantiating the model slide
                    GameObject go = Instantiate(Models[i].modelSlide, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
                    go.transform.position = exprienceSlidesParent.position;
                    go.transform.rotation = exprienceSlidesParent.rotation;

                    go.transform.SetParent(exprienceSlidesParent);
                    go.transform.localScale = Vector3.one;
                    // hiding the model
                    go.SetActive(false);
                    experienceModelList.Add(go);
                    //this is trail scale hack

                    // checking to see if labels exits for this slide
                    if (slideLocalizationData[i].labelsExist)
                    {
                        // making labels from localized data
                        List<GameObject> expLabels = makeLabels(i, slideLocalizationData, scaleMultiplier);
                        allLabelsObjects.AddRange(expLabels);
                        // initializing labels 
                        initiateExperienceModelLabels(alwaysShowLabels, go, expLabels);


                    }
                    // check to see if slides description is enabled to display
                    if (slideLocalizationData[i].isInfoDisplayed)
                    {
                        // make the description ui element
                        makeDescriptionUIElement(go, slideLocalizationData[i]);
                    }
                    // setting the narration of the slide from localized data;
                    Models[i].narration = slideLocalizationData[i].Narration;
                    Models[i].isNarrationEnabled = slideLocalizationData[i].isNarrationEnabled;
                    Models[i].isInfoDisplayed = slideLocalizationData[i].isInfoDisplayed;
                }
            }

        }



        // getting labels from localized data and instantiating them .
        List<GameObject> makeLabels(int id, List<experienceLabelNarrationDataClass> slideLocalizationData, float scaleMultiplier)
        {
            List<GameObject> labels = new List<GameObject>();
            for (int i = 0; i < slideLocalizationData[id].labelList.Count; i++)
            {
                // check to see if the label has any text. if not then we set label enabled to false even though
                // we create a label
                // we create empty label so we can keep the count ids same 
                bool enabledStatus = !string.IsNullOrEmpty(slideLocalizationData[id].labelList[i].label);
                // Debug.Log("label enable status " + enabledStatus + ">" + slideLocalizationData[id].labelList[i].labelName);

                GameObject go = Instantiate(labelPrefab, new Vector3(0, 0, 0), Quaternion.Euler(90, 0, 0)) as GameObject;
                go.transform.localScale = new Vector3(scaleMultiplier, scaleMultiplier, 1);


                labelElement le = go.GetComponent<labelElement>();

                // fille localization data for labels
                le.fillWithData(slideLocalizationData[id].labelList[i].label, slideLocalizationData[id].labelList[i].labelAlignment
                    , enabledStatus);
                labels.Add(go);
            }
            return labels;
        }



        // assiging the line material and label parent to the label script in the exp model and initiating it.
        void initiateExperienceModelLabels(bool alwaysShowLabels, GameObject go, List<GameObject> labels)
        {
            experienceLabeler expL = go.GetComponent<experienceLabeler>();

            if (expL != null)
            {
                Debug.LogError(MainCamera.name);
                expL.InitMe(originMarkerPrefab, labels, MainCamera, labelsParent, scaleAndMoveParent, lineMaterialCommon);
            }
        }

        // make a description ui element
        void makeDescriptionUIElement(GameObject parent, experienceLabelNarrationDataClass data)
        {
            GameObject go = Instantiate(descriptionPrefab, new Vector3(0, 0, 0), Quaternion.identity) as GameObject;
            labelElement de = go.GetComponent<labelElement>();
            de.fillWithData(data.desciption, data.descriptionAlignment, data.isInfoDisplayed);
            go.transform.SetParent(parent.transform);
            go.transform.localPosition = data.descriptionPosition;
            descriptionList.Add(go);
        }

        // change active slide
        public GameObject changeActiveSlide(int id)
        {
            hideOrShowGameObjects(false, experienceModelList);
            experienceModelList[id].SetActive(true);
            return experienceModelList[id];
        }

        // get slide list
        public List<GameObject> getSlideList()
        {
            return experienceModelList;
        }

        // clean up experience
        public void cleanUpExperience()
        {
            /*
                        mUI.enabled = false;
                        for (int i = 0; i < allLabelsObjects.Count; i++)
                        {
                            Destroy(allLabelsObjects[i]);
                        }

                        for (int i = 0; i < experienceModelList.Count; i++)
                        {
                            Destroy(experienceModelList[i]);
                        }
              */
        }

        // slide step player
        [Header("UI ELEMENTS")]
        [SerializeField]
        private GameObject centerPlayGraphic;
        [SerializeField]
        private GameObject centerPauseGrapic;
        [SerializeField]
        private Slider audioSlider;


        // auto toggle of play and pause
        public void playPauseToggle(bool play)
        {
            if (play)
            {
                centerPlayGraphic.SetActive(false);
                centerPauseGrapic.SetActive(true);
            }
            else
            {
                centerPlayGraphic.SetActive(true);
                centerPauseGrapic.SetActive(false);
            }
        }

        // manual setting of play and pause
        public void isPlaying(bool state)
        {
            if (state)
            {
                centerPlayGraphic.SetActive(false);
                centerPauseGrapic.SetActive(true);
            }
            else
            {
                centerPlayGraphic.SetActive(true);
                centerPauseGrapic.SetActive(false);
            }
        }

        // start the audio slider
        public void audioSliderStart(float length)
        {

            audioSlider.gameObject.SetActive(true);
            audioSlider.direction = Slider.Direction.LeftToRight;
            audioSlider.minValue = 0;
            audioSlider.maxValue = length;
        }

        // stop the audio slider
        public void audioSliderStop()
        {

            audioSlider.gameObject.SetActive(false);
            audioSlider.value = 0;

        }

        //update audio slider
        public void audioSliderUpdate(float time)
        {
            audioSlider.value = time;

        }

    }
}
