using Appearition.Common;
using UnityEngine;
using UnityEngine.UI;

namespace Appearition.Example
{
    /// <summary>
    /// Base class for any ArMedia which uses a Canvas to render its content.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [RequireComponent(typeof(Canvas))]
    [RequireComponent(typeof(CanvasScaler))]
    [RequireComponent(typeof(GraphicRaycaster))]
    public abstract class BaseCanvasArMedia : MonoBehaviour, IArMedia
    {
        //References
        public AppearitionExperience ExperienceRef { get; private set; }
        protected RectTransform RectTransformRef { get; set; }
        protected Canvas CanvasRef { get; set; }
        protected CanvasScaler CanvasScalerRef { get; set; }

        //ApiData
        /// <summary>
        /// Contains the ApiData of the current ArMedia.
        /// </summary>
        public MediaFile Data { get; protected set; }

        public abstract bool IsMediaReadyAndDownloaded { get; }

        public virtual void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            if (media == null)
                return;

            ExperienceRef = associatedExperience;
            Data = media;

            //8AR fix, if the on target media's translationX is absurdly high, the object is not on the grid and should not be spawned.
            if (Data.translationX < -9000)
            {
                AppearitionLogger.LogInfo(
                    string.Format("Media of id {0} is not located on the grid, but is onTarget (8AR hack). Disabling media.", Data.arMediaId));

                this.enabled = false;
                gameObject.SetActive(false);
                return;
            }

            //Setup the common settings for the canvas objects
            RectTransformRef = GetComponent<RectTransform>();
            CanvasRef = GetComponent<Canvas>();
            CanvasScalerRef = GetComponent<CanvasScaler>();

            //Setup the canvas object based on the mediafile ApiData.
            if (Data.isTracking)
            {
                RectTransformRef.sizeDelta = Vector2.one;
                CanvasRef.renderMode = RenderMode.WorldSpace;
                transform.localRotation = Quaternion.identity; //Quaternion.Euler(Vector3.right * 90);
                transform.localScale = Vector3.one / 100f * (AppearitionArHandler.Instance.ProviderHandler?.ScaleMultiplier ?? 1f);
                CanvasScalerRef.dynamicPixelsPerUnit = 100; //10000;

                //Attach camera.
                CanvasRef.worldCamera = FindObjectOfType<Vuforia.VuforiaBehaviour>()?.GetComponent<Camera>();
            }
            else
            {
                CanvasRef.renderMode = RenderMode.ScreenSpaceOverlay;
                CanvasScalerRef.referenceResolution = new Vector2(Screen.height, Screen.width);
                CanvasScalerRef.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
                CanvasScalerRef.matchWidthOrHeight = 1;

                if (AppearitionArHandler.Instance.shouldFullscreenMediaHaveBlackBackground)
                {
                    Image tmp = new GameObject("Black BG").AddComponent<Image>();
                    tmp.transform.SetParent(transform);
                    tmp.transform.SetAsFirstSibling();
                    tmp.rectTransform.anchorMin = Vector2.zero;
                    tmp.rectTransform.anchorMax = Vector2.one;
                    tmp.rectTransform.pivot = Vector2.one / 2;
                    tmp.rectTransform.sizeDelta = RectTransformRef.sizeDelta;
                    tmp.color = new Color(0, 0, 0, 0.7f);
                }
            }

            CanvasRef.sortingOrder = -1 - Data.arMediaId;
        }

        /// <summary>
        /// Changes the visibility state of the Ar Media.
        /// </summary>
        /// <param name="state"></param>
        public virtual void ChangeDisplayState(bool state)
        {
            gameObject.SetActive(state);
        }

        /// <summary>
        /// Whether or not the current Media is visible.
        /// </summary>
        public bool IsVisible
        {
            get { return gameObject.activeInHierarchy; }
        }

        /// <summary>
        /// Occurs when the experience's tracking state changes from is tracking to tracking lost.
        /// </summary>
        /// <param name="isTracking"></param>
        public void OnExperienceTrackingStateChanged(bool isTracking)
        {
            //Display or hide the object with the tracking.
            ChangeDisplayState(isTracking);
        }

        /// <summary>
        /// Using the MediaFile's ApiData, sets the given RectTransform's 
        /// </summary>
        /// <param name="mediaRectTransform"></param>
        /// <param name="isTracking"></param>
        protected void SetMediaRectPosition(RectTransform mediaRectTransform, bool isTracking)
        {
            mediaRectTransform.anchorMin = Vector2.zero;
            mediaRectTransform.anchorMax = Vector2.one;
            mediaRectTransform.pivot = Vector2.one / 2;

            if (isTracking)
            {
                //mediaRectTransform.Translate(new Vector3(Data.GetPosition.x * (1 / CanvasRef.transform.localScale.x),
                //    Data.GetPosition.y * (1 / CanvasRef.transform.localScale.y),
                //    Data.GetPosition.z * (1 / CanvasRef.transform.localScale.z)));
                mediaRectTransform.localRotation = Data.GetRotation;
                mediaRectTransform.localScale = Data.GetScale;
                mediaRectTransform.localPosition = CanvasRef.transform.rotation * new Vector3(Data.GetPosition.x * (1 / CanvasRef.transform.localScale.x),
                    Data.GetPosition.y * (1 / CanvasRef.transform.localScale.y),
                    Data.GetPosition.z * (1 / CanvasRef.transform.localScale.z));


                
            }
            else
            {
                mediaRectTransform.anchoredPosition3D = Vector3.zero;
            }
        }
    }
}