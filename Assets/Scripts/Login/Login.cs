﻿using System.Collections;
using System.Linq;
using UnityEngine;
using Appearition.AccountAndAuthentication;
using Appearition;
using Appearition.Common;
using UnityEngine.UI;

[RequireComponent(typeof(LoginData))]
public class Login : MonoBehaviour
{
    public delegate void LoginOccured();

    public static event LoginOccured OnLoginOccured;

    LoginData loginData;
    Coroutine _mainLoginProcess;
    public Text userFeedback;


    public void OnEnable()
    {
        _mainLoginProcess = null;
        loginData = GetComponent<LoginData>();
    }


    public void LoginButtonPress()
    {
        UserPrefs.Instance.RememberMe = loginData.rememberMe;
        if (_mainLoginProcess == null)
            _mainLoginProcess = StartCoroutine(LoginProcess(loginData.email, loginData.password));
    }

    IEnumerator LoginProcess(string username, string password)
    {
        //Launch request
        bool? loginOutcome = default;

        yield return AccountHandler.LoginProcess(username, password, onComplete: isLoginSuccess => loginOutcome = isLoginSuccess);

        //Do some little waiting animation?
        while (!loginOutcome.HasValue)
        {
            if (userFeedback != null)
            {
                userFeedback.color = Color.white;
                userFeedback.text = "Logging in...";
            }

            yield return null;
        }

        //Apply the token.
        if (loginOutcome.Value)
        {
            LoadingPanel.Instance.DisplayMessage("Logging in..", Color.white);


            string userSessionToken = AppearitionGate.Instance.CurrentUser.SessionToken;

            yield return AccountHandler.GetProfileDataProcess();
            //TODO : Delete after confirmation with Dhaya
            //if the only tenant hold by the user isn't the main tenant, switch it.
            //if (AppearitionGate.Instance.CurrentUser.allTenantsAvailable.Count == 1)
            //{
            //    TenantData tenant = AppearitionGate.Instance.CurrentUser.allTenantsAvailable.First();

            //    if (tenant != null && !tenant.tenantKey.Equals(AppearitionGate.Instance.CurrentUser.selectedTenant))
            //    {
            //        //Swap it
            //        AppearitionGate.Instance.CurrentUser.selectedTenant = tenant.tenantKey;
            //        Debug.Log("User uses a different tenant and doesn't have access to primary tenant.");
            //        if (loginData.rememberMe)
            //            PlayerPrefs.SetString(AppearitionConstants.PROFILE_PREFERRED_TENANT, AppearitionGate.Instance.CurrentUser.selectedTenant);
            //    }
            //    //TODO : Check with Dhaya as to what this is
            //    //if the user doesn't have any tenant, then just proceed. The registration process will handle the customer creation.
            //}

            //Do the full check.
            bool isVerificationSuccess = false;
            using (AccountVerificationProcess accountVerificationProcess = new AccountVerificationProcess(username, null, message =>
                {
                    //if (_loadingScreenHolder != null) _loadingScreenHolder.DisplayMessage(message, Color.white);
                }, null,
                failure =>
                {
                    if (userFeedback != null)
                    {
                        userFeedback.color = Color.white;
                        userFeedback.text = failure.ToString();
                    }
                }, complete => isVerificationSuccess = complete))
            {
                yield return accountVerificationProcess.ExecuteMainProcess();

                if (!isVerificationSuccess)
                {
                    if (userFeedback != null)
                    {
                        userFeedback.color = Color.white;
                        userFeedback.text = "Account verification Failed. Try again later or contact our support on the EMS portal.";
                    }

                    yield return new WaitForSeconds(3);
                    LoadingPanel.Instance.HideMessage();
                    _mainLoginProcess = null;
                    yield break;
                }
            }

            //Login successful, storing token.
            if (loginData.rememberMe)
            {
                AppearitionLogger.LogInfo("Remembering the user's token for future use: " + userSessionToken);
                PlayerPrefs.SetString(AppearitionConstants.PROFILE_SESSION_TOKEN_NAME, userSessionToken);
            }
            else
            {
                if (PlayerPrefs.HasKey(AppearitionConstants.PROFILE_SESSION_TOKEN_NAME))
                    PlayerPrefs.DeleteKey(AppearitionConstants.PROFILE_SESSION_TOKEN_NAME);
                if (PlayerPrefs.HasKey(AppearitionConstants.PROFILE_PREFERRED_TENANT))
                    PlayerPrefs.DeleteKey(AppearitionConstants.PROFILE_PREFERRED_TENANT);
            }

            //Check if there are multiple tenants 
            ExtendedProfile profile = null;

            yield return AccountHandler.GetProfileDataProcess(success => profile = success);

            if (profile == null)
            {
                if (userFeedback != null)
                {
                    userFeedback.color = Color.white;
                    userFeedback.text = "Login due to wrong account configuration.";
                }

                _mainLoginProcess = null;
                yield break;
            }

            //Set the user main channel
            SetUserChannelAsCurrentProcess setUserChannelProcess = new SetUserChannelAsCurrentProcess();
            yield return setUserChannelProcess.ExecuteMainProcess();

            //Get AR Provider credentials
            yield return VuforiaLicenseFetcherB.FetchVuforiaLicense();

            OnLoginOccured?.Invoke();
            LoadingPanel.Instance.DisplayMessage("Login success", Color.white);

            yield return new WaitForSeconds(0.5f);

            LoadingPanel.Instance.DisplayMessage("Loading content..", Color.white);

            yield return MediaTypeHandler.GetActiveMediaTypesProcess(null, true);

            LoadingPanel.Instance.HideMessage();
            loginData.userAuthenticationComplete.Invoke();
        }
        else
        {
            if (userFeedback != null)
            {
                userFeedback.color = Color.white;
                userFeedback.text = "Login Failed, please check your credentials";
            }
        }

        _mainLoginProcess = null;
    }

    public void OnDisable()
    {
        if (userFeedback != null)
            userFeedback.text = "";
        LoadingPanel.Instance.HideMessage();
    }

    #region Testing

    void DeleteSavedToken()
    {
        PlayerPrefs.DeleteKey(AppearitionConstants.PROFILE_SESSION_TOKEN_NAME);
    }

    void DeleteSavedTenantKey()
    {
        PlayerPrefs.DeleteKey(AppearitionConstants.PROFILE_PREFERRED_TENANT);
    }

    #endregion
}