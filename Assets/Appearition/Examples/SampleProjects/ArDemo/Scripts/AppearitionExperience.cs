﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;

namespace Appearition.Example
{
    /// <summary>
    /// Class containing a single Appearition Experience.
    /// </summary>
    public class AppearitionExperience : MonoBehaviour
    {
        #region Events

        public delegate void TrackingStateChanged(bool isTracking);

        /// <summary>
        /// Occurs whenever the tracking state of the Ar Experience has changed. Communicates the new tracking state.
        /// </summary>
        public event TrackingStateChanged OnTrackingStateChanged;

        #endregion

        //References
        /// <summary>
        /// ApiData of this experience.
        /// </summary>
        public Asset Data { get; protected set; }

        /// <summary>
        /// Contains all the media objects attached to this experience.
        /// </summary>
        public List<IArMedia> Medias { get; protected set; }

        //Internal Variables
        /// <summary>
        /// Whether or not the current Experience is being tracked.
        /// </summary>
        public bool IsCurrentlyTracking { get; protected set; }

        /// <summary>
        /// Setup a single experience, creating content on the scene and leaving it idle until the its target is found.
        /// </summary>
        /// <param name="newData"></param>
        public void SetupExperience(Asset newData)
        {
            //Create containers
            Medias = new List<IArMedia>();

            if (newData == null)
                return;

            //Firstly, store the ApiData
            Data = newData;

            //Handle the ApiData's content.
            if (Data.mediaFiles != null)
            {
                for (int i = 0; i < Data.mediaFiles.Length; i++)
                {
                    if (string.IsNullOrEmpty(Data.mediaFiles[i].mediaType))
                        continue;

                    //Prepare variables
                    //TODO Fix the issue where the ArMediaId is 0 on Asset/ListByChannel?
                    if (Data.mediaFiles[i].arMediaId == 0)
                        Data.mediaFiles[i].arMediaId = i;

                    GameObject tmpMediaPrefab = (GameObject) Resources.Load(string.Format("{0}/{1}", AppearitionArDemoConstants.ARPREFAB_FOLDER_NAME_IN_RESOURCES, Data.mediaFiles[i].mediaType));
                    IArMedia tmpMedia;
                    GameObject tmpArMediaGameObject;

                    if (tmpMediaPrefab == null)
                    {
                        //Handle by ArMedia type. Try to find the ArMedia class using the MediaType name, and create the content using the class's instruction
                        Type arMediaType = AppearitionArDemoConstants.GetArMediaClassFromMediaType(Data.mediaFiles[i].mediaType);

                        if (arMediaType != null || Data.mediaFiles[i].isDataQuery)
                        {
                            tmpArMediaGameObject = new GameObject(GetMediaNameBasedOnMediaFile(Data.mediaFiles[i]));
                            
                            if(arMediaType == null && Data.mediaFiles[i].isDataQuery)
                                arMediaType = typeof(MiscDataIntegrationArMedia);
                            
                            tmpMedia = (IArMedia) tmpArMediaGameObject.AddComponent(arMediaType);
                        }
                        else
                        {
                            AppearitionLogger.LogError(string.Format("No prefab or type was found for the MediaType {0}. Ignoring said media.", Data.mediaFiles[i].mediaType));
                            continue;
                        }
                    }
                    else
                    {
                        //Handle prefab loading.
                        tmpArMediaGameObject = Instantiate(tmpMediaPrefab);
                        tmpMedia = tmpArMediaGameObject.GetComponent<IArMedia>();

                        if (tmpMedia == null)
                        {
                            Destroy(tmpArMediaGameObject);
                            AppearitionLogger.LogError(string.Format("No IArMedia class was attached to the prefab for the MediaType {0}. " +
                                                                     "Ensure the prefab contains instructions as for how it should be handled.", Data.mediaFiles[i].mediaType));

                            continue;
                        }
                    }

                    //Attach it to this object.
                    tmpArMediaGameObject.transform.SetParent(transform);
                    tmpArMediaGameObject.transform.localPosition = Vector3.zero;
                    tmpArMediaGameObject.transform.localRotation = Quaternion.identity;
                    tmpArMediaGameObject.transform.localScale = Vector3.one * (AppearitionArHandler.Instance.ProviderHandler?.ScaleMultiplier ?? 1f);

                    Medias.Add(tmpMedia);
                    tmpMedia.Setup(this, Data.mediaFiles[i]);

                    //If Cloud Reco, turn it on right away. Otherwise, leave it off until the Ar picks it up.
                    tmpArMediaGameObject.gameObject.SetActive(AppearitionArDemoConstants.USING_CLOUD_RECO);
                }
            }
            if(IsCurrentlyTracking)
            {
                ArProviderTrackingStateChanged(IsCurrentlyTracking);
            }
            ////Finally, disable this object until it's found.
            //gameObject.SetActive(false);
        }

        /// <summary>
        /// Called by the ArProvider handler, informing the experience that the tracking state has changed.
        /// </summary>
        /// <param name="isTracking"></param>
        public void ArProviderTrackingStateChanged(bool? isTracking)
        {
            IsCurrentlyTracking = isTracking.GetValueOrDefault();
            if (Medias != null)
            {
                for (int i = 0; i < Medias.Count; i++)
                    //Medias[i].ChangeDisplayState(isTracking.GetValueOrDefault());
                    Medias[i].OnExperienceTrackingStateChanged(isTracking.GetValueOrDefault());
            }
            if (!isTracking.HasValue)
                AppearitionArHandler.Instance.ArProviderTargetStateChanged(this, AppearitionArHandler.TargetState.Unknown);
            else
                AppearitionArHandler.Instance.ArProviderTargetStateChanged(this,
                    isTracking.Value ? AppearitionArHandler.TargetState.TargetFound : AppearitionArHandler.TargetState.TargetLost);

            if (OnTrackingStateChanged != null)
                OnTrackingStateChanged(isTracking.GetValueOrDefault());
        }

        public void ChangeDisplayState(bool shouldBeVisible)
        {
            for (int i = 0; i < Medias.Count; i++)
                Medias[i].ChangeDisplayState(shouldBeVisible);
        }

        /// <summary>
        /// Uses a common naming system for how a GameObject's name should be called using a MediaFile's ApiData.
        /// </summary>
        /// <param name="mediaFile"></param>
        /// <returns></returns>
        string GetMediaNameBasedOnMediaFile(MediaFile mediaFile)
        {
            return string.Format("Media Id: {0}, Type: {1}", mediaFile.arMediaId, mediaFile.mediaType);
        }
    }
}