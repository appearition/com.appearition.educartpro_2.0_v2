using System.Linq;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using UnityEngine;
using UnityEngine.UI;

namespace Appearition.Example
{
    public class ImageArMedia : BaseCanvasArMedia
    {
        /// <summary>
        /// Image displaying the content of the media.
        /// </summary>
        protected Image ImageRef { get; set; }

        private bool? _isImageDownload;

        public override bool IsMediaReadyAndDownloaded => _isImageDownload.GetValueOrDefault();

        public override void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            base.Setup(associatedExperience, media);

            _isImageDownload = null;

            //Create an image object as a child, which will contain the image itself.
            ImageRef = new GameObject("Image").AddComponent<Image>();
            ImageRef.transform.SetParent(transform);

            ImageRef.type = Image.Type.Simple;
            ImageRef.preserveAspect = !Data.isTracking;

            if (!string.IsNullOrEmpty(Data.text))
            {
                Button attachedButton = ImageRef.gameObject.AddComponent<Button>();
                attachedButton.onClick.AddListener(OnAttachedButtonPressed);
            }

            //Position the image
            SetMediaRectPosition(ImageRef.rectTransform, Data.isTracking);
            ImageRef.rectTransform.localPosition *= (AppearitionArHandler.Instance.ProviderHandler?.ScaleMultiplier ?? 1f);
            ImageRef.rectTransform.localScale *= (2f / 3f);
            ImageRef.gameObject.SetActive(false);
        }

        public override void ChangeDisplayState(bool state)
        {
            //If loaded but no sprite is present, download / load the file!
            if (state && ImageRef.sprite == null && !_isImageDownload.HasValue)
            {
                //No sprite? Download it. While it's loading, use a placeholder.
                Sprite placeholderSprite = AppearitionArDemoConstants.GetImagePlaceholder();

                if (placeholderSprite != null)
                    ImageRef.sprite = placeholderSprite;

                ArTargetHandler.LoadMediaFileContent(ExperienceRef.Data, Data, mediaOutcome =>
                {
                    if (mediaOutcome != null && mediaOutcome.Count > 0)
                    {
                        Sprite tmpSprite = ImageUtility.LoadOrCreateSprite(mediaOutcome.First().Value);
                        if (tmpSprite != null)
                            ImageRef.sprite = tmpSprite;
                        else
                            ImageRef.sprite = null;
                    }
                    else
                    {
                        Debug.Log("Oh no, no sprite =(");
                        ImageRef.sprite = null;
                    }

                    //Flag!
                    _isImageDownload = ImageRef.sprite != null;
                    ImageRef.gameObject.SetActive(ImageRef.sprite != null);
                });
            }

            base.ChangeDisplayState(state);
        }

        /// <summary>
        /// Called when clicking on the Image, if the MediaFile contains a URL in the Text field.
        /// </summary>
        void OnAttachedButtonPressed()
        {
            Application.OpenURL(Data.text);
        }
    }
}