﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using System;
using Appearition;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using System.Linq;
using System.IO;


public class UploadEditedExperience : MonoBehaviour
{
    public EditData editData;
    public void OnPublishButtonPressed(bool shouldPublish)
    {
        StartCoroutine(UploadExperience(shouldPublish));
    }

    IEnumerator UploadExperience(bool publishOnComplete)
    {
        editData = GetComponent<EditData>();
        LoadingPanel.Instance.DisplayMessage("Your experience is being created, please wait..", Color.white);

        #region NAMEUPLOAD
        string nameToUpload = "";
        Debug.Log(editData.expName);
        if (editData.expName != "")
        {
            nameToUpload += Regex.Replace(editData.expName, @"\[.*?\]", "");
        }
        else
        {
            nameToUpload += Regex.Replace(editData.arTarget.name, @"\[.*?\]", "");
        }
        Debug.Log(nameToUpload + "\t" + editData.arTarget.name);
        //Name check
        if (nameToUpload != editData.arTarget.name)
        {
            bool? isExpNameChanged = default;
            ArTargetHandler.ChangeExperienceName(editData.arTarget, nameToUpload, onComplete: result => isExpNameChanged = result);

            while (!isExpNameChanged.HasValue)
            {
                yield return null;
            }

            //Handle failure
            if (!isExpNameChanged.Value)
            {
                yield return EditExperienceProcessFailedHandling();
                yield break;
            }
        }

        #endregion

        #region MARKERUPLOAD
        if (editData.markerToUpload.isDeleted)
        {
            ArTarget tmpArTarget = null;
            bool? isArTargetCreationSuccess = null;
            StartCoroutine(ArTargetHandler.RemoveTargetImageFromArTargetProcess(editData.arTarget, editData.arTarget.targetImages[0], true, onSuccess: tmpOutcome => { tmpArTarget = tmpOutcome; },
                onComplete: isSuccess => { isArTargetCreationSuccess = isSuccess; }));

            while (!isArTargetCreationSuccess.HasValue)
            {
                yield return null;
            }

            //Handle failure
            if (!isArTargetCreationSuccess.Value)
            {
                yield return EditExperienceProcessFailedHandling();
                yield break;
            }
        }
        if (editData.markerToUpload.path != null)
        {
            Texture targetImageTexture = null;
            string markerPath = editData.markerToUpload.path;
            if (!string.IsNullOrWhiteSpace(markerPath))
            {
                targetImageTexture = ImageUtility.LoadOrCreateTexture(markerPath);
            }


            //Upload the target image!
            ArTarget tmpArTarget = null;
            bool? isArTargetCreationSuccess = null;
            StartCoroutine(ArTargetHandler.ReplaceTargetImageProcess(editData.arTarget, editData.arTarget.targetImages[0], "tmpTarget.jpg", targetImageTexture, true, onSuccess: tmpOutcome => { tmpArTarget = tmpOutcome; },
                onComplete: isSuccess => { isArTargetCreationSuccess = isSuccess; }));

            while (!isArTargetCreationSuccess.HasValue)
            {
                yield return null;
            }

            //Handle failure
            if (!isArTargetCreationSuccess.Value)
            {
                yield return EditExperienceProcessFailedHandling();
                yield break;
            }
        }

        #endregion

        #region MEDIAUPLOAD
        if (editData.mediaToUpload.path != null)
        {
            //MediaUpload mu = GetComponentInChildren<MediaUpload>();

            if (editData.mediaToUpload.path != "")
            {
                MediaToUpload mu = editData.mediaToUpload;
                bool? isMediaRemoved = null;
                Debug.Log("Media is Dirty");
                ArTargetHandler.RemoveMediaFromArTarget(editData.arTarget, editData.arTarget.mediaFiles.First(), onComplete: isSuccess => { isMediaRemoved = true; });
                while (!isMediaRemoved.HasValue)
                {
                    yield return null;
                }
                string mediaType = mu.mediaType;
                Debug.Log(mediaType);

                if (mediaType.Equals("weblink", StringComparison.InvariantCultureIgnoreCase))
                {
                    MediaFile wbMediaFile = new MediaFile();
                    wbMediaFile.mediaType = mediaType;
                    if (!mu.path.Contains(@"https://"))
                    {
                        if (mu.path.Contains(@"http://"))
                        {
                            mu.path.Replace(@"http://", @"https://");
                        }
                        else
                        {
                            mu.path = @"https://" + mu.path;
                        }
                    }
                    wbMediaFile.url = mu?.path;
                    bool? isExperienceCreationSuccess = null;

                    Debug.Log("Launching upload media !");
                    ArTargetHandler.AddMediaToExistingExperience(editData.arTarget, wbMediaFile, false, (arTargetOutcome, mediaOutcome) => { wbMediaFile = mediaOutcome; },
                        onComplete: isSuccess => { isExperienceCreationSuccess = isSuccess; });

                    while (!isExperienceCreationSuccess.HasValue)
                    {
                        yield return null;
                    }

                    //Handle failure
                    if (!isExperienceCreationSuccess.Value)
                    {
                        yield return EditExperienceProcessFailedHandling();
                        yield break;
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(mediaType))
                    {
                        object mediaToUpload = null;
                        string mediaPath = mu.path;
                        if (!string.IsNullOrEmpty(mediaPath))
                        {
                            Debug.Log("Loading media..");
                            //Fetch the file!
                            bool isMediaDoneFetching = false;

                            yield return FileHandler.LoadBytesFromFileProcess(mediaPath, tmpOutcome =>
                            //AppearitionGate.LoadObjectFromPath(mediaPath, tmpOutcome =>
                            {
                                mediaToUpload = tmpOutcome;
                                isMediaDoneFetching = true;
                            });

                            while (!isMediaDoneFetching)
                            {
                                yield return null;
                            }

                            Debug.Log("Media done loading !" + mediaToUpload);
                        }

                        //Make a media to upload
                        MediaFile tmpMediaToUpload = new MediaFile();
                        string mediaExtension = "";

                        //Ignore None for mediatype
                        MediaType tmpMediaType = MediaTypeHandler.ActiveMediaTypes.First(o => o.Name.Equals(mediaType, StringComparison.InvariantCultureIgnoreCase));

                        if (mediaToUpload != null && tmpMediaType != null)
                        {
                            mediaExtension = System.IO.Path.GetExtension(mu.path);
                            tmpMediaToUpload.mimeType = tmpMediaType.GetMimeTypeForGivenExtension(mediaExtension);
                        }

                        tmpMediaToUpload.fileName = "TmpFile" + mediaExtension;
                        if (tmpMediaType != null)
                            tmpMediaToUpload.mediaType = tmpMediaType.Name;
                        tmpMediaToUpload.isPrivate = false;

                        //Upload the experience!
                        bool? isExperienceCreationSuccess = null;

                        Debug.Log("Launching upload media !");
                        ArTargetHandler.AddMediaToExistingExperience(editData.arTarget, tmpMediaToUpload, mediaToUpload, false, (arTargetOutcome, mediaOutcome) => { tmpMediaToUpload = mediaOutcome; },
                            onComplete: isSuccess => { isExperienceCreationSuccess = isSuccess; });

                        while (!isExperienceCreationSuccess.HasValue)
                        {
                            yield return null;
                        }

                        if (isExperienceCreationSuccess.Value)
                        {
                            Debug.Log("Media successfully uploaded !");
                        }
                        //Handle failure
                        else
                        {
                            yield return EditExperienceProcessFailedHandling();
                            yield break;
                        }

                    }
                }
            }
        }
        #endregion

        if(publishOnComplete)
        {
            LoadingPanel.Instance.DisplayMessage("Publishing Your Experience", Color.white);            yield return ArTargetHandler.PublishExperienceProcess(editData.arTarget, onSuccess => { LoadingPanel.Instance.DisplayMessage("Experience Update Succeeded", Color.white);
            }, onFailure => { LoadingPanel.Instance.DisplayMessage("Publish Failed", Color.white); });
        }
        bool isRefreshComplete = false;

        //Reset
        LoadingPanel.Instance.HideMessage();
        ExperienceSuccessPanel.Instance.Activate(3f);
        gameObject.SetActive(false);

        //OnPublishPanelToSwitch.DisplaySuccessPanel(isMarker, false);
    }

    /// <summary>
    /// Should be called if the edit process failed. Handles what needs to happen in case of failure.
    /// After starting this process, stop the main one.
    /// </summary>
    /// <param name="message"></param>
    /// <returns></returns>
    IEnumerator EditExperienceProcessFailedHandling()
    {
        ExperienceFailPanel.Instance.Activate(3f);
        yield return null;
        gameObject.SetActive(false);
    }

}
