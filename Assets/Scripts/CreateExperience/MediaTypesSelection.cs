﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition.Common;

[System.Serializable]
public class MediaTypes
{
    public string mediaName;
    public GameObject mediaUploadBtn;

}


public class MediaTypesSelection : MonoBehaviour
{
    public Dropdown dropdown;
    GameObject previousActivatedObject;
    [Header("Media Types")]
    public List<MediaTypes> mediaTypesList = new List<MediaTypes>();
    public MediaFile mf;
    bool hasPopulated = false;
    // Start is called before the first frame update
    void Start()
    {
        if (!hasPopulated)
            PopulateList();
        //Debug.Log("Populating");
    }

    public void Setup(MediaFile m)
    {
        Debug.Log(m.arMediaId);
        mf = m;
        PopulateList();
        hasPopulated = true;
        Debug.Log(mf.mediaType);
        if (mf.mediaType.ToLower() == "image")
        {
            dropdown.value = 0;
            OnDropDownMediaNameChanged(0);
        }
        if (mf.mediaType.ToLower() == "model")
        {
            dropdown.value = 1;
            OnDropDownMediaNameChanged(1);
        }
        if (mf.mediaType.ToLower() == "video")
        {
           // dropdown.value = 2;
           // OnDropDownMediaNameChanged(2);
        }
        if (mf.mediaType.ToLower() == "audio")
        {
            //dropdown.value = 3;
            //OnDropDownMediaNameChanged(3);
        }
        if (mf.mediaType.ToLower() == "assetbundle_android" || mf.mediaType.ToLower() == "assetbundle_ios")
        {
            //dropdown.value = 4;
            //OnDropDownMediaNameChanged(4);
        }
        if (mf.mediaType.ToLower() == "weblink")
        {
            dropdown.value = 5;
            OnDropDownMediaNameChanged(5);
        }


    }

    void PopulateList()
    {
        List<string> typeNames = new List<string>();
        foreach (MediaTypes mt in mediaTypesList)
        {
            typeNames.Add(mt.mediaName);
        }
        dropdown.AddOptions(typeNames);
        mediaTypesList[0].mediaUploadBtn.SetActive(true);
        previousActivatedObject = mediaTypesList[0].mediaUploadBtn;
    }

    public void OnDropDownMediaNameChanged(int value)
    {
        previousActivatedObject.SetActive(false);
        mediaTypesList[value].mediaUploadBtn.SetActive(true);
        previousActivatedObject = mediaTypesList[value].mediaUploadBtn;
        DisplayExistingMedia(mf);
    }

    public void DisplayExistingMedia(MediaFile mf)
    {
        //ADD Code to display Media Here
    }

    // Update is called once per frame
    void Update()
    {

    }
}
