﻿using System;
using System.Collections;
using System.Collections.Generic;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common.ListExtensions;
using Appearition.Example;
using UnityEngine;
using Vuforia;

namespace Appearition.ArDemo.Vuforia
{
    public class VuforiaMarkerlessProviderHandler : MonoBehaviour, IArProviderHandler
    {
        public enum MarkerlessPlacementState
        {
            LookingForGround,
            Loading,
            Idle
        }
        
        //References
        [SerializeField] PlaneFinderBehaviour _planeFinder;
        [SerializeField] ContentPositioningBehaviour _contentPositioning;
        [SerializeField] AnchorBehaviour _anchorTemplate;
        PositionalDeviceTracker _positionalDeviceTracker;
        SmartTerrain _smartTerrain;
        List<VuforiaTargetEventHandler> _activeExperiences = new List<VuforiaTargetEventHandler>();
        List<AppearitionExperience> _experiencesBeingLoaded = new List<AppearitionExperience>();
        List<GameObject> _contentReadyToBePlaced = new List<GameObject>();
        public MarkerlessData mData;

        //Internal Variables
        public float ScaleMultiplier => 1f;
        public float ManipulationMultiplier => 0.01f;
        public float ManipulationScaleMultiplier => 300f;
        protected virtual Vector2 ClippingPlanes => new Vector2(0.1f, 100f);

        static float _cameraDepthBackup = -99999f;

        protected bool _isActive;

        Camera _providerCamera;



        public Camera ProviderCamera
        {
            get
            {
                if (_providerCamera == null)
                    _providerCamera = VuforiaBehaviour.Instance.GetComponent<Camera>();
                return _providerCamera;
            }
        }

        public bool IsArProviderInitialized { get; }

        /// <summary>
        /// Whether Vuforia's engine is active or not.
        /// </summary>
        public bool IsArProviderActive => VuforiaBehaviour.Instance != null && VuforiaBehaviour.Instance.enabled && _isActive;

        public bool IsScanning { get; private set; }

        public bool DisableScanningOnTargetFound => true;

        public bool UseExtendedTracking => true;

        public MarkerlessPlacementState CurrentState { get; private set; }
        
        public AppearitionExperience LastSelectedExperience { get; private set; }
        
        #region Init

        void Awake()
        {
            //HACK, enable Vuforia Camera during initialization.
            if (ProviderCamera.depth > -100)
            {
                _cameraDepthBackup = ProviderCamera.depth;
                ProviderCamera.depth = -999f;
            }

            ProviderCamera.enabled = true;

            if (_planeFinder != null)
            {
                var contentPlacer = _planeFinder.GetComponent<ContentPositioningBehaviour>();
                if (contentPlacer != null)
                    contentPlacer.OnContentPlaced.AddListener(OnMarkerlessContentPlaced);
            }

            VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
            DeviceTrackerARController.Instance.RegisterTrackerStartedCallback(OnTrackerStarted);
        }

        protected virtual IEnumerator Start()
        {
            yield return new WaitForSeconds(1);

            //Restore camera
            //ProviderCamera.enabled = false;
            if (_cameraDepthBackup > -9999)
                ProviderCamera.depth = _cameraDepthBackup;
        }

        void OnVuforiaStarted()
        {
            if (!_isActive)
                return;
            
            var stateManager = TrackerManager.Instance.GetStateManager();

            // Check trackers to see if started and start if necessary
            _positionalDeviceTracker = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();
            _smartTerrain = TrackerManager.Instance.GetTracker<SmartTerrain>();

            if (_positionalDeviceTracker != null && _smartTerrain != null)
            {
                if (!_positionalDeviceTracker.IsActive)
                {
                    Debug.LogError("The Ground Plane feature requires the Device Tracker to be started. " +
                                   "Please enable it in the Vuforia Configuration or start it at runtime through the scripting API.");
                    return;
                }

                if (_positionalDeviceTracker.IsActive && !_smartTerrain.IsActive)
                    _smartTerrain.Start();
            }
            else
            {
                if (_positionalDeviceTracker == null)
                    Debug.Log("PositionalDeviceTracker returned null. GroundPlane not supported on this device.");
                if (_smartTerrain == null)
                    Debug.Log("SmartTerrain returned null. GroundPlane not supported on this device.");
            }
        }

        void OnTrackerStarted()
        {
            _positionalDeviceTracker = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();
            _smartTerrain = TrackerManager.Instance.GetTracker<SmartTerrain>();

            if (_positionalDeviceTracker != null && _smartTerrain != null)
            {
                if (!_positionalDeviceTracker.IsActive)
                {
                    Debug.LogError("The Ground Plane feature requires the Device Tracker to be started. " +
                                   "Please enable it in the Vuforia Configuration or start it at runtime through the scripting API.");
                    return;
                }

                if (!_smartTerrain.IsActive)
                    _smartTerrain.Start();

                Debug.Log("PositionalDeviceTracker is Active?: " + _positionalDeviceTracker.IsActive +
                          "\nSmartTerrain Tracker is Active?: " + _smartTerrain.IsActive);
            }
        }

        #endregion

        public IEnumerator SetupLicense()
        {
            yield break;
        }

        public void ChangeArProviderActiveState(bool shouldBeEnabled)
        {
            _isActive = shouldBeEnabled;
            CurrentState = _isActive ? MarkerlessPlacementState.LookingForGround : MarkerlessPlacementState.Idle;
            LastSelectedExperience = null;
            
            if (VuforiaManager.Instance != null)
            {
                //ProviderCamera.enabled = shouldBeEnabled;

                if (!VuforiaManager.Instance.Initialized)
                    InitializeVuforia();
                else if (VuforiaBehaviour.Instance.enabled != shouldBeEnabled && VuforiaManager.Instance.Initialized)
                    VuforiaBehaviour.Instance.enabled = shouldBeEnabled;
            }
            
            if(!shouldBeEnabled && _planeFinder != null)
                _planeFinder.gameObject.SetActive(false);

            if (shouldBeEnabled)
            {
                //ProviderCamera.nearClipPlane = ClippingPlanes.x;
                //ProviderCamera.farClipPlane = ClippingPlanes.y;
                StartCoroutine(LoadSelectedExperience(mData.markerLessAsset));
            }

        }

        public virtual void InitializeVuforia()
        {
            _isActive = true;
            VuforiaBehaviour.Instance.enabled = true;
            VuforiaRuntime.Instance.InitVuforia();
        }

        public void ChangeScanningState(bool shouldScan)
        {
            if (_planeFinder != null)
                _planeFinder.gameObject.SetActive(shouldScan);
            if (_contentPositioning != null && shouldScan)
            {
                if (_contentPositioning.AnchorStage != null)
                {
                    AppearitionExperience tmpOngoingExperience = _contentPositioning.GetComponent<AppearitionExperience>();
                    if (tmpOngoingExperience == null && !string.IsNullOrEmpty(_contentPositioning.AnchorStage.gameObject.scene.name))
                        Destroy(_contentPositioning.AnchorStage.gameObject);
                }

                AnchorBehaviour anchor = Instantiate(_anchorTemplate, transform);
                _contentPositioning.AnchorStage = anchor;
            }

            if (shouldScan)
                ResetTrackers();
            IsScanning = shouldScan;
        }

        /// <summary>
        /// This method stops and restarts the PositionalDeviceTracker.
        /// It is called by the UI Reset Button and when RELOCALIZATION status has
        /// not changed for 10 seconds.
        /// </summary>
        public void ResetTrackers()
        {
            if (!_isActive)
                return;
            
            _smartTerrain = TrackerManager.Instance.GetTracker<SmartTerrain>();
            _positionalDeviceTracker = TrackerManager.Instance.GetTracker<PositionalDeviceTracker>();
            CurrentState = MarkerlessPlacementState.LookingForGround;

            // Stop and restart trackers
            if (_smartTerrain != null && _positionalDeviceTracker != null)
            {
                _smartTerrain.Stop(); // stop SmartTerrain tracker before PositionalDeviceTracker
                _positionalDeviceTracker.Reset();
                _smartTerrain.Start(); // start SmartTerrain tracker after PositionalDeviceTracker
            }
        }

        #region Experience

        /// <summary>
        /// Occurs when opening the AR mode. Start loading the asset in advance, but.. hidden!
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        public IEnumerator LoadSelectedExperience(Asset asset)
        {
            bool? isLoaded = default;
            var experience = AppearitionArHandler.CreateExperienceFromAsset(asset, onComplete => isLoaded = onComplete);
            experience.gameObject.SetActive(false);
            int index = _experiencesBeingLoaded.Count;
            _experiencesBeingLoaded.Add(experience);
            LastSelectedExperience = experience;

            Debug.LogError("Started !");
            while (!isLoaded.HasValue)
                yield return null;

            //Make sure it still exists..
            if (_experiencesBeingLoaded.Count == 0 || _experiencesBeingLoaded.HiziAll(o => o.Data == null || !o.Data.assetId.Equals(asset?.assetId, StringComparison.InvariantCultureIgnoreCase)))
            {
                Debug.LogError("=[");
                yield break;
            }

            //Handle load failure
            if (!isLoaded.GetValueOrDefault())
            {
                OnValidateAppearitionExperience(false);
                Debug.LogError("=[");
                yield break;
            }

            //Wait until the item is ready to be placed..
            while (_contentReadyToBePlaced.Count <= index)
                yield return null;

            //Make sure it still exists..
            if (_experiencesBeingLoaded.Count == 0 || _experiencesBeingLoaded.HiziAll(o => o.Data == null || !o.Data.assetId.Equals(asset?.assetId, StringComparison.InvariantCultureIgnoreCase)))
            {
                Debug.LogError("=[");
                yield break;
            }

            Debug.LogError("=3");

            //Place it!
            var holder = _contentReadyToBePlaced[index];

            holder.transform.SetParent(transform);
            yield return null;
            experience.transform.SetParent(holder.transform);
            experience.gameObject.SetActive(true);
            experience.transform.localPosition = Vector3.zero;
            experience.transform.localRotation = Quaternion.identity;
            experience.transform.localScale = Vector3.one;

            VuforiaTargetEventHandler eventHandler = holder.GetComponent<VuforiaTargetEventHandler>();
            if (eventHandler == null)
                holder.gameObject.AddComponent<VuforiaTargetEventHandler>().Setup(experience, holder.GetComponent<AnchorBehaviour>());
            else
                eventHandler.Setup(experience, holder.GetComponent<AnchorBehaviour>());

            _contentReadyToBePlaced.RemoveAt(index);
            _experiencesBeingLoaded.RemoveAt(index);
            _activeExperiences.Add(eventHandler);

            Debug.LogError("Ready !");
            ChangeScanningState(false);

            //for(int i = 0; i < _activeExperiences.Count; i++)
            if (eventHandler != null)
                eventHandler.OnTrackableStateChanged(TrackableBehaviour.Status.NO_POSE, TrackableBehaviour.Status.TRACKED);
                
            CurrentState = MarkerlessPlacementState.Idle;
            //Let ArHandler know about it
            AppearitionArHandler.ArProviderTargetLoaded(true);
            AppearitionArHandler.Instance.ArProviderTargetStateChanged(experience, AppearitionArHandler.TargetState.TargetFound);
        }

        /// <summary>
        /// Occurs when the asset is ready to be placed.
        /// </summary>
        /// <param name="contentHolder"></param>
        private void OnMarkerlessContentPlaced(GameObject contentHolder)
        {
            //Let the loader know that it should be placing the goods
            if (_experiencesBeingLoaded.Count > _contentReadyToBePlaced.Count)
            {
                Debug.LogError($"Added ! {_experiencesBeingLoaded.Count} vs {_contentReadyToBePlaced.Count}");
                if (CurrentState == MarkerlessPlacementState.LookingForGround)
                    CurrentState = MarkerlessPlacementState.Loading;
                _contentReadyToBePlaced.Add(contentHolder);
            }
        }

        /// <summary>
        /// Occurs after the experience is loaded; contains whether it was successful or not.
        /// </summary>
        /// <param name="isValid"></param>
        void OnValidateAppearitionExperience(bool isValid)
        {
            //If the experience picked up was not valid, clear content and restart scanning.
            if (!isValid)
            {
                ClearTargetsBeingTracked();
                ChangeScanningState(true);
            }
        }

        #endregion

        #region Cleanup

        public void ClearTargetsBeingTracked(bool shouldRestartScanning = true)
        {
            //Find the target currently active, and unload its content.
            for (int i = _activeExperiences.Count - 1; i >= 0; i--)
            {
                if (_activeExperiences[i] != null)
                    Destroy(_activeExperiences[i].gameObject);
            }

            _activeExperiences.Clear();

            for (int i = _contentReadyToBePlaced.Count - 1; i >= 0; i--)
            {
                if (_contentReadyToBePlaced[i] != null)
                    Destroy(_contentReadyToBePlaced[i].gameObject);
            }

            _contentReadyToBePlaced.Clear();

            for (int i = _experiencesBeingLoaded.Count - 1; i >= 0; i--)
            {
                if (_experiencesBeingLoaded[i] != null)
                    Destroy(_experiencesBeingLoaded[i].gameObject);
            }

            _experiencesBeingLoaded.Clear();

            //Let the ArHandler know about the new Object's state.
            if (IsArProviderActive)
                AppearitionArHandler.Instance.ArProviderTargetStateChanged(null, AppearitionArHandler.TargetState.None);
        }

        public void RefocusCamera()
        {
            CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
        }

        #endregion
    }
}