﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition.Common;
using UnityEngine.Video;

public class VideoMediaObject : MonoBehaviour
{
    //public string markerPath;
   
    public GameObject uploadPanel;
    public CreateData createData;
    Sprite emsMarkerCache;

    public VideoPlayer videoPlayer;

    
    public void GetMediaVideo()
    {
        Debug.Log("True");
        NativeGallery.Permission permission = NativeGallery.GetVideoFromGallery((path) =>
        {
            Debug.Log("Video path: " + path);
            if (path != null)
            {
                var filePath = string.Format("file://{0}", path);
                videoPlayer.url = filePath;

                createData.mediaToUpload.path = path;
                createData.mediaToUpload.mediaType = "video";
                videoPlayer.gameObject.SetActive(true);
                uploadPanel.SetActive(false);
            }

        }, "Select a Video");

    }

    public void Delete()
    {
        createData.mediaToUpload.mediaType = "";
        createData.mediaToUpload.path = "";
        uploadPanel.SetActive(true);
        videoPlayer.gameObject.SetActive(false);
    }
}
