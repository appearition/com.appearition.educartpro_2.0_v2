﻿using Appearition.ArTargetImageAndMedia;
using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition.Common;
using System.Linq;
using Appearition.ArTargetImageAndMedia.API;

public class UserExperienceCardSetup : MonoBehaviour
{
    //References
    

    public RectTransform _mainHolder;
    public RectTransform _mainImageRectTransform;
    public RectTransform _imageHolder;


    public Image targetImage;
    public Text experienceName;
    public Text createdDate;
    public GameObject markerTag;
    public GameObject markerlessTag;

    public Button scanButton;
    public GameObject editButton;
    public GameObject lockButton;

    //public Asset data;


    public void Setup(ArTarget asset)
    {
        GetComponent<UserExperienceCardData>().data = asset;

        experienceName.text = Regex.Replace(asset.name, @"\[.*?\]", "");
        editButton.SetActive(asset.canEditExperience);
        lockButton.SetActive(!asset.canEditExperience);
        if (asset.IsMarkerless)
        {
            markerTag.SetActive(false);
            markerlessTag.SetActive(true);
            scanButton.onClick.AddListener(OpenMarkerlessScan);
        }
        else
        {
            markerTag.SetActive(true);
            markerlessTag.SetActive(false);
            scanButton.onClick.AddListener(OpenMarkerScan);
        }

        if (asset.ModifiedUtcDate.Date == DateTime.Today)
            createdDate.text = "Today";
        else if (DateTime.Today - asset.ModifiedUtcDate.Date == TimeSpan.FromDays(1))
            createdDate.text = "Yesterday";
        else
            createdDate.text = asset.ModifiedUtcDate.ToShortDateString();
        StartCoroutine(FetchTargetImage());

        if (_mainHolder != null)
            _mainHolder.sizeDelta = new Vector2(1000, _mainHolder.sizeDelta.y);
    }
    void OpenMarkerlessScan()
    {
        GetComponent<EnableMarkerlessScanMode>().SwitchToMarkerless();
    }

    void OpenMarkerScan()
    {
        GetComponent<EnableMarkerScanMode>().SwitchToMarker();
    }

    IEnumerator FetchTargetImage()
    {

        ArTarget asset = GetComponent<UserExperienceCardData>().data;
        if (asset.targetImages.Count() > 0)
        {
            //Assume the targetimage has been downloaded already.
            string targetImagePath = ArTargetHandler.GetPathToTargetImage(asset, asset.targetImages[0]);

            if (!File.Exists(targetImagePath))
            {
                yield return ArTargetHandler.DownloadAssetsContent(new Asset[] { asset }, true, false, false);
                targetImagePath = ArTargetHandler.GetPathToTargetImage(asset, asset.targetImages[0]);
            }

            Sprite tmpTex = null;

            yield return ImageUtility.LoadOrCreateSpriteAsync(targetImagePath, asset.targetImages[0].checksum, sprite => tmpTex = sprite);

            asset.targetImages[0].image = tmpTex;
            //asset.targetImages[0].image = ImageUtility.LoadOrCreateTexture(targetImagePath);
            if (tmpTex != null)
            {
                targetImage.sprite = tmpTex;
            }

            _mainImageRectTransform.anchorMin = _mainImageRectTransform.anchorMax = Vector2.one / 2;

            float textureWidth = targetImage.sprite.texture.width;
            float textureHeight = targetImage.sprite.texture.height;
            float holderRatio = _imageHolder.rect.width / _imageHolder.rect.height;
            float imageRatio = textureWidth / textureHeight;
            if (holderRatio > imageRatio)
            {
                //_mainImageRectTransform.anchorMin = new Vector2(0, 0.5f);
                //_mainImageRectTransform.anchorMin = new Vector2(1, 0.5f);

                _mainImageRectTransform.sizeDelta = new Vector2(_mainHolder.sizeDelta.x, textureHeight / textureWidth * _mainHolder.sizeDelta.x);
            }
            else
            {
                //_mainImageRectTransform.anchorMin = new Vector2(0.5f, 0);
                //_mainImageRectTransform.anchorMin = new Vector2(0.5f, 1);

                //_mainImageRectTransform.sizeDelta = new Vector2(_mainImageRectTransform.sizeDelta.x / _mainImageRectTransform.sizeDelta.y * _imageHolder.sizeDelta.y, 0);
                _mainImageRectTransform.sizeDelta = new Vector2(textureWidth / textureHeight * _mainHolder.sizeDelta.y, _mainHolder.sizeDelta.y);
            }

        }
    }

}
