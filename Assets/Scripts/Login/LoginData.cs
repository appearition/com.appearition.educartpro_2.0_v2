﻿using UnityEngine;
using UnityEngine.Events;

public class LoginData : MonoBehaviour
{
    public string email;
    public string password;

    public bool isEmailValid;
    public bool isPasswordValid;
    public bool rememberMe;

    public UnityEvent userAuthenticationComplete;

    private void Start()
    {
        userAuthenticationComplete.AddListener(Reset);
    }

    private void OnDisable()
    {
        Reset();
    }

    private void Reset()
    {
        email = "";
        password = "";
        isEmailValid = false;
        isPasswordValid = false;
        rememberMe = false;
    }
}
