﻿using System.Collections.Generic;
using Appearition.API;
using Appearition.Common;
using UnityEngine.Serialization;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Asset/DataByAsset/0 , where 0 is Channel ID.
    /// </summary>
    [System.Serializable]
    public class Asset_DataByAsset : BaseApiPost
    {
        public override int ApiVersion => 2;

        public string Data;

        /// <summary>
        /// Post ApiData
        /// </summary>
        [System.Serializable]
        public class PostData
        {
            public string assetId;
            public int arMediaId;
            public List<Setting> parameters;
        }

        public override Dictionary<string, string> JsonReplaceKvp => new Dictionary<string, string> {{ "parameters", "params"}};
    }
}