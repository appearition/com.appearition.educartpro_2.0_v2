// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: Asset_ListByChannel.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using Appearition.API;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// Class holder for the Asset / List By Channel API
    /// </summary>
    [System.Serializable][System.Obsolete]
    public class Asset_ListByChannel : BaseApiGet
    {
        //Variables
        public ApiData Data;

        //SubClasses
        [System.Serializable]
        public class ApiData
        {
            //Variables
            public Asset[] assets;
        }
    }
}