﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


public class SwitchPanel : MonoBehaviour
{
    [TextArea]
    public string comment;

    [Tooltip("Switches Pannel on start")]
    public bool switchOnStart;

    [Tooltip("automatically binds an event to the button")]
    public bool switchOnButtonClick;

    public List<GameObject> panelToActivate = new List<GameObject>();
    public List<GameObject> panelToDeactivate = new List<GameObject>();

    public UnityEvent OnSwitch;

    Button _button;
    public void Start()
    {
        if(switchOnStart == true)
        {
            SwitchPanels();
        }

        if(switchOnButtonClick == true)
        {
            AddSwitchPanelsToButton();
        }
        
    }

    public void AddSwitchPanelsToButton()
    {
        _button = gameObject.GetComponent<Button>();
        _button.onClick.AddListener(SwitchPanels);
    }

    public void SwitchPanels()
    {
        TurnPanelsOn();
        TurnPanelsOff();
        OnSwitch.Invoke();
    }

    public void TurnPanelsOn()
    {
        foreach (GameObject pta in panelToActivate)
        {
            if (pta != null)
                pta.SetActive(true);
        }

    }

    public void TurnPanelsOff()
    {
        foreach (GameObject ptda in panelToDeactivate)
        {
            if(ptda != null)
                ptda.SetActive(false);
        }
    }

}
