﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition.Common;

public class WeblinkMediaObject : MonoBehaviour
{
    public InputField inputfield;
    public CreateData createData;

    public void GetMediaWeblink()
    {
        Debug.Log("CALLING");
        createData.mediaToUpload.path = inputfield.text;
        createData.mediaToUpload.mediaType = "weblink";
    }
}
