﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefreshUserExperience : MonoBehaviour
{
    public SetupMyProfile dMyExp;
    Vector2 scrollViewPosition;
    public void OnScrollViewMoved(Vector2 value)
    {

        scrollViewPosition = value;
    }

    public void UserReleaseDrag()
    {

        if (scrollViewPosition.y > 1.02f)
        {
            dMyExp.RefreshUserData();
        }
        if (scrollViewPosition.y < 0f)
        {
            dMyExp.AddNextSet();
        }
    }
}
