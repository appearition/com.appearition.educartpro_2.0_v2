﻿namespace Appearition.ContentLibrary
{
    public static class ContentLibraryConstants
    {
        public const string NO_PROVIDER_NAME_PROVIDED = "No provider name has been provided as part of this process.";
        public const string GET_CONTENT_ITEM_SUCCESS_OFFLINE = "Content item of the channel {0} have been successfully fetched offline!";
        public const string GET_CONTENT_ITEM_SUCCESS = "Content item of the channel {0} have been successfully fetched!";
        public const string GET_CONTENT_ITEM_FAILURE = "An error occured when trying to fetch the content item from the channel of id {0}";



        public const string CONTENT_FILE_DOWNLOAD_NO_FILE_GIVEN = "The file to download is null.";
    }
}