// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: Learn_Tracking.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using Appearition.API;

namespace Appearition.Learn.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Learn/Tracking/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class Learn_Tracking : BaseApiPost
    {
        public override int ApiVersion
        {
            get { return 2; }
        }

        //Variables
        public string Data;

        /// <summary>
        /// Post ApiData
        /// </summary>
        [System.Serializable]
        public class PostData : LearningSession
        {
        }
    }
}