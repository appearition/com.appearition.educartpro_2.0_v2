﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition;

public class EnableCreateButton : MonoBehaviour
{
    public Button createButton;

    private void OnEnable()
    {
        if(UserRoleConstants.HasPermissionToCreateAndEditAnyExperiences || UserRoleConstants.HasPermissionToCreateAndEditOwnedExperiences)
        {
            createButton.interactable = true;
        }
        else
        {
            createButton.interactable = false;
        }
    }

}
