﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition.ArTargetImageAndMedia;

public class EnableActionSheet : MonoBehaviour
{

    public GameObject ActionSheetMarker;
    public GameObject ActionSheetMarkerless;

    Asset data;

    public void Start()
    {
        data = GetComponent<ExperienceCardData>().data;
    }
    public void OnKebabMenuPressed()
    {
        
        if (data.IsMarkerless)
        {
           
            ActionSheetMarker.SetActive(false);
            ActionSheetMarkerless.SetActive(true);
            ActionSheetMarkerless.GetComponent<ActionSheetData>().asset = data;
        }
        else
        {
            
            ActionSheetMarker.SetActive(true);
            ActionSheetMarkerless.SetActive(false);
            ActionSheetMarker.GetComponent<ActionSheetData>().asset = data;
        }
    }

}
