﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableMarkerScanMode : MonoBehaviour
{
    public GameObject MarkerObject;
    public GameObject closePanel;
    public void SwitchToMarker()
    {
        MarkerObject.SetActive(true);
        closePanel.SetActive(false);
    }

}
