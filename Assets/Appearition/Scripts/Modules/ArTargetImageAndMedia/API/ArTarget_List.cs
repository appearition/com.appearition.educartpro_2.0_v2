// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: ArTarget_List.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Appearition.API;
using UnityEngine;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/ArTarget/List/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class ArTarget_List : BaseApiPost
    {
        public override int ApiVersion => 2;

        [System.Serializable]
        public class QueryContent : Asset_List.QueryContent
        {
            /// <summary>
            /// AssetId, basically.
            /// </summary>
            [SerializeField] string ArTargetKey;
            /// <summary>
            /// Published, Unpublished, or blank.
            /// </summary>
            public string Status;
            public bool MarketOnly;
            public bool ExcludeMarket;
            
            public QueryContent()
            { 
            }

            public QueryContent(QueryContent cc) : base(cc)
            {
                ArTargetKey = string.IsNullOrEmpty(cc.AssetId) ? cc.ArTargetKey : cc.AssetId;
                Status = cc.Status;
                MarketOnly = cc.MarketOnly;
                ExcludeMarket = cc.ExcludeMarket;
            }
        }

        [System.Serializable]
        public class QueryOutcome
        {
            public List<ArTarget> ArTargets = new List<ArTarget>();
            public int Page;
            public int TotalRecords;
            public int RecordsPerPage;
            public List<string> StatusOptions;
        }

        public QueryOutcome Data;

        /// <summary>
        /// Post ApiData
        /// </summary>
        [System.Serializable]
        public class PostData : QueryContent
        {
            public PostData(QueryContent cc) : base(cc)
            {
            }
        }
    }
}