﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition;
using UnityEngine.UI;
using Appearition.AccountAndAuthentication;

public class ProfileSelectTenant : MonoBehaviour
{
    public SetupMyProfile sumProfile;
    public GameObject parent;
    public Text tenantName;
    string tenantKey;

    public void Setup(TenantData tData)
    {
        tenantName.text = tData.tenantName;
        tenantKey = tData.tenantKey;
    }

    public void SelectTenantTogglePressed(bool value)
    {
        if (value == true)
        {
            Debug.Log(tenantKey);
            AppearitionGate.Instance.CurrentUser.selectedTenant = tenantKey;
            if (UserPrefs.Instance.RememberMe)
            {
                PlayerPrefs.SetString(AppearitionConstants.PROFILE_PREFERRED_TENANT, tenantKey);
            }
            LoadingPanel.Instance.DisplayMessage("Switching Tenant", Color.white);
            sumProfile.RefreshUserData();
            parent.SetActive(false);
        }
    }

}
