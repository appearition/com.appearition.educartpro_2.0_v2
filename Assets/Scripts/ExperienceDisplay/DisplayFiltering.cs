﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Appearition;
using Appearition.ArTargetImageAndMedia;
using Appearition.ChannelManagement;
using UnityEngine;
using UnityEngine.UI;

public class DisplayFiltering : MonoBehaviour
{
    //UI references
    public FilterStateToggle filterTogglePrefab;
    public RectTransform filterRowPrefab;

    public RectTransform classFiltersHolder;
    public RectTransform tagFiltersHolder;
    public RectTransform stateFiltersHolder;

    public FilterStateToggle classButtonFilter;
    public FilterStateToggle tagButtonFilter;
    public FilterStateToggle stateButtonFilter;

    List<FilterStateToggle> _classFilters = new List<FilterStateToggle>();
    List<FilterStateToggle> _tagFilters = new List<FilterStateToggle>();
    List<FilterStateToggle> _stateFilters = new List<FilterStateToggle>();

    //Ongoing Data
    List<Channel> _currentChannels = new List<Channel>();
    Dictionary<string, List<string>> _filtersPerChannel = new Dictionary<string, List<string>>();
    Coroutine _currentRefreshProcess;
    bool _requiresReset = true;

    //Settings
    [Header("My Profile type of behavior")]
    public bool onlyShowLocalData;

    #region Event Handling

    void Awake()
    {
        Logout.OnLogoutOccured += Logout_OnLogoutOccured;
        Login.OnLoginOccured += Logout_OnLogoutOccured;
    }

    private void Logout_OnLogoutOccured()
    {
        _requiresReset = true;
    }

    void OnDestroy()
    {
        Logout.OnLogoutOccured -= Logout_OnLogoutOccured;
        Login.OnLoginOccured -= Logout_OnLogoutOccured;
    }

    #endregion

    #region Get Rules 

    /// <summary>
    /// Gathers all the restrictions that should be used in the query API.
    /// </summary>
    /// <param name="channelsAndFilters"></param>
    /// <returns></returns>
    public IEnumerator GetChannelAndFiltersForQueryProcess(Action<List<Channel>, List<string>> channelsAndFilters)
    {
        List<Channel> selectedChannels = new List<Channel>();
        List<string> selectedFilters = new List<string>();

        //First, get the settings
        if (!FetchExperiences.Instance.filterTenantSettings.isSetup)
            yield return FetchExperiences.Instance.RefreshFilterTenantSettings();

        //Then, make sure everything was actually set up.
        if (_requiresReset)
            yield return ResetFiltersProcess(true);

        var activeClasses = GetActiveClassFilters();
        List<string> tagFilters = GetActiveTagFilters();

        //If any of the tenant filters are selected, handle things quite differently.
        List<string> activeTenantFilters = new List<string>();
        for (int i = 0; i < activeClasses.Count; i++)
        {
            if (FetchExperiences.Instance.filterTenantSettings.tenantLevelFilters.Contains(activeClasses[i]))
            {
                activeTenantFilters.Add(activeClasses[i]);
                break;
            }
        }

        if (!onlyShowLocalData && activeTenantFilters.Count > 0)
        {
            //If a tenant-level filter is on, that's all that will get passed through.
            selectedFilters.Add(activeTenantFilters.First());
        }
        else
        {
            //Finally, create the output. Class first!
            for (int i = 0; i < _currentChannels.Count; i++)
            {
                if (activeClasses.Contains(_currentChannels[i].name))
                {
                    selectedChannels.Add(_currentChannels[i]);
                    //break;
                }
            }

            //Then, add the active tags! Unless it's a tenant-level tag, of course..
            for (int i = 0; i < tagFilters.Count; i++)
            {
                if (!FetchExperiences.Instance.filterTenantSettings.tenantLevelFilters.Contains(tagFilters[i]) &&
                    (FetchExperiences.Instance.filterTenantSettings.allowExperienceTypeFilter || !FetchExperiences.Instance.filterTenantSettings.typeTags.Contains(tagFilters[i])))
                    selectedFilters.Add(tagFilters[i]);
            }

            //Finally, the states.. if needed.
            if (stateFiltersHolder != null)
            {
                List<string> stateFilters = GetActiveStateFilters();

                for (int i = 0; i < stateFilters.Count; i++)
                {
                    if (!selectedFilters.Contains(stateFilters[i]))
                        selectedFilters.Add(stateFilters[i]);
                }
            }
        }

        //Finally, callback!
        channelsAndFilters?.Invoke(selectedChannels, selectedFilters);
    }

    #endregion

    #region Reset Filters 

    public IEnumerator ResetFiltersProcess(bool refreshEmsData)
    {
        //Setting check first~
        if (!FetchExperiences.Instance.filterTenantSettings.isSetup)
            yield return FetchExperiences.Instance.RefreshFilterTenantSettings();

        //Firstly, delete all the content. 
        for (int i = 0; i < classFiltersHolder.childCount; i++)
            Destroy(classFiltersHolder.GetChild(i).gameObject);
        _classFilters.Clear();

        for (int i = 0; i < tagFiltersHolder.childCount; i++)
            Destroy(tagFiltersHolder.GetChild(i).gameObject);
        _tagFilters.Clear();

        if (stateFiltersHolder != null)
        {
            for (int i = 0; i < stateFiltersHolder.childCount; i++)
                Destroy(stateFiltersHolder.GetChild(i).gameObject);
            _stateFilters.Clear();
        }

        //Get the EMS data
        if (refreshEmsData)
            yield return ResetChannelAndFilterData();

        List<string> content = new List<string>();
        List<bool> states = new List<bool>();

        //Ignore tenant filters? Then all channels start as activated? eg MyProfile
        if (onlyShowLocalData)
        {
            for (int i = 0; i < _currentChannels.Count; i++)
            {
                content.Add(_currentChannels[i].name);
                states.Add(true);
            }

            //Setup the class filters.
            SetupFilters(content, states, _classFilters, classFiltersHolder, FilterStateToggle.FilterType.Class);
            content.Clear();
            states.Clear();

            //Then, setup the filters
            foreach (var filterCollections in _filtersPerChannel.Values)
            {
                for (int i = 0; i < filterCollections.Count; i++)
                {
                    if (!content.Contains(filterCollections[i]))
                    {
                        content.Add(filterCollections[i]);
                        states.Add(false);
                    }
                }
            }

            //Setup the tag filters
            SetupFilters(content, states, _tagFilters, tagFiltersHolder, FilterStateToggle.FilterType.Tag);
            content.Clear();
            states.Clear();

            //Add type to the filters while at it, if needed.
            if (FetchExperiences.Instance.filterTenantSettings.allowExperienceTypeFilter)
            {
                for (int i = 0; i < FetchExperiences.Instance.filterTenantSettings.typeTags.Count; i++)
                {
                    content.Add(FetchExperiences.Instance.filterTenantSettings.typeTags[i]);
                    states.Add(true);
                }
            }

            SetupFilters(content, states, _tagFilters, tagFiltersHolder, FilterStateToggle.FilterType.Tag);
            content.Clear();
            states.Clear();

            //Finally, set the state filters
            if (stateFiltersHolder != null)
            {
                content.AddRange(FetchExperiences.Instance.filterTenantSettings.stateTags);
                for (int i = 0; i < content.Count; i++)
                    states.Add(true);
                SetupFilters(content, states, _stateFilters, stateFiltersHolder, FilterStateToggle.FilterType.State);
            }
        }
        //If not dealing with local data, listen to the data. If none, don't select anything.
        else
        {
            List<string> tmpFilters = new List<string>();

            for (int i = 0; i < FetchExperiences.Instance.filterTenantSettings.tenantLevelFilters.Count; i++)
            {
                string tmpClassFilter = FetchExperiences.Instance.filterTenantSettings.tenantLevelFilters[i];
                if (!content.Contains(tmpClassFilter))
                {
                    content.Add(tmpClassFilter);
                    states.Add(FetchExperiences.Instance.filterTenantSettings.tenantLevelFirstSelectedTags.Contains(tmpClassFilter));

                    if (refreshEmsData)
                    {
                        yield return ArTargetHandler.GetAllRelatedTagsProcess(0, tmpClassFilter, success =>
                        {
                            for (int k = 0; k < success.Count; k++)
                            {
                                //Debug.LogError(success[k]);

                                if (!tmpFilters.Contains(success[k]) && !FetchExperiences.Instance.filterTenantSettings.tenantLevelFilters.Contains(success[k]))
                                    tmpFilters.Add(success[k]);
                            }

                            if (!_filtersPerChannel.ContainsKey(tmpClassFilter))
                                _filtersPerChannel.Add(tmpClassFilter, success);
                        });
                    }

                    else if (_filtersPerChannel.ContainsKey(tmpClassFilter))
                        tmpFilters.AddRange(_filtersPerChannel[tmpClassFilter]);
                }
            }

            //Also add all non-first channels, but as not selected. 
            for (int i = 0; i < _currentChannels.Count; i++)
            {
                if (!content.Contains(_currentChannels[i].name))
                {
                    content.Add(_currentChannels[i].name);
                    states.Add(FetchExperiences.Instance.filterTenantSettings.tenantLevelFirstSelectedTags.Contains(_currentChannels[i].name));
                }
            }

            //Setup channels
            SetupFilters(content, states, _classFilters, classFiltersHolder, FilterStateToggle.FilterType.Class);
            content.Clear();
            states.Clear();

            //Setup filters previously stored
            for (int i = 0; i < tmpFilters.Count; i++)
            {
                content.Add(tmpFilters[i]);
                states.Add(true);
            }

            SetupFilters(content, states, _tagFilters, tagFiltersHolder, FilterStateToggle.FilterType.Tag);
            content.Clear();
            states.Clear();
        }

        //Backup in case the window was open.
        BackupCurrentData();
        RefreshFilterButtons();
        _requiresReset = false;
    }

    IEnumerator ResetChannelAndFilterData()
    {
        //For the classes, get all the classes. That data will be used soon anyway!
        _currentChannels.Clear();
        _filtersPerChannel.Clear();

        if(AppearitionGate.Instance.CurrentUser.IsUserLoggedIn)
            yield return ChannelHandler.GetAllChannelsProcess(success => _currentChannels.AddRange(success));

        for (int i = 0; i < _currentChannels.Count; i++)
        {
            if (!_filtersPerChannel.ContainsKey(_currentChannels[i].name))
            {
                //Add a new entry
                List<string> channelFilters = new List<string>();

                //Fetch all used tags too!
                yield return ArTargetHandler.GetAllRegisteredTagsInChannelProcess(_currentChannels[i].channelId,
                    success =>
                    {
                        for (int k = 0; k < success.Count; k++)
                        {
                            if (!channelFilters.Contains(success[k]) &&
                                !FetchExperiences.Instance.filterTenantSettings.typeTags.Contains(success[k]) &&
                                !FetchExperiences.Instance.filterTenantSettings.tenantLevelFilters.Contains(success[k]))
                                channelFilters.Add(success[k]);
                        }
                    });

                for (int k = 0; k < FetchExperiences.Instance.filterTenantSettings.filterLevelFirstSelectedTags.Count; k++)
                {
                    if (!channelFilters.Contains(FetchExperiences.Instance.filterTenantSettings.filterLevelFirstSelectedTags[k]))
                        channelFilters.Add(FetchExperiences.Instance.filterTenantSettings.filterLevelFirstSelectedTags[k]);
                }

                if(!_filtersPerChannel.ContainsKey(_currentChannels[i].name))
                    _filtersPerChannel.Add(_currentChannels[i].name, channelFilters);
            }
        }
    }

    #endregion

    #region Refresh Filters 

    public void OnFilterPressed(string toggleName, bool state, FilterStateToggle.FilterType filterType)
    {
        if (_currentRefreshProcess != null)
            StopCoroutine(_currentRefreshProcess);
        _currentRefreshProcess = StartCoroutine(RefreshFiltersProcess(toggleName, state, filterType));
    }

    /// <summary>
    /// Refresh the tag UI.
    /// </summary>
    /// <returns></returns>
    IEnumerator RefreshFiltersProcess(string newSelection, bool newTagState, FilterStateToggle.FilterType filterType)
    {
        //No selection? Must be a mistake. Reset just in case.
        if (newSelection == null)
            yield return ResetFiltersProcess(false);
        else
        {
            var activeClasses = GetActiveClassFilters();

            //If a class-type filter was changed, remake the filters. Only one class-type can be selected at a time.
            if (filterType == FilterStateToggle.FilterType.Class)
            {
                bool isNewSelectionTenantLevelFilter = FetchExperiences.Instance.filterTenantSettings.tenantLevelFilters.Contains(newSelection);

                //If the use tried to deselect the only tag, tell them no.
                if (!newTagState && activeClasses.Count <= 0)
                {
                    _classFilters.FirstOrDefault(o => o.GetName.Equals(newSelection))?.ChangeValueWithoutEvent(true);
                    _currentRefreshProcess = null;
                    yield break;
                }


                for (int i = 0; i < tagFiltersHolder.childCount; i++)
                    Destroy(tagFiltersHolder.GetChild(i).gameObject);
                _tagFilters.Clear();

                //Find the other active class and turn it off.
                for (int i = 0; i < _classFilters.Count; i++)
                {
                    if (!string.Equals(newSelection, _classFilters[i].GetName, StringComparison.InvariantCultureIgnoreCase))
                        _classFilters[i].ChangeValueWithoutEvent(false);
                }

                //If a first tag is required, only set that one up.
                List<string> classTags = _filtersPerChannel[newSelection];
                List<string> content = new List<string>();
                List<bool> states = new List<bool>();

                //Select all associated filters.
                if (isNewSelectionTenantLevelFilter)
                {
                    content.AddRange(classTags);
                    for (int i = 0; i < content.Count; i++)
                        states.Add(true);
                }
                //If it's a class, select the first ones if any. Otherwise, select all.
                else
                {
                    if (!onlyShowLocalData)
                    {
                        if (FetchExperiences.Instance.filterTenantSettings.filterLevelFirstSelectedTags.Count > 0)
                        {
                            for (int i = 0; i < FetchExperiences.Instance.filterTenantSettings.filterLevelFirstSelectedTags.Count; i++)
                            {
                                if (!content.Contains(FetchExperiences.Instance.filterTenantSettings.filterLevelFirstSelectedTags[i]))
                                {
                                    content.Add(FetchExperiences.Instance.filterTenantSettings.filterLevelFirstSelectedTags[i]);
                                    states.Add(true);
                                }
                            }
                        }
                    }

                    //Add the rest, mindful of the preview block
                    for (int i = 0; i < classTags.Count; i++)
                    {
                        if (!content.Contains(classTags[i]))
                        {
                            content.Add(classTags[i]);
                            states.Add(false);
                        }
                    }
                }

                //Update the filters
                SetupFilters(content, states, _tagFilters, tagFiltersHolder, FilterStateToggle.FilterType.Tag);

                //If the experiences state were changed, reset them.
                if (stateFiltersHolder != null)
                {
                    for (int i = 0; i < _stateFilters.Count; i++)
                        _stateFilters[i].ChangeValueWithoutEvent(true);
                }
            }
            //Handle tags and states. Don't let no selection happen.
            else if (!newTagState)
            {
                bool isState = FetchExperiences.Instance.filterTenantSettings.stateTags.Contains(newSelection);

                if (isState)
                {
                    var states = GetActiveStateFilters();

                    if (states.Count == 0)
                        _stateFilters.FirstOrDefault(o => o.GetName.Equals(newSelection))?.ChangeValueWithoutEvent(true);
                }
                else if (!onlyShowLocalData)
                {
                    //If the filters were changed, make sure there is at least one selected.
                    List<string> filters = GetActiveTagFilters();

                    if (filters.Count == 0)
                        _tagFilters.FirstOrDefault(o => o.GetName.Equals(newSelection))?.ChangeValueWithoutEvent(true);
                }
            }
        }

        RefreshFilterButtons();
        //Finally, end it.
        _currentRefreshProcess = null;
    }

    #endregion

    void SetupFilters(List<string> filterContent, List<bool> filterStates, List<FilterStateToggle> container, RectTransform holder, FilterStateToggle.FilterType filterType)
    {
        HorizontalLayoutGroup rowHolder = Instantiate(filterRowPrefab, holder).GetComponent<HorizontalLayoutGroup>();
        float currentWidth = rowHolder.padding.left;

        for (int i = 0; i < filterContent.Count; i++)
        {
            //Create a new filter!
            var tmpFilter = Instantiate(filterTogglePrefab, rowHolder.transform);
            tmpFilter.Setup(filterContent[i], filterStates[i], filterType, OnFilterPressed);
            currentWidth += tmpFilter.GetTextWidth;

            //If the filter goes too far because of its overly stretched name, move it to the next row.
            if (currentWidth + rowHolder.padding.right > holder.rect.width)
            {
                rowHolder = Instantiate(filterRowPrefab, holder).GetComponent<HorizontalLayoutGroup>();
                tmpFilter.transform.SetParent(rowHolder.transform);
                currentWidth = rowHolder.padding.left + tmpFilter.GetTextWidth;
            }

            if (i + 1 < filterContent.Count)
                currentWidth += rowHolder.spacing;

            //Finally, store it
            container.Add(tmpFilter);
        }
    }

    IEnumerator RefreshOngoingEmsData()
    {
        if (onlyShowLocalData)
        {
            //Refresh tags?
            var displayScript = FindObjectOfType<DisplayUserExperiences>();
            if (displayScript != null)
                yield return displayScript.DispCoroutine();
        }
        else
        {
            //Refresh content
            var displayScript = FindObjectOfType<DisplayPublicExperiences>();
            if (displayScript != null)
                yield return displayScript.GetData();
        }

        _currentRefreshProcess = null;
    }

    void RefreshFilterButtons()
    {
        if (classButtonFilter != null)
            classButtonFilter.ChangeValueWithoutEvent(GetActiveClassFilters().Count > 0);
        if (tagButtonFilter != null)
            tagButtonFilter.ChangeValueWithoutEvent(GetActiveTagFilters().Count > 0);
        if (stateButtonFilter != null)
            stateButtonFilter.ChangeValueWithoutEvent(GetActiveStateFilters().Count > 0);
    }

    #region Backup & Restore

    List<string> _backupClassContent = new List<string>();
    List<string> _backupTagContent = new List<string>();
    List<string> _backupStateContent = new List<string>();

    /// <summary>
    /// Called when the menu opens.
    /// </summary>
    public void BackupCurrentData()
    {
        _backupClassContent = GetActiveClassFilters();
        _backupTagContent = GetActiveTagFilters();
        _backupStateContent = GetActiveStateFilters();
    }

    /// <summary>
    /// Called when the menu is closed without saving changes.
    /// </summary>
    public void RestoreCurrentData()
    {
        for (int i = 0; i < _classFilters.Count; i++)
            _classFilters[i].ChangeValueWithoutEvent(_backupClassContent.Contains(_classFilters[i].GetName));

        for (int i = 0; i < _tagFilters.Count; i++)
            _tagFilters[i].ChangeValueWithoutEvent(_backupTagContent.Contains(_tagFilters[i].GetName));

        for (int i = 0; i < _stateFilters.Count; i++)
            _stateFilters[i].ChangeValueWithoutEvent(_backupStateContent.Contains(_stateFilters[i].GetName));

        RefreshFilterButtons();
    }

    #endregion

    #region Clear 

    public void ClearAllFilters()
    {
        //If there is no data, then don't proceed. There must be an ongoing process creating content.
        _requiresReset = true;
    }

    public void ClearClassFilters()
    {
        //If there is no data, then don't proceed. There must be an ongoing process creating content.
        if (_classFilters.Count == 0)
            return;

        if (_currentRefreshProcess == null)
            _currentRefreshProcess = StartCoroutine(ResetFiltersProcess(false));
    }

    //IEnumerator ClearFiltersProcess()
    //{
    //    yield return ResetFiltersProcess(false);

    //    yield return RefreshOngoingEmsData();
    //    Debug.LogError("OwO");
    //    _currentRefreshProcess = null;
    //}

    public void ClearTagFilters()
    {
        var activeClasses = GetActiveClassFilters();

        //If tenant-level tags, don't do anything.
        if (activeClasses.Any(o => FetchExperiences.Instance.filterTenantSettings.tenantLevelFilters.Contains(o)))
            return;

        //Set the first selected instead, if any
        if (!onlyShowLocalData && FetchExperiences.Instance.filterTenantSettings.filterLevelFirstSelectedTags.Count > 0)
        {
            List<string> onTags = new List<string>();
            for (int i = 0; i < activeClasses.Count; i++)
            {
                if (!_filtersPerChannel.ContainsKey(activeClasses[i]))
                    continue;

                var classTags = _filtersPerChannel[activeClasses[i]];

                for (int k = 0; k < classTags.Count; k++)
                {
                    if (FetchExperiences.Instance.filterTenantSettings.filterLevelFirstSelectedTags.Contains(classTags[k]) && !onTags.Contains(classTags[k]))
                        onTags.Add(classTags[k]);
                }
            }

            for (int i = 0; i < _tagFilters.Count; i++)
                _tagFilters[i].ChangeValueWithoutEvent(onTags.Contains(_tagFilters[i].GetName));
        }
        else
        {
            for (int i = 0; i < _tagFilters.Count; i++)
                _tagFilters[i].ChangeValueWithoutEvent(false);
        }
    }

    public void ClearStateFilters()
    {
        if (stateFiltersHolder != null)
        {
            for (int i = 0; i < _stateFilters.Count; i++)
                _stateFilters[i].ChangeValueWithoutEvent(true);
        }
    }

    #endregion


    #region Get Active Filters 

    public List<string> GetActiveClassFilters()
    {
        List<string> outcome = new List<string>();

        for (int i = 0; i < _classFilters.Count; i++)
        {
            if (_classFilters[i].FilterState && !string.IsNullOrEmpty(_classFilters[i].GetName))
                outcome.Add(_classFilters[i].GetName);
        }

        return outcome;
    }

    public List<string> GetActiveTagFilters()
    {
        List<string> outcome = new List<string>();

        for (int i = 0; i < _tagFilters.Count; i++)
        {
            if (_tagFilters[i].FilterState && !string.IsNullOrEmpty(_tagFilters[i].GetName))
                outcome.Add(_tagFilters[i].GetName);
        }

        return outcome;
    }

    public List<string> GetActiveStateFilters()
    {
        List<string> outcome = new List<string>();

        for (int i = 0; i < _stateFilters.Count; i++)
        {
            if (_stateFilters[i].FilterState && !string.IsNullOrEmpty(_stateFilters[i].GetName))
                outcome.Add(_stateFilters[i].GetName);
        }

        return outcome;
    }

    #endregion

    void OnDisable()
    {
        _currentRefreshProcess = null;
    }
}