﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using Hologo2;

using UnityEngine;

namespace Appearition.Example
{
    public abstract class AssetBundleArMedia : MonoBehaviour, IArMedia
    {
        //ApiData
        /// <summary>
        /// Reference to the experience which this AssetBundle is part of.
        /// </summary>
        public AppearitionExperience AssociatedExperience { get; protected set; }

        /// <summary>
        /// Contains the ApiData of the current ArMedia.
        /// </summary>
        public MediaFile Data { get; protected set; }

        private AssetBundle _currentBundle;
        private AssetBundle _currentLocalBundle;

        /// <summary>
        /// Reference and storage for the bundle associated to that mediafile.
        /// </summary>
        public AssetBundle CurrentBundle
        {
            get { return _currentBundle; }
            private set
            {
                if (value == null && _currentBundle != null)
                    _currentBundle.Unload(true);

                _currentBundle = value;
            }
        }


        /// <summary>
        /// Reference and storage for the bundle associated to that mediafile.
        /// </summary>
        public AssetBundle CurrentLocalBundle
        {
            get { return _currentLocalBundle; }
            private set
            {
                if (value == null && _currentLocalBundle != null)
                    _currentLocalBundle.Unload(true);

                _currentLocalBundle = value;
            }
        }

        private bool? _hasLoadedAssetbundle;

        private GameObject _content;

        /// <summary>
        /// Whether or not the current build platform is suitable for this assetbundle
        /// </summary>
        public abstract bool IsPlatformAllowedForAssetbundle { get; }

        public bool IsMediaReadyAndDownloaded => _hasLoadedAssetbundle.GetValueOrDefault() || !IsPlatformAllowedForAssetbundle;

        public virtual void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            //Store media
            AssociatedExperience = associatedExperience;
            Data = media;

            if (!_hasLoadedAssetbundle.HasValue)
            {
                _hasLoadedAssetbundle = false;
                //Fetch it from the files if available.
                ArTargetHandler.LoadMediaFileContent(associatedExperience.Data, media, OnAssetbundleLoaded);
            }
        }

        public virtual void ChangeDisplayState(bool state)
        {
            gameObject.SetActive(state);
        }

        /// <summary>
        /// Whether or not the current Media is visible.
        /// </summary>
        public bool IsVisible
        {
            get { return gameObject.activeInHierarchy; }
        }

        public virtual void OnExperienceTrackingStateChanged(bool isTracking)
        {
            Debug.LogError("OwO");

            //Check if the assetbundle has been fetched already.
            if (isTracking && !_hasLoadedAssetbundle.HasValue && IsPlatformAllowedForAssetbundle)
            {
                _hasLoadedAssetbundle = false;
                //Fetch it from the files if available.
                ArTargetHandler.LoadMediaFileContent(AssociatedExperience.Data, Data, OnAssetbundleLoaded);
            }

            //Finally enable/disable object
            ChangeDisplayState(isTracking);
        }

        /// <summary>
        /// Once downloaded, finds the content inside the assetbundle to spawn, if any.
        /// </summary>
        /// <param name="obj"></param>
        private void OnAssetbundleLoaded(Dictionary<string, byte[]> obj)
        {
            Object[] bundleContent = null;
            //Object[] bundleLocalContent = null;
            GameObject bundleContentToSpawn = null;
            CurrentBundle = AssetBundle.LoadFromMemory(obj.First().Value);



            //Bundle not found? Most likely in the editor due to duplicate loading.
            if (CurrentBundle == null)
            {
                AppearitionLogger.LogError(string.Format(
                    "An error occured when trying to load the assetbundle from the media of id {0}. If your experience has several experiences for different platforms and you currently are running in editor, disregard this message.",
                    Data.arMediaId));
                return;
            }
            //HACK : ONLY FOR HOLOGO TESTING
            if (Data.contentItemProviderName == "HologoContentLibrary")
            {
                _content = Instantiate(Resources.Load("Hologo_Asset_Holder"), transform) as GameObject;
                _content.transform.localPosition = Data.GetPosition;
                _content.transform.localRotation = Data.GetRotation;
                _content.transform.localScale = Data.GetScale * AppearitionArDemoConstants.ASSETBUNDLE_SCALE_MULTIPLIER;
                CurrentLocalBundle = AssetBundle.LoadFromMemory(obj.ElementAt(1).Value);
                experienceViewController evc = _content.GetComponentInChildren<experienceViewController>(true);
                evc.myLoadedAssetBundle = CurrentLocalBundle;
                evc.myLoadedLocalAssetBundle = CurrentBundle;
                evc.Init(AssociatedExperience.Data.IsMarkerless? 350f : 1f);
                _hasLoadedAssetbundle = true;
                CurrentBundle.Unload(false);
                CurrentLocalBundle.Unload(false);
                return;
            }
            bundleContent = CurrentBundle.LoadAllAssets();

            if (bundleContent.Length > 0)
            {
                //Spawn the first item found
                for (int i = 0; i < bundleContent.Length; i++)
                {
                    if (bundleContent[i] == null)
                        continue;

                    try
                    {
                        bundleContentToSpawn = (GameObject) bundleContent[i];

                        if (bundleContentToSpawn != null)
                            break;
                    } catch
                    {
                        //Keep looping
                    }
                }
            }

            if (bundleContentToSpawn == null)
            {
                AppearitionLogger.LogWarning(string.Format("Unable to find content to instantiate in the assetbundle of id {0}, and name + {1}", Data.arMediaId, Data.fileName));
            }
            //If the experience is null, then the object was about to be created after the user decided to move on.. 
            else if (AssociatedExperience != null)
            {
                _content = Instantiate(bundleContentToSpawn, transform);
                _content.transform.localPosition = Data.GetPosition;
                _content.transform.localRotation = Data.GetRotation;
                _content.transform.localScale = Data.GetScale * AppearitionArDemoConstants.ASSETBUNDLE_SCALE_MULTIPLIER;
                //tmp.transform.localRotation = Quaternion.Euler(Vector3.right * -90 + Vector3.forward * 180);
                _hasLoadedAssetbundle = true;

                //Apply cameras
                Canvas[] allCanvases = GetComponentsInChildren<Canvas>();

                for (int i = 0; i < allCanvases.Length; i++)
                {
                    if (allCanvases[i].renderMode == RenderMode.WorldSpace)
                        allCanvases[i].worldCamera = AppearitionArHandler.Instance.ProviderHandler.ProviderCamera;
                }


                Debug.Log(string.Format("Asset downloaded ! Name: {0}, Mediatype: {1}", Data.fileName, Data.mediaType));
            }
        }

        #region utilities 

        public static bool WouldMediaWorkForThisPlatform<T>() where T : AssetBundleArMedia
        {
            bool output = false;
            if (typeof(T).IsSubclassOf(typeof(MonoBehaviour)))
            {
                T tmp = new GameObject().AddComponent<T>();
                output = tmp.IsPlatformAllowedForAssetbundle;
                Destroy(tmp.gameObject);
            }
            else
            {
                T tmp = System.Activator.CreateInstance<T>();
                output = tmp.IsPlatformAllowedForAssetbundle;
            }

            return output;
        }

        #endregion

        void OnDestroy()
        {
            AssetBundle.UnloadAllAssetBundles(true);
        }
    }
}