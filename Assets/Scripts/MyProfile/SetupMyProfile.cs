﻿using Appearition;
using Appearition.ArTargetImageAndMedia;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Appearition.ChannelManagement;
using UnityEngine;
using UnityEngine.UI;

public class SetupMyProfile : MonoBehaviour
{
    public DisplayFiltering dFilter;

    public MyProfileData data;

    public Text userName;

    public Text experienceCount;

    public Text tenantName;

    private void OnEnable()
    {
        string usernameTextCombined = AppearitionGate.Instance.CurrentUser.firstName + " " + AppearitionGate.Instance.CurrentUser.lastName;
        if (userName != null)
        {
            userName.text = $"{AppearitionGate.Instance.CurrentUser.firstName} {AppearitionGate.Instance.CurrentUser.lastName}";
        }
        RefreshUserData();
    }

    public void RefreshUserData()
    {
        tenantName.text = AppearitionGate.Instance.CurrentUser.allTenantsAvailable.First(tenant => tenant.tenantKey == AppearitionGate.Instance.CurrentUser.selectedTenant).tenantName;
        StartCoroutine(GetContent());
    }

    IEnumerator GetContent()
    {
        GetComponent<DisplayUserExperiences>().fetchingMessage.SetActive(true);
        List<Channel> allowedChannels = new List<Channel>();
        List<string> allowedTags = new List<string>();
        yield return dFilter.GetChannelAndFiltersForQueryProcess((c, t) =>
        {
            allowedChannels.AddRange(c);
            allowedTags.AddRange(t);
        });
        CoroutineWithData cd = new CoroutineWithData(this, FetchExperiences.Instance.GetUserContent(allowedChannels, allowedTags));
        yield return cd.coroutine;
        List<ArTarget> userAssets = (List<ArTarget>)cd.result;
        data.userTargets = userAssets;
        experienceCount.text = userAssets.Count + " experiences";
        GetComponent<DisplayUserExperiences>().Disp();
        LoadingPanel.Instance.HideMessage();
    }

    public void AddNextSet()
    {
        GetComponent<DisplayUserExperiences>().AddNextSet();
    }

}
