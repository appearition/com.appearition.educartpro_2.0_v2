﻿using System.Collections;
using System.Collections.Generic;
using Appearition.ArTargetImageAndMedia;
using Appearition.Example;
using UnityEngine;
using UnityEngine.UI;

public class HelpScreenUIHandler : MonoBehaviour
{
    public string keyName;
    public GameObject helpScreenHolder;

    private void OnEnable()
    {
        if (PlayerPrefs.HasKey(keyName))
        {
            helpScreenHolder.SetActive(false);
        }
        else
        {
            PlayerPrefs.SetInt(keyName, 1);
            helpScreenHolder.SetActive(true);
        }
    }

    public void onGotItButtonPressed()
    {
        PlayerPrefs.SetInt(keyName,1);
        helpScreenHolder.SetActive(false);
    }

}
