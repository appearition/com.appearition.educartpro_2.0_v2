﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition;

public class SelectTenant : MonoBehaviour
{
    public Button nextButton;
    public void SelectTenantTogglePressed(bool value)
    {
        if (value == true)
        {
            string tenantKey = GetComponent<TenantElementData>().elementTenant.tenantKey;
            Debug.Log(tenantKey);
            AppearitionGate.Instance.CurrentUser.selectedTenant = tenantKey;
            if (UserPrefs.Instance.RememberMe)
            {
                PlayerPrefs.SetString(AppearitionConstants.PROFILE_PREFERRED_TENANT, tenantKey);
            }
            nextButton.interactable = true;
        }
    }

}
