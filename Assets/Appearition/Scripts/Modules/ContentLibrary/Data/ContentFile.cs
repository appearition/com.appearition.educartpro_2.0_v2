﻿namespace Appearition.ContentLibrary
{
    [System.Serializable]
    public class ContentFile
    {
        public string Url;
        public string MetaData;
        public string MimeType;
        public string FileName;
        public string Checksum;
        public long FileSizeBytes;
        public long FileSizeKBytes;
        public long FileSizeMBytes;
    }
}