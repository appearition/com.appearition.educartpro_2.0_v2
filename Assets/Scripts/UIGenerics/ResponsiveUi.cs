﻿using UnityEngine;

public class ResponsiveUi : MonoBehaviour
{
    float ratio;
    RectTransform recTransform;
    //public RectTransform recTransform;

    [Header("For Aspect ratio below 0.5")]
    public Vector2 resizedWidthHeight;
    public Vector3 resizedPostion;

    [Header("For Aspect ratio above 0.5")]
    public Vector2 actualWidthHeight;
    public Vector3 actualPosition;

    public void OnEnable()
    {
        recTransform = this.gameObject.GetComponent<RectTransform>();
        ratio = Camera.main.aspect;
        if (ratio <= 0.50)
        {
            
            recTransform.sizeDelta = resizedWidthHeight;
            recTransform.localPosition = resizedPostion;
        }
        else
        {
            recTransform.sizeDelta = actualWidthHeight;
            recTransform.localPosition = actualPosition;
        }
    }

}
