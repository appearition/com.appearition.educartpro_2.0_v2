﻿#if UNITY_IOS
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;

public static class MyBuildPostprocess
{
    [PostProcessBuild(999)]
    public static void OnPostProcessBuild(BuildTarget buildTarget, string path)
    {
        if (buildTarget == BuildTarget.iOS)
        {
            string projectPath = path + "/Unity-iPhone.xcodeproj/project.pbxproj";

            PBXProject pbxProject = new PBXProject();
            pbxProject.ReadFromFile(projectPath);

            string target = pbxProject.ProjectGuid();
            Debug.LogFormat("RMIT OnPostprocessBuild: {0} {1}", target, buildTarget);
            pbxProject.SetBuildProperty(target, "ENABLE_BITCODE", "NO");

            target = pbxProject.GetUnityMainTargetGuid();
            pbxProject.AddFrameworkToProject(target, "SystemConfiguration.framework", true);
            pbxProject.AddFrameworkToProject(target, "UIKit.framework", true);
            pbxProject.AddFrameworkToProject(target, "CoreData.framework", true);
            pbxProject.AddFrameworkToProject(target, "AdSupport.framework", true);
            pbxProject.AddFrameworkToProject(target, "libsqlite3.tbd", true);
            pbxProject.AddFrameworkToProject(target, "libz.tbd", true);



            pbxProject.WriteToFile(projectPath);

            // Get plist
            string plistPath = path + "/Info.plist";
            PlistDocument plist = new PlistDocument();
            plist.ReadFromFile(plistPath);

            // Get root
            PlistElementDict rootDict = plist.root;

            // Change value of CFBundleVersion in Xcode plist
            var buildKey = "App Uses Non-Exempt Encryption";
            rootDict.SetBoolean(buildKey, false);

            // Write to file
            plist.WriteToFile(plistPath);


        }
    }
}
#endif