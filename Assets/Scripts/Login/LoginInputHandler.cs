﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

[RequireComponent(typeof(LoginData))]
public class LoginInputHandler : MonoBehaviour
{
    LoginData loginData;
    public Text emailFeedbackText;
    public Button loginButton;

    [Header("UI References For Refresh")]
    public InputField email;
    public InputField password;
    public Toggle rememberMe;

    void OnEnable()
    {
        loginData = GetComponent<LoginData>();
        loginData.email = "";
        loginData.password = "";
        loginData.rememberMe = true;
        loginData.isEmailValid = false;
        loginData.isPasswordValid = false;
        CheckLoginButtonInteractivity();
        email.text = "";
        password.text = "";
    }

    //Subbscribed to OnInputValueChanged Event in Editor
    public void SetAndValidateEmail(string input)
    {
        loginData.email = input;

        loginData.isEmailValid = true;

        CheckLoginButtonInteractivity();
    }

    //Subbscribed to OnInputValueChanged Event in Editor
    public void SetAndValidatePassword(string input)
    {
        loginData.password = input;
        if (loginData.password.Length > 0)
        {
            loginData.isPasswordValid = true;
        }
        CheckLoginButtonInteractivity();
    }

    //Subbscribed to OnInputValueChanged Event in Editor
    public void RememberMeToggle(bool toggle)
    {
        loginData.rememberMe = toggle;
    }

    public void CheckLoginButtonInteractivity()
    {
        loginButton.interactable = loginData.isEmailValid && loginData.isPasswordValid;
    }

    void ResetUI()
    {

    }

}
