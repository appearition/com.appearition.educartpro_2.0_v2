﻿using System.Collections;
using System.Collections.Generic;
using Appearition.Example;
using UnityEngine;
using UnityEngine.UI;

public class RefreshMarkerScanMode : MonoBehaviour
{
    public void OnRefreshButtonPress()
    {
        AppearitionArHandler.Instance.ClearCurrentTargetBeingTracked(true);
    }


}
