﻿using UnityEngine;
using System.Collections;


public class UserPrefs : Singleton<UserPrefs>
{
    private void Awake()
    {
        if (!PlayerPrefs.HasKey("SkipOnboarding"))
        {
            SkipOnboarding = false;
        }
        if (!PlayerPrefs.HasKey("RememberMe"))
        {
            RememberMe = true;
        }
    }


    public bool SkipOnboarding
    {
        get
        {

            if (PlayerPrefs.GetString("SkipOnboarding") == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            PlayerPrefs.SetString("SkipOnboarding", value.ToString());
        }
    }

    public bool RememberMe
    {
        get
        {
            if (PlayerPrefs.GetString("RememberMe") == "True")
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        set
        {
            Debug.Log(value);
            PlayerPrefs.SetString("RememberMe", value.ToString());
        }

    }
}
