// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: ArTarget_Get.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using Appearition.API;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/ArTarget/Get/0?arTargetId=1 , where 0 is Channel ID and 1 is Ar Target ID
    /// </summary>
    [System.Serializable]
    public class ArTarget_Get : BaseApiGet
    {
        //Request Params
        [System.Serializable]
        public class RequestContent : BaseRequestContent
        {
            public int arTargetId;
        }

        //Variables
        public ArTarget Data;
    }
}