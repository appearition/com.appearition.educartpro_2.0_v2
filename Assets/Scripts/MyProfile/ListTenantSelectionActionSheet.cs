﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition;
using Appearition.AccountAndAuthentication;
using UnityEngine.UI;

public class ListTenantSelectionActionSheet : MonoBehaviour
{
    public GameObject tenantListElement;
    public GameObject UIParent;

    List<GameObject> tenantElementsCache = new List<GameObject>();

    public void OnEnable()
    {

        for (int i = tenantElementsCache.Count - 1; i >= 0; i--)
        {
            Destroy(tenantElementsCache[i]);
        }
        tenantElementsCache.Clear();
        foreach (TenantData tenantData in AppearitionGate.Instance.CurrentUser.allTenantsAvailable)
        {
            
            GameObject go = Instantiate(tenantListElement, UIParent.transform);
            go.SetActive(true);
            go.GetComponent<ProfileSelectTenant>().Setup(tenantData);
            tenantElementsCache.Add(go);
            if (tenantData.tenantKey == AppearitionGate.Instance.CurrentUser.selectedTenant)
            {
                go.GetComponent<Toggle>().SetIsOnWithoutNotify(true);
            }
        }
    }
}

