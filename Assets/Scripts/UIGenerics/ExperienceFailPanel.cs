﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExperienceFailPanel : Singleton<ExperienceFailPanel>
{
    public GameObject myProfile;
    public void Activate(float delay)
    {
        GetComponent<Canvas>().enabled = true;
        Invoke("Deactivate", delay);
    }

    void Deactivate()
    {
        GetComponent<Canvas>().enabled = false;
        myProfile.SetActive(true);
    }
}
