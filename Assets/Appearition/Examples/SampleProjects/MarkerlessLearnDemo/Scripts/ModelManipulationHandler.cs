﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: "ModelManipulationHandler.cs" 
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;


namespace Appearition.Example.MarkerlessLearnDemo
{
    /// <summary>
    /// Class in charge of manipulating the 3D model that's active.
    /// </summary>
    public class ModelManipulationHandler : MonoBehaviour
    {
        #region Internal Singleton

        static ModelManipulationHandler _instance;

        static ModelManipulationHandler Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<ModelManipulationHandler>();
                return _instance;
            }
        }

        #endregion

        #region Events

        public delegate void ManipulationStateChanged(bool isCurrentlyManipulating);

        /// <summary>
        /// Occurs whenever the state of manipulation of the model has been changed. If true, the model is currently being translated, rotated or scaled.
        /// </summary>
        public static event ManipulationStateChanged OnManipulationStateChanged;


        public delegate void ModelTransformReset();

        /// <summary>
        /// Occurs whenever the model transform values are reset. Most likely called by the user via UI.
        /// </summary>
        public static event ModelTransformReset OnModelTransformReset;

        public delegate void ManipulationActiveStateChanged(bool isActive);

        /// <summary>
        /// Occurs whenever IsModelManipulationActive value changes. Determines whether or not the model can currently be manipulated or not.
        /// </summary>
        public static event ManipulationActiveStateChanged OnManipulationActiveStateChanged;

        #endregion

        //References
        /// <summary>
        /// Reference to the current camera used for manipulation.
        /// </summary>
        public Camera CurrentCamera => ArCameraContainer.CurrentCamera;

        /// <summary>
        /// Shortcut to the model currently being manipulated. Should only be accessed if IsModelManipulationActive is true.
        /// </summary>
        Transform CurrentModelTransform => currentModelOverride;

        /// <summary>
        /// Shortcut to the current node of the model being the center of focus. Transformations of the entire model should be made based on this focus point.
        /// </summary>
        Transform CurrentNodeTransform => CurrentModelTransform;

        //Internal Variables
        Vector3 _backupModelPos;
        Quaternion _backupModelRot;
        Vector3 _backupModelScale;
        [Header("Manipulation Settings")] public bool isManipulationActiveOnStart = false;
        public float translationSpeed = 0.3f;
        public float rotationSpeed = 5f;
        public float scalingSpeed = 0.5f;
        public Vector2 scaleMinMax = new Vector2(0.1f, 10f);
        bool _wasManipulatedThisFrame = false;
        bool _wasManipulatedLastFrame = false;
        [Header("Debug Settings")] [Tooltip("A way to force in a model to manipulate.")]
        public Transform currentModelOverride = null;

        //HACK
        bool pumpTranslationNumbersUp = false;
        public float translationMultiplierIfPumpedUp = 50;

        //Properties 
        static bool _isModelManipulationActive;

        /// <summary>
        /// Whether or not the model can be manipulated. Toggles when a new model becomes selected.
        /// </summary>
        public static bool IsModelManipulationActive
        {
            get => _isModelManipulationActive;

            protected set
            {
                _isModelManipulationActive = value;
                OnManipulationActiveStateChanged?.Invoke(_isModelManipulationActive);
            }
        }


        void Awake()
        {
            //Subscribes to events
            //AppearitionLearnDemoHandler.OnCurrentLearnAssetBeingViewedChanged += AppearitionLearnDemoHandler_OnCurrentLearnAssetBeingViewedChanged;
            InputHandler.OnDoubleFingerOngoingHold += InputHandler_OnDoubleFingerOngoingHold;
            InputHandler.OnSingleFingerOngoingHold += InputHandler_OnSingleFingerOngoingHold;
            //ArFoundationMarkerlessHandler.OnCurrentExperienceChanged += ArFoundationMarkerlessHandler_OnCurrentExperienceChanged;
            AppearitionArHandler.OnTargetStateChanged += AppearitionArHandler_OnTargetStateChanged;
            //MarkerlessModeHandler.OnModeStateChanged += MarkerlessModeHandler_OnModeStateChanged;
        }

        void OnDestroy()
        {
            InputHandler.OnDoubleFingerOngoingHold -= InputHandler_OnDoubleFingerOngoingHold;
            InputHandler.OnSingleFingerOngoingHold -= InputHandler_OnSingleFingerOngoingHold;
            //ArFoundationMarkerlessHandler.OnCurrentExperienceChanged -= ArFoundationMarkerlessHandler_OnCurrentExperienceChanged;
            AppearitionArHandler.OnTargetStateChanged -= AppearitionArHandler_OnTargetStateChanged;
        }

        #region Setup Model To Manipulate

        Coroutine findArMediaProcessStorage;

        //MARKER
        private void AppearitionArHandler_OnTargetStateChanged(AppearitionExperience experience, AppearitionArHandler.TargetState newstate)
        {
            //currentModelOverride = FindArMediaToManipulateFromExperience(experience);
            if (findArMediaProcessStorage != null)
                StopCoroutine(findArMediaProcessStorage);
            findArMediaProcessStorage = StartCoroutine(FindArMediaToManipulateFromExperience(experience));
            IsModelManipulationActive = currentModelOverride != null;
            pumpTranslationNumbersUp = true;
        }

        //MARKERLESS
        private void ArFoundationMarkerlessHandler_OnCurrentExperienceChanged(AppearitionExperience previousExperience, AppearitionExperience experience)
        {
            //Only enable manipulation if the experience is interactible.
            //currentModelOverride = FindArMediaToManipulateFromExperience(experience);
            if (findArMediaProcessStorage != null)
                StopCoroutine(findArMediaProcessStorage);
            findArMediaProcessStorage = StartCoroutine(FindArMediaToManipulateFromExperience(experience));
            IsModelManipulationActive = currentModelOverride != null;
            pumpTranslationNumbersUp = false;
        }

        /// <summary>
        /// From a given experience, tries to find a media which can be interacted with.
        /// </summary>
        /// <param name="experience"></param>
        /// <returns></returns>
        IEnumerator FindArMediaToManipulateFromExperience(AppearitionExperience experience)
        {
            currentModelOverride = null;
            if (experience == null)
            {
                findArMediaProcessStorage = null;
                yield break;
            }

            IArMedia interactibleMedia = null; // experience.Medias.FirstOrDefault(o => o.Data.isInteractive);
            while (experience.Medias == null)
                yield return null;
            for (int i = 0; i < experience.Medias.Count; i++)
            {
                if (experience.Medias[i].Data.isInteractive)
                {
                    AssetBundleArMedia assetBundle = experience.Medias[i] as AssetBundleArMedia;
                    if (assetBundle == null || assetBundle.IsPlatformAllowedForAssetbundle)
                    {
                        interactibleMedia = experience.Medias[i];
                        break;
                    }
                }
            }

            if (interactibleMedia != null)
            {
                //Find the first media that is interactible.
                for (int i = 0; i < experience.transform.childCount; i++)
                {
                    var tmpArMedia = experience.transform.GetChild(i).GetComponent<IArMedia>();
                    if (tmpArMedia != null && tmpArMedia.Data.arMediaId == interactibleMedia.Data.arMediaId)
                    {
                        currentModelOverride = experience.transform.GetChild(i);
                        break;
                    }
                }

                //yield return new WaitForSeconds(3);
                ////CHANGE, IF ONE CAN BE INTERACTIVE, ALL OF THEM CAN
                //currentModelOverride = new GameObject("Manipulation Center").transform;
                //currentModelOverride.SetParent(experience.transform);
                //currentModelOverride.localPosition = Vector3.zero;
                //currentModelOverride.rotation = Quaternion.identity;
                //currentModelOverride.localScale = Vector3.one;

                //for (int i = 0; i < experience.transform.childCount; i++)
                //{
                //    if (experience.transform.GetChild(i) == currentModelOverride.transform)
                //        continue;

                //    //var tmpArMedia = experience.transform.GetChild(i).GetComponent<IArMedia>();
                //    //if (tmpArMedia != null && tmpArMedia.Data.isInteractive)
                //    experience.transform.GetChild(i).SetParent(currentModelOverride.transform);
                //}
            }

            findArMediaProcessStorage = null;
        }

        #endregion

        void Start()
        {
            if (isManipulationActiveOnStart)
                IsModelManipulationActive = true;
        }

        //private void AppearitionLearnDemoHandler_OnCurrentLearnAssetBeingViewedChanged(AppearitionMixedAsset oldAsset, AppearitionMixedAsset newAsset)
        //{
        //    //Reset the old asset's transform if any
        //    if (oldAsset != null && oldAsset.ModelMedia != null)
        //    {
        //        ResetModelTransform(oldAsset.ModelMedia.transform);
        //    }

        //    //Store the new asset's transform if any
        //    if (newAsset != null && newAsset.ModelMedia != null)
        //    {
        //        _backupModelPos = newAsset.ModelMedia.transform.localPosition;
        //        _backupModelRot = newAsset.ModelMedia.transform.localRotation;
        //        _backupModelScale = newAsset.ModelMedia.transform.localScale;
        //    }

        //    //Refresh flags
        //    IsModelManipulationActive = newAsset != null && newAsset.ModelMedia != null;
        //}

        //private void MarkerlessModeHandler_OnModeStateChanged(MarkerlessModeHandler.ModeState newstate)
        //{
        //    ResetModelTransform();
        //}

        /// <summary>
        /// Rotate the current model parallel to the camera in a clockwise or counterclockwise rotation.
        /// </summary>
        /// <param name="rotateClockwise"></param>
        public void RotateOnThirdAxis(bool rotateClockwise)
        {
            if (IsModelManipulationActive && CurrentModelTransform != null && CurrentCamera != null)
            {
                CurrentModelTransform.RotateAround(CurrentNodeTransform.position, CurrentCamera.transform.forward, rotationSpeed * (rotateClockwise ? -1 : 1));
                OnManipulationStateChanged?.Invoke(false);
            }
        }

        /// <summary>
        /// On single finger hold, if there's any model, rotate the model.
        /// </summary>
        /// <param name="currentScreenPos"></param>
        /// <param name="scaledDeltaPos"></param>
        private void InputHandler_OnSingleFingerOngoingHold(Vector2 currentScreenPos, Vector2 scaledDeltaPos)
        {
            //No camera, no manipulation.
            if (CurrentCamera == null || CurrentNodeTransform == null || CurrentModelTransform == null)
                return;
            if (IsModelManipulationActive)
            {
                Vector3 rotationDelta = (CurrentCamera.transform.right * scaledDeltaPos.y + CurrentCamera.transform.up * -scaledDeltaPos.x) * rotationSpeed * Time.deltaTime;

                CurrentModelTransform.RotateAround(CurrentNodeTransform.position, rotationDelta.normalized, rotationDelta.magnitude);

                _wasManipulatedThisFrame = true;
            }
        }

        /// <summary>
        /// On double fingers hold, if any model, translate and scale the mode.
        /// </summary>
        /// <param name="screenCenterPos"></param>
        /// <param name="scaledCenterDeltaPos"></param>
        /// <param name="scaledDistanceDelta"></param>
        private void InputHandler_OnDoubleFingerOngoingHold(Vector2 screenCenterPos, Vector2 scaledCenterDeltaPos, float scaledDistanceDelta)
        {
            //No camera, no manipulation.
            if (CurrentCamera == null)
                return;
            if (IsModelManipulationActive)
            {
                float extraMulti = AppearitionArHandler.Instance?.ProviderHandler?.ScaleMultiplier ?? 1 ;
                CurrentModelTransform.position += (CurrentCamera.transform.right * scaledCenterDeltaPos.x + CurrentCamera.transform.up * scaledCenterDeltaPos.y) *
                                                  (pumpTranslationNumbersUp ? translationMultiplierIfPumpedUp : 1) * translationSpeed * extraMulti * Time.deltaTime;

                CurrentModelTransform.localScale = Mathf.Clamp(CurrentModelTransform.localScale.x + scaledDistanceDelta * scalingSpeed * extraMulti * Time.deltaTime, scaleMinMax.x, scaleMinMax.y) * Vector3.one;

                _wasManipulatedThisFrame = true;
            }
        }

        void LateUpdate()
        {
            //After update, check if the model was manipulated. If not, then tell the event about it!
            if (!_wasManipulatedThisFrame && _wasManipulatedLastFrame)
            {
                OnManipulationStateChanged?.Invoke(false);
            }
            else if (_wasManipulatedThisFrame)
            {
                OnManipulationStateChanged?.Invoke(true);
            }

            //Refresh the flag value
            _wasManipulatedLastFrame = _wasManipulatedThisFrame;
            _wasManipulatedThisFrame = false;
        }

        /// <summary>
        /// Resets the model being manipulated by its default values.
        /// Also triggers the event.
        /// </summary>
        public void ResetModelTransform()
        {
            if (IsModelManipulationActive)
            {
                CurrentModelTransform.localPosition = _backupModelPos;
                CurrentModelTransform.localRotation = _backupModelRot;
                CurrentModelTransform.localScale = _backupModelScale;

                OnModelTransformReset?.Invoke();
            }
        }

        /// <summary>
        /// Resets the model being manipulated by its default values.
        /// </summary>
        public void ResetModelTransform(Transform target)
        {
            if (target != null)
            {
                target.localPosition = _backupModelPos;
                target.localRotation = _backupModelRot;
                target.localScale = _backupModelScale;
            }
        }

        /// <summary>
        /// Float Remap
        /// </summary>
        /// <param name="value"></param>
        /// <param name="from1"></param>
        /// <param name="to1"></param>
        /// <param name="from2"></param>
        /// <param name="to2"></param>
        /// <returns></returns>
        float Remap(float value, float from1, float to1, float from2, float to2)
        {
            return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
        }
    }
}