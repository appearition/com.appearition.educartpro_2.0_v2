﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: ArTarget_Get.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Appearition.API;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Asset/List/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class Asset_List : BaseApiPost
    {
        public override int ApiVersion => 2;

        [System.Serializable]
        public class QueryContent
        {
            /// <summary>
            /// Will gather all the experiences whose name contain this string.
            /// </summary>
            public string Name;
            public string AssetId;
            /// <summary>
            /// Will gather all experiences which were created by this given username.
            /// </summary>
            public string CreatedByUsername;
            /// <summary>
            /// Starts at 0.
            /// </summary>
            public int Page;
            public int RecordsPerPage = 10;
            public List<string> Tags = new List<string>();
            /// <summary>
            /// If true, only experiences which contain all the given tags will be fetched.
            /// Otherwise, will fetch all experiences which contains at least one of the given tags.
            /// </summary>
            public bool FilterTagsUsingAnd = true;
            public bool IncludeTargetImages = true;
            public bool IncludeMedia = true;

            public QueryContent()
            {
            }

            public QueryContent(QueryContent cc)
            {
                Name = cc.Name;
                AssetId = cc.AssetId;
                CreatedByUsername = cc.CreatedByUsername;
                Page = cc.Page;
                RecordsPerPage = cc.RecordsPerPage;

                if (cc.Tags == null)
                    Tags = new List<string>();
                else
                    Tags = new List<string>(cc.Tags);

                FilterTagsUsingAnd = cc.FilterTagsUsingAnd;
                IncludeTargetImages = cc.IncludeTargetImages;
                IncludeMedia = cc.IncludeMedia;
            }
        }

        [System.Serializable]
        public class QueryOutcome
        {
            public List<Asset> assets = new List<Asset>();
            public int totalRecords;
        }

        public QueryOutcome Data;

        /// <summary>
        /// Post ApiData
        /// </summary>
        [System.Serializable]
        public class PostData : QueryContent
        {
            public PostData(QueryContent cc) : base(cc) { }
        }
    }
}