﻿using Appearition.ArTargetImageAndMedia;
using System;
using System.Collections;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition.Common;
using System.Linq;
using UnityEngine.Timeline;

[RequireComponent(typeof(EditData))]
public class EditSetup : MonoBehaviour
{

    public EditData eData;

    public InputField expName;

    public GameObject mediaObject;

    public GameObject markerObject;

    //HACK : Fix later
    public Image targetImage;

    public GameObject contentParent;

    //public Toggle isMarkerToggle;

    //public Toggle isMarkerlessToggle;

    public List<GameObject> instantiatedObjects;

    public GameObject markerInstance;

    public void Setup(ArTarget data)
    {
        for (int i = instantiatedObjects.Count - 1; i >= 0; i--)
        {
            Destroy(instantiatedObjects[i]);
        }
        instantiatedObjects.Clear();
        eData.arTarget = data;

        expName.text = Regex.Replace(eData.arTarget.name, @"\[.*?\]", "");
        eData.expName = expName.text;
        if(eData.arTarget.IsMarker)
        {
            SetupMarker();
        }
        //if (eData.arTarget.name.Contains("[MARKERLESS]"))
        //{
        //    isMarkerlessToggle.SetIsOnWithoutNotify(true);
        //    isMarkerToggle.SetIsOnWithoutNotify(false);
        //    eData.isMarker = false;
        //}
        //else
        //{
        //    isMarkerlessToggle.SetIsOnWithoutNotify(false);
        //    isMarkerToggle.SetIsOnWithoutNotify(true);
        //    SetupMarker();
        //    eData.isMarker = true;
        //}

        foreach (MediaFile mf in eData.arTarget.mediaFiles)
        {
            if (mf.mediaType.ToLower() == "audio" || mf.mediaType.ToLower() == "assetbundle_android" || mf.mediaType.ToLower() == "assetbundle_ios" || mf.mediaType.ToLower() == "model")
            {
                continue;
            }
            GameObject go = Instantiate(mediaObject, contentParent.transform);
            instantiatedObjects.Add(go);
            go.SetActive(true);
            go.GetComponentInChildren<MediaTypesSelection>().Setup(mf);
        }


    }

    void SetupMarker()
    {
        GameObject go = Instantiate(markerObject, contentParent.transform);
        StartCoroutine(FetchAndSetTargetImage(go));
        go.transform.SetSiblingIndex(3);
        instantiatedObjects.Add(go);
        go.SetActive(true);
        markerInstance = go;
    }

    public void DeleteMarker(bool value)
    {
        if (value)
            markerInstance.SetActive(false);
    }
    
    public void AddMarker(bool value)
    {
        if (value)
            markerInstance.SetActive(true);
    }


    IEnumerator FetchAndSetTargetImage(GameObject markerGo)
    {
        //Assume the targetimage has been downloaded already.
        string targetImagePath = ArTargetHandler.GetPathToTargetImage(eData.arTarget, eData.arTarget.targetImages[0]);
        if (!File.Exists(targetImagePath))
        {
            yield return ArTargetHandler.DownloadAssetsContent(new Asset[] { eData.arTarget }, true, false, false);
            if (eData.arTarget.targetImages.Count() > 0)
                targetImagePath = ArTargetHandler.GetPathToTargetImage(eData.arTarget, eData.arTarget.targetImages[0]);
        }

        if (eData.arTarget.targetImages.Count() > 0)
        {
            if (File.Exists(targetImagePath))
            {
                eData.arTarget.targetImages[0].image = ImageUtility.LoadOrCreateSprite(targetImagePath);
                markerGo.GetComponentInChildren<MarkerObjectEdit>().SetImage(eData.arTarget.targetImages[0].image);
            }
        }

    }

}
