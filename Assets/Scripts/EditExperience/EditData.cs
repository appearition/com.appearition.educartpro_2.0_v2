﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition.ArTargetImageAndMedia;


public class EditData : MonoBehaviour
{
    public string expName;

    public bool isMarker;

    public bool isPublic;

    public MarkerToUpload markerToUpload = new MarkerToUpload();

    public MediaToUpload mediaToUpload = new MediaToUpload();

    public ArTarget arTarget;
    
}
