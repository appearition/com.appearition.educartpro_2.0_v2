﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleGIFStatus : MonoBehaviour
{
    public GameObject gifUi;
    public Camera currentCamera;
    // Start is called before the first frame update
    private void OnEnable()
    {
        GetActiveCamera.currentCamera = currentCamera;
        gifUi.SetActive(true);
    }
    
    private void OnDisable()
    {
        gifUi.SetActive(false);
    }
}
