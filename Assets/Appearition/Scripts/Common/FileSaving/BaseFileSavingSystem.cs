﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: BaseFileSavingSystem.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

//using System;
//using System.Collections;
//using System.IO;
//using System.Security.Cryptography;
//using System.Text;
//using Appearition.Common;
//using Appearition.Common.ObjectExtensions;
//using UnityEngine;
//using UnityEngine.Networking;

//namespace Appearition
//{
//    public abstract class BaseFileSavingSystem
//    {
//        #region Settings

//        #region General 

//        /// <summary>
//        /// Full path where the root folder should be located. Eg: Application.streamingAssets.
//        /// Inside that folder, a new folder will be created with the name set in RootFolderName.
//        /// </summary>
//        public abstract string RootLocation { get; }

//        /// <summary>
//        /// The name of the root folder.
//        /// </summary>
//        public virtual string RootFolderName
//        {
//            get { return "AppearitionCloudData"; }
//        }

//        ///// <summary>
//        ///// Determines the root folder structure used for the EMS data.
//        ///// </summary>
//        //public virtual AppearitionConstants.FileGrouping FileGroupingType
//        //{
//        //    get { return AppearitionConstants.FileGrouping.GroupByModuleAndThenByAssetName; }
//        //}

//        ///// <summary>
//        ///// If the system does not group by module, defines whether or not the folders (under root) should be organized by modules.
//        ///// </summary>
//        //public virtual bool ShouldEachModuleHaveItsOwnSubFolder
//        //{
//        //    get { return true; }
//        //}

//        #endregion

//        #region Json

//        /// <summary>
//        /// The file extension used for JSON files. By default, JSON are stored as ".txt".
//        /// </summary>
//        protected virtual string JsonFileExtension
//        {
//            get { return ".txt"; }
//        }

//        ///// <summary>
//        ///// Whether or not the JsonFiles should be stored in a subfolder inside the chosen directory,
//        ///// or stored at the root of their chosen directory.
//        ///// </summary>
//        //protected virtual bool AreJsonFileStoredInSubFolder
//        //{
//        //    get { return true; }
//        //}

//        ///// <summary>
//        ///// The name of the Json subfolder inside the chosen directory if AreJsonFileStoredInSubFolder is true or if the FileGrouping is set to GroupByDataType.
//        ///// </summary>
//        //protected virtual string JsonSubFolderName
//        //{
//        //    get { return "JsonData"; }
//        //}

//        /// <summary>
//        /// Whether or not the JSON files should be encrypted or left as UTF8 text. If true, remember to override JsonEncryptionKey to customize the key.
//        /// </summary>
//        protected abstract bool EncryptJsonFiles { get; }


//        #region JSON File Encryption 

//        /// <summary>
//        /// Encrypts the given data using MD5 encryption, using the encryptionKey as a key.
//        /// </summary>
//        /// <returns>The data.</returns>
//        /// <param name="data">Data.</param>
//        public string EncryptData(string data)
//        {
//            //Prepare data
//            byte[] byteData = UTF8Encoding.UTF8.GetBytes(data);

//            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
//            {
//                //Prepare key
//                byte[] key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(JsonEncryptionKey));

//                using (TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider() {
//                    Key = key,
//                    Mode = CipherMode.ECB,
//                    Padding = PaddingMode.PKCS7
//                })
//                {
//                    //Prepare and encrypt final data
//                    ICryptoTransform cryptoTransform = tripleDes.CreateEncryptor();
//                    byte[] output = cryptoTransform.TransformFinalBlock(byteData, 0, byteData.Length);

//                    //Finally, output encrypted data
//                    return Convert.ToBase64String(output);
//                }
//            }
//        }

//        /// <summary>
//        /// Decrypts the given data using MD5 encryption, using the encryptionKey as a key.
//        /// </summary>
//        /// <returns>The data.</returns>
//        /// <param name="data">Data.</param>
//        public string DecryptData(string data)
//        {
//            //Prepare data
//            byte[] byteData = Convert.FromBase64String(data);

//            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
//            {
//                //Prepare key
//                byte[] key = md5.ComputeHash(UTF8Encoding.UTF8.GetBytes(JsonEncryptionKey));

//                using (TripleDESCryptoServiceProvider tripleDes = new TripleDESCryptoServiceProvider() {
//                    Key = key,
//                    Mode = CipherMode.ECB,
//                    Padding = PaddingMode.PKCS7
//                })
//                {
//                    //Prepare and decrypt final data
//                    ICryptoTransform cryptoTransform = tripleDes.CreateDecryptor();
//                    byte[] output = cryptoTransform.TransformFinalBlock(byteData, 0, byteData.Length);

//                    //Finally, output decrypted data
//                    return UTF8Encoding.UTF8.GetString(output);
//                }
//            }
//        }

//        #endregion

//        #endregion

//        //#region Media

//        ///// <summary>
//        ///// Whether or not Triggers and Targets should be stored in a separate folder.
//        ///// </summary>
//        //public virtual bool SeparateSubFolderForTriggerAndTargets
//        //{
//        //    get { return true; }
//        //}

//        ///// <summary>
//        ///// Name of the folder where Targets and Triggers are stored (when applicable), if SeparateSubFolderForTriggerAndTargets is set to true.
//        ///// </summary>
//        //public virtual string SubFolderForTriggerAndTargets
//        //{
//        //    get { return "Targets"; }
//        //}

//        ///// <summary>
//        ///// Whether or not each file should be separated by the file extension or not.
//        ///// </summary>
//        //public virtual bool SeparateSubFolderPerExtension
//        //{
//        //    get { return false; }
//        //}

//        //#endregion

//        #endregion

//        #region Directory Access 

//        /// <summary>
//        /// The path to the root directory where all the cloud data is stored (including media, triggers/targets and json).
//        /// </summary>
//        public virtual string GetPathToRootDirectory
//        {
//            get { return RootLocation + "/" + RootFolderName; }
//        }

//        /// <summary>
//        /// Returns the path to where the Json files are stored based on the currently used module.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="assetName">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <returns>The full path to the directory of where JSON files are to be stored on the FileSavingSystem settings.</returns>
//        public virtual string GetPathToJsonDirectory<T>(string assetName) where T : BaseHandler
//        {
//            switch (FileGroupingType)
//            {
//                case AppearitionConstants.FileGrouping.GroupByModule:
//                    return GetPathToRootDirectory + "/" + typeof(T).Namespace + (AreJsonFileStoredInSubFolder && !string.IsNullOrEmpty(JsonSubFolderName) ? "/" + JsonSubFolderName : "");

//                case AppearitionConstants.FileGrouping.GroupByModuleAndThenByAssetName:
//                    if (string.IsNullOrEmpty(assetName))
//                    {
//                        AppearitionLogger.LogError("Tried to get the path to the JSON root directory using the AssetName folder grouping and no asset name attached.");
//                        return "";
//                    }

//                    return GetPathToRootDirectory + "/" + typeof(T).Namespace + "/" + assetName +
//                           (AreJsonFileStoredInSubFolder && !string.IsNullOrEmpty(JsonSubFolderName) ? "/" + JsonSubFolderName : "");

//                case AppearitionConstants.FileGrouping.GroupByAssetName:
//                    if (string.IsNullOrEmpty(assetName))
//                    {
//                        AppearitionLogger.LogError("Tried to get the path to the JSON root directory using the AssetName folder grouping and no asset name attached.");
//                        return "";
//                    }

//                    return GetPathToRootDirectory + (ShouldEachModuleHaveItsOwnSubFolder ? "/" + typeof(T).Namespace : "") + "/" + assetName +
//                           (AreJsonFileStoredInSubFolder && !string.IsNullOrEmpty(JsonSubFolderName) ? "/" + JsonSubFolderName : "");

//                case AppearitionConstants.FileGrouping.GroupByDataType:
//                    return GetPathToRootDirectory + (ShouldEachModuleHaveItsOwnSubFolder ? "/" + typeof(T).Namespace : "") + (!string.IsNullOrEmpty(JsonSubFolderName)
//                               ? "/" + JsonSubFolderName
//                               : "/JSON");

//                default:
//                    AppearitionLogger.LogError("Unhandled File grouping type when trying to get the path to json directory.");
//                    return "";
//            }
//        }

//        /// <summary>
//        /// Based on the current type of media, the settings and a given asset if applicable, fetches the full path to the directory where this type of media is stored.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="fileName">The name of the associated file (including extension).</param>
//        /// <param name="typeOfMedia">Type of media required to find the directory.</param>
//        /// <param name="assetName">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <returns>The full path to the directory of said media based on the FileSavingSystem settings.</returns>
//        public virtual string GetPathToMediaDirectory<T>(string fileName, AppearitionConstants.SavedDataType typeOfMedia, string assetName) where T : BaseHandler
//        {
//            switch (FileGroupingType)
//            {
//                case AppearitionConstants.FileGrouping.GroupByModule:
//                    return GetPathToRootDirectory + "/" + typeof(T).Namespace + (typeOfMedia == AppearitionConstants.SavedDataType.TargetOrTrigger
//                               ? (SeparateSubFolderForTriggerAndTargets ? "/" + SubFolderForTriggerAndTargets : "")
//                               : (SeparateSubFolderPerExtension && Path.HasExtension(fileName) ? "/" + Path.GetExtension(fileName) : ""));

//                case AppearitionConstants.FileGrouping.GroupByModuleAndThenByAssetName:
//                    if (string.IsNullOrEmpty(assetName))
//                    {
//                        AppearitionLogger.LogError("Tried to get the path to the JSON root directory using the AssetName folder grouping and no asset name attached.");
//                        return "";
//                    }

//                    return GetPathToRootDirectory + "/" + typeof(T).Namespace + "/" + assetName + (typeOfMedia == AppearitionConstants.SavedDataType.TargetOrTrigger
//                               ? (SeparateSubFolderForTriggerAndTargets ? "/" + SubFolderForTriggerAndTargets : "")
//                               : (SeparateSubFolderPerExtension && Path.HasExtension(fileName) ? "/" + Path.GetExtension(fileName) : ""));

//                case AppearitionConstants.FileGrouping.GroupByAssetName:
//                    if (string.IsNullOrEmpty(assetName))
//                    {
//                        AppearitionLogger.LogError("Tried to get the path to the JSON root directory using the AssetName folder grouping and no asset name attached.");
//                        return "";
//                    }

//                    return GetPathToRootDirectory + (ShouldEachModuleHaveItsOwnSubFolder ? "/" + typeof(T).Namespace : "") + "/" + assetName +
//                           (typeOfMedia == AppearitionConstants.SavedDataType.TargetOrTrigger
//                               ? (SeparateSubFolderForTriggerAndTargets ? "/" + SubFolderForTriggerAndTargets : "")
//                               : (SeparateSubFolderPerExtension && Path.HasExtension(fileName) ? "/" + Path.GetExtension(fileName) : ""));

//                case AppearitionConstants.FileGrouping.GroupByDataType:
//                    return GetPathToRootDirectory + (ShouldEachModuleHaveItsOwnSubFolder ? "/" + typeof(T).Namespace : "") + (typeOfMedia == AppearitionConstants.SavedDataType.TargetOrTrigger
//                               ? (SeparateSubFolderForTriggerAndTargets ? "/" + SubFolderForTriggerAndTargets : "")
//                               : (SeparateSubFolderPerExtension && Path.HasExtension(fileName) ? "/" + Path.GetExtension(fileName) : ""));

//                default:
//                    AppearitionLogger.LogError("Unhandled File grouping type when trying to get the path to json directory.");
//                    return "";
//            }
//        }

//        static readonly string[] ComplexPathKeywords = new[] { "<Module>", "<Asset>", "<Media>", };

//        ///// <summary>
//        ///// 
//        ///// </summary>
//        ///// <param name="pathFormat"></param>
//        ///// <param name="pathParams"></param>
//        ///// <returns></returns>
//        //public string ParseComplexPath(string pathFormat, params string[] pathParams)
//        //{ 

//        //}

//        #endregion

//        #region File/Path Access

//        /// <summary>
//        /// Gets the path to the JSON file of a given name using a given module.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="jsonFileName">The name of this JSON file.</param>
//        /// <param name="assetName">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <returns></returns>
//        public virtual string GetPathToJsonFile<T>(string jsonFileName, string assetName) where T : BaseHandler
//        {
//            string directoryPath = GetPathToJsonDirectory<T>(assetName);

//            if (string.IsNullOrEmpty(directoryPath))
//            {
//                AppearitionLogger.LogError("Empty directory path when trying to fetch the JsonFile of name " + jsonFileName);
//                return "";
//            }

//            if (!Path.HasExtension(jsonFileName))
//                jsonFileName += JsonFileExtension;

//            return directoryPath + "/" + jsonFileName;
//        }

//        /// <summary>
//        /// Gets the path to the media file of a given name using a given module.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="fileName">The name of the media file, whether it's Target/Trigger or Media.</param>
//        /// <param name="typeOfMedia">The type of Media.</param>
//        /// <param name="assetName">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <returns></returns>
//        public virtual string GetPathToMediaFile<T>(string fileName, AppearitionConstants.SavedDataType typeOfMedia, string assetName) where T : BaseHandler
//        {
//            string directoryPath = GetPathToMediaDirectory<T>(fileName, typeOfMedia, assetName);

//            if (string.IsNullOrEmpty(directoryPath))
//            {
//                AppearitionLogger.LogError("Empty directory path when trying to fetch the media of name " + fileName);
//                return "";
//            }

//            return directoryPath + "/" + fileName;
//        }

//        #endregion

//        #region Save / Load File 

//        #region JSON 

//        #region Get 

//        /// <summary>
//        /// Tries to load the content of a JSON file at a given path.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="jsonFileName">The name of this JSON file (including extension).</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains the content of the JSON file.</param>
//        public void GetJsonContentFromFile<T>(string jsonFileName, string assetIfAny = "", Action<string> onComplete = null) where T : BaseHandler
//        {
//            GetJsonContentFromFile(GetPathToJsonFile<T>(jsonFileName, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Tries to load the content of a JSON file at a given path.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="jsonFileName">The name of this JSON file (including extension).</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains the content of the JSON file.</param>
//        public IEnumerator GetJsonContentFromFileProcess<T>(string jsonFileName, string assetIfAny = "", Action<string> onComplete = null) where T : BaseHandler
//        {
//            yield return GetJsonContentFromFileProcess(GetPathToJsonFile<T>(jsonFileName, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Tries to load the content of a JSON file at a given path.
//        /// </summary>
//        /// <param name="pathToJsonFile">The full path to the JSON file.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains the content of the JSON file.</param>
//        public void GetJsonContentFromFile(string pathToJsonFile, Action<string> onComplete = null)
//        {
//            //Empty check
//            if (string.IsNullOrEmpty(pathToJsonFile))
//            {
//                AppearitionLogger.LogError("Empty path provided when trying to get the content of a JSON file.");
//                if (onComplete != null)
//                    onComplete(null);
//                return;
//            }

//            //Existing check
//            if (File.Exists(pathToJsonFile))
//                AppearitionGate.Instance.StartCoroutine(GetJsonContentFromFileProcess(pathToJsonFile, onComplete));
//            else
//            {
//                AppearitionLogger.LogWarning(string.Format("Tried to get the content of the JSON file at path {0} but the file does not exist.", pathToJsonFile));
//                if (onComplete != null)
//                    onComplete(null);
//            }
//        }

//        /// <summary>
//        /// Tries to load the content of a JSON file at a given path.
//        /// </summary>
//        /// <param name="pathToJsonFile">The full path to the JSON file.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains the content of the JSON file.</param>
//        public virtual IEnumerator GetJsonContentFromFileProcess(string pathToJsonFile, Action<string> onComplete = null)
//        {
//            string output;
//            using (FileStream fs = new FileStream(pathToJsonFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
//            {
//                while (!fs.CanRead)
//                    yield return null;

//                using (StreamReader sw = new StreamReader(fs, Encoding.UTF8))
//                {
//                    output = sw.ReadToEnd();
//                }
//            }

//            if (EncryptJsonFiles)
//            {
//                if (!string.IsNullOrEmpty(JsonEncryptionKey))
//                    output = DecryptData(output);
//                else
//                    AppearitionLogger.LogWarning(string.Format("Tried to get the content of the JSON file at path {0} but the file is encrypted and the encryption key is empty.", pathToJsonFile));
//            }

//            if (onComplete != null)
//                onComplete(output);
//        }

//        #endregion

//        #region Save

//        /// <summary>
//        /// Saves JSON content into the appropriate file using a JSON file name (including extension).
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="jsonContent">The serialized content in JSON format, ready to be stored.</param>
//        /// <param name="jsonFileName">The name of the JSON file (including extension).</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Occurs once the process is complete and the JSON file has been written. The bool defines whether the process was successful or not.</param>
//        public void SaveJsonContentToFile<T>(string jsonContent, string jsonFileName, string assetIfAny = "", Action<bool> onComplete = null) where T : BaseHandler
//        {
//            SaveJsonContentToFile(jsonContent, GetPathToJsonFile<T>(jsonFileName, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Saves JSON content into the appropriate file using a JSON file name (including extension).
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="jsonContent">The serialized content in JSON format, ready to be stored.</param>
//        /// <param name="jsonFileName">The name of the JSON file (including extension).</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Occurs once the process is complete and the JSON file has been written. The bool defines whether the process was successful or not.</param>
//        public IEnumerator JsonWritingProcess<T>(string jsonContent, string jsonFileName, string assetIfAny = "", Action<bool> onComplete = null) where T : BaseHandler
//        {
//            yield return SaveJsonContentToFileProcess(jsonContent, GetPathToJsonFile<T>(jsonFileName, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Saves JSON content into a file at the given full path (including extension).
//        /// </summary>
//        /// <param name="jsonContent">The serialized content in JSON format, ready to be stored.</param>
//        /// <param name="pathToJsonFile">The full path to the JSON file.</param>
//        /// <param name="onComplete">Occurs once the process is complete and the JSON file has been written. The bool defines whether the process was successful or not.</param>
//        public void SaveJsonContentToFile(string jsonContent, string pathToJsonFile, Action<bool> onComplete = null)
//        {
//            AppearitionGate.Instance.StartCoroutine(SaveJsonContentToFileProcess(jsonContent, pathToJsonFile, onComplete));
//        }

//        /// <summary>
//        /// Process of writing the JSON content into a file.
//        /// Locks the current file being written, and waits for the file to be unlocked before writing on it if locked by another process.
//        /// </summary>
//        /// <param name="jsonContent">The serialized content in JSON format, ready to be stored.</param>
//        /// <param name="pathToJsonFile">The full path to the JSON file.</param>
//        /// <param name="onComplete">Occurs once the process is complete and the JSON file has been written. The bool defines whether the process was successful or not.</param>
//        public virtual IEnumerator SaveJsonContentToFileProcess(string jsonContent, string pathToJsonFile, Action<bool> onComplete = null)
//        {
//            //Empty check
//            if (string.IsNullOrEmpty(pathToJsonFile))
//            {
//                AppearitionLogger.LogError("Empty path provided when trying to get the content of a JSON file.");
//                if (onComplete != null)
//                    onComplete(false);
//                yield break;
//            }

//            // If needed, encrypt json content.
//            if (EncryptJsonFiles)
//            {
//                if (!string.IsNullOrEmpty(JsonEncryptionKey))
//                    jsonContent = EncryptData(jsonContent);
//                else
//                    AppearitionLogger.LogWarning(string.Format("Tried to encrypt the content of the JSON file to be saved at path path {0} but the encryption key is empty.", pathToJsonFile));
//            }

//            using (FileStream fs = new FileStream(pathToJsonFile, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None))
//            {
//                while (!fs.CanWrite)
//                    yield return null;

//                using (StreamWriter sw = new StreamWriter(fs, Encoding.UTF8))
//                {
//                    sw.Write(jsonContent);
//                }
//            }

//            if (onComplete != null)
//                onComplete(true);
//        }

//        #endregion

//        #region Delete

//        /// <summary>
//        /// Deletes the JSON file of a given name (including extension) in the appropriate folder based on the FileSavingSystem settings.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="jsonFileName">The name of the JSON file (including extension).</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Occurs once the process is complete and the JSON file has been written. The bool defines whether the process was successful or not.</param>
//        public void DeleteJsonFile<T>(string jsonFileName, string assetIfAny = "", Action<bool> onComplete = null) where T : BaseHandler
//        {
//            DeleteJsonFile(GetPathToJsonFile<T>(jsonFileName, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Deletes the JSON file of a given name (including extension) in the appropriate folder based on the FileSavingSystem settings.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="jsonFileName">The name of the JSON file (including extension).</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Occurs once the process is complete and the JSON file has been written. The bool defines whether the process was successful or not.</param>
//        public IEnumerator DeleteJsonFileProcess<T>(string jsonFileName, string assetIfAny = "", Action<bool> onComplete = null) where T : BaseHandler
//        {
//            yield return DeleteJsonFileProcess(GetPathToJsonFile<T>(jsonFileName, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Deletes the JSON file of a given name at a given path.
//        /// </summary>
//        /// <param name="pathToJsonFile">Full path to the JSON file.</param>
//        /// <param name="onComplete">Occurs once the process is complete and the JSON file has been written. The bool defines whether the process was successful or not.</param>
//        public void DeleteJsonFile(string pathToJsonFile, Action<bool> onComplete = null)
//        {
//            AppearitionGate.Instance.StartCoroutine(DeleteJsonFileProcess(pathToJsonFile, onComplete));
//        }

//        /// <summary>
//        /// Deletes the JSON file of a given name at a given path.
//        /// </summary>
//        /// <param name="pathToJsonFile">Full path to the JSON file.</param>
//        /// <param name="onComplete">Occurs once the process is complete and the JSON file has been written. The bool defines whether the process was successful or not.</param>
//        public virtual IEnumerator DeleteJsonFileProcess(string pathToJsonFile, Action<bool> onComplete = null)
//        {
//            //Empty check
//            if (string.IsNullOrEmpty(pathToJsonFile))
//            {
//                AppearitionLogger.LogWarning("Empty path provided when trying to delete a JSON file.");
//                if (onComplete != null)
//                    onComplete(false);
//                yield break;
//            }

//            //No file check
//            if (!File.Exists(pathToJsonFile))
//            {
//                AppearitionLogger.LogWarning(string.Format("Tried delete a JSON file at path {0} but no file was found at that location.", pathToJsonFile));
//                if (onComplete != null)
//                    onComplete(false);
//                yield break;
//            }

//            while (true)
//            {
//                try
//                {
//                    File.Delete(pathToJsonFile);

//                    //No problem? break!
//                    break;
//                } catch
//                {
//                    //Issue occured when trying to delete. Wait a bit.. Unless file is already gone?
//                    if (!File.Exists(pathToJsonFile))
//                        break;
//                }

//                yield return new WaitForSeconds(0.5f);
//            }

//            if (onComplete != null)
//                onComplete(true);
//        }

//        #endregion

//        #endregion

//        #region Media

//        #region Get

//        /// <summary>
//        /// Fetches the media stored locally of a given name with the directory path generated based on the FileSavingSystem settings.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="fileName">The name of the Media file (including extension).</param>
//        /// <param name="typeOfMedia">The type of media given. Mainly used to determine what folder this media should be placed in.</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains the content of the data of the Media.</param>
//        public void GetMediaContentFromFile<T>(string fileName, AppearitionConstants.SavedDataType typeOfMedia, string assetIfAny = "", Action<byte[]> onComplete = null) where T : BaseHandler
//        {
//            GetMediaContentFromFile(GetPathToMediaFile<T>(fileName, typeOfMedia, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Fetches the media stored locally of a given name with the directory path generated based on the FileSavingSystem settings.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="fileName">The name of the Media file (including extension).</param>
//        /// <param name="typeOfMedia">The type of media given. Mainly used to determine what folder this media should be placed in.</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains the content of the data of the Media.</param>
//        public virtual IEnumerator GetMediaContentFromFileProcess<T>(string fileName, AppearitionConstants.SavedDataType typeOfMedia, string assetIfAny = "", Action<byte[]> onComplete = null)
//            where T : BaseHandler
//        {
//            yield return GetMediaContentFromFileProcess(GetPathToMediaFile<T>(fileName, typeOfMedia, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Fetches the media stored locally of a given name at a given path.
//        /// </summary>
//        /// <param name="pathToMediaFile">The full path to the media file.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains the content of the data of the Media.</param>
//        public void GetMediaContentFromFile(string pathToMediaFile, Action<byte[]> onComplete = null)
//        {
//            AppearitionGate.Instance.StartCoroutine(GetMediaContentFromFileProcess(pathToMediaFile, onComplete));
//        }

//        /// <summary>
//        /// Fetches the media stored locally of a given name at a given path.
//        /// </summary>
//        /// <param name="pathToMediaFile">The full path to the media file.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains the content of the data of the Media.</param>
//        public virtual IEnumerator GetMediaContentFromFileProcess(string pathToMediaFile, Action<byte[]> onComplete = null)
//        {
//            //Empty check
//            if (string.IsNullOrEmpty(pathToMediaFile))
//            {
//                AppearitionLogger.LogError("Empty path provided when trying to get the content of a media file.");
//                if (onComplete != null)
//                    onComplete(null);
//                yield break;
//            }

//            //No file check
//            if (!File.Exists(pathToMediaFile))
//            {
//                AppearitionLogger.LogError(string.Format("Tried to load a Media File at path {0} but no file was found at that location.", pathToMediaFile));
//                if (onComplete != null)
//                    onComplete(null);
//                yield break;
//            }

//            UnityWebRequest loadMediaProcess = UnityWebRequest.Get(string.Format("file:///{0}", pathToMediaFile.Trim()));

//            yield return loadMediaProcess.SendWebRequest();

//            byte[] outcome = null;
//            if (loadMediaProcess.downloadHandler.data != null && loadMediaProcess.downloadHandler.data.Length > 0)
//            {
//                //Make an object out of it !
//                outcome = loadMediaProcess.downloadHandler.data;
//            }
//            else
//                AppearitionLogger.LogWarning(string.Format("Tried to load data at the path {0} but failed.", pathToMediaFile));

//            if (onComplete != null)
//                onComplete(outcome);
//        }

//        #endregion

//        #region Save

//        /// <summary>
//        /// Saves the Media File data to the appropriate folder using the given parameters and the settings as on this FileSavingSystem.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="mediaData">The content of this Media File.</param>
//        /// <param name="fileName">The name of the Media file (including extension).</param>
//        /// <param name="typeOfMedia">The type of media given. Mainly used to determine what folder this media should be placed in.</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public void SaveMediaContentToFile<T>(byte[] mediaData, string fileName, AppearitionConstants.SavedDataType typeOfMedia, string assetIfAny = "", Action<bool> onComplete = null)
//            where T : BaseHandler
//        {
//            SaveMediaContentToFile(mediaData, GetPathToMediaFile<T>(fileName, typeOfMedia, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Saves the Media File data to the appropriate folder using the given parameters and the settings as on this FileSavingSystem.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="mediaData">The content of this Media File.</param>
//        /// <param name="fileName">The name of the Media file (including extension).</param>
//        /// <param name="typeOfMedia">The type of media given. Mainly used to determine what folder this media should be placed in.</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public IEnumerator SaveMediaContentToFileProcess<T>(byte[] mediaData, string fileName, AppearitionConstants.SavedDataType typeOfMedia, string assetIfAny = "", Action<bool> onComplete = null)
//            where T : BaseHandler
//        {
//            yield return SaveMediaContentToFileProcess(mediaData, GetPathToMediaFile<T>(fileName, typeOfMedia, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Saves the Media File data to the given path.
//        /// </summary>
//        /// <param name="mediaData">The content of this Media File.</param>
//        /// <param name="pathToMediaFile">The full path to the media file.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public void SaveMediaContentToFile(byte[] mediaData, string pathToMediaFile, Action<bool> onComplete = null)
//        {
//            AppearitionGate.Instance.StartCoroutine(SaveMediaContentToFileProcess(mediaData, pathToMediaFile, onComplete));
//        }

//        /// <summary>
//        /// Saves the Media File data to the given path.
//        /// </summary>
//        /// <param name="mediaData">The content of this Media File.</param>
//        /// <param name="pathToMediaFile">The full path to the media file.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public virtual IEnumerator SaveMediaContentToFileProcess(byte[] mediaData, string pathToMediaFile, Action<bool> onComplete = null)
//        {
//            //Empty check
//            if (string.IsNullOrEmpty(pathToMediaFile))
//            {
//                AppearitionLogger.LogError("Empty path provided when trying to save the content of a media file.");
//                if (onComplete != null)
//                    onComplete(false);
//                yield break;
//            }

//            //Empty data check
//            if (mediaData == null)
//            {
//                AppearitionLogger.LogWarning(string.Format("Tried to save empty data for a media file at path {0}. Aborting process.", pathToMediaFile));
//                if (onComplete != null)
//                    onComplete(false);
//                yield break;
//            }

//            //Save it!
//            File.WriteAllBytes(pathToMediaFile, mediaData);

//            if (onComplete != null)
//                onComplete(true);
//        }

//        #endregion

//        #region Delete

//        /// <summary>
//        /// Deletes the media file using a file name (including extension) at a location generated by the FileSavingSystem settings.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="fileName">The name of the Media file (including extension).</param>
//        /// <param name="typeOfMedia">The type of media given. Mainly used to determine what folder this media should be placed in.</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public void DeleteMediaFile<T>(string fileName, AppearitionConstants.SavedDataType typeOfMedia, string assetIfAny = "", Action<bool> onComplete = null) where T : BaseHandler
//        {
//            DeleteMediaFile(GetPathToMediaFile<T>(fileName, typeOfMedia, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Deletes the media file using a file name (including extension) at a location generated by the FileSavingSystem settings.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="fileName">The name of the Media file (including extension).</param>
//        /// <param name="typeOfMedia">The type of media given. Mainly used to determine what folder this media should be placed in.</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public IEnumerator DeleteMediaFileProcess<T>(string fileName, AppearitionConstants.SavedDataType typeOfMedia, string assetIfAny = "", Action<bool> onComplete = null) where T : BaseHandler
//        {
//            yield return DeleteMediaFileProcess(GetPathToMediaFile<T>(fileName, typeOfMedia, assetIfAny), onComplete);
//        }

//        /// <summary>
//        /// Deletes the media file at a given path.
//        /// </summary>
//        /// <param name="pathToMediaFile">Full path to the media file.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public void DeleteMediaFile(string pathToMediaFile, Action<bool> onComplete = null)
//        {
//            AppearitionGate.Instance.StartCoroutine(DeleteMediaFileProcess(pathToMediaFile, onComplete));
//        }

//        /// <summary>
//        /// Deletes the media file at a given path.
//        /// </summary>
//        /// <param name="pathToMediaFile">Full path to the media file.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public virtual IEnumerator DeleteMediaFileProcess(string pathToMediaFile, Action<bool> onComplete = null)
//        {
//            //Empty check
//            if (string.IsNullOrEmpty(pathToMediaFile))
//            {
//                AppearitionLogger.LogWarning("Empty path provided when trying to delete a media file.");
//                if (onComplete != null)
//                    onComplete(false);
//                yield break;
//            }

//            //No file check
//            if (!File.Exists(pathToMediaFile))
//            {
//                AppearitionLogger.LogWarning(string.Format("Tried delete a media file at path {0} but no file was found at that location.", pathToMediaFile));
//                if (onComplete != null)
//                    onComplete(false);
//                yield break;
//            }

//            while (true)
//            {
//                try
//                {
//                    File.Delete(pathToMediaFile);

//                    //No problem? break!
//                    break;
//                } catch
//                {
//                    //Issue occured when trying to delete. Wait a bit.. Unless file is already gone?
//                    if (!File.Exists(pathToMediaFile))
//                        break;
//                }

//                yield return new WaitForSeconds(0.5f);
//            }

//            if (onComplete != null)
//                onComplete(true);
//        }

//        #endregion

//        #region Copy

//        /// <summary>
//        /// Copies the media file from a source location, to a destination generated based on the FileSavingSystem settings.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="pathToSourceFile">Full path to the source media file.</param>
//        /// <param name="fileName">The name of the Media file (including extension).</param>
//        /// <param name="typeOfMedia">The type of media given. Mainly used to determine what folder this media should be placed in.</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public void CopyFileToMediaDirectory<T>(string pathToSourceFile, string fileName, AppearitionConstants.SavedDataType typeOfMedia, string assetIfAny = "", Action<bool> onComplete = null)
//            where T : BaseHandler
//        {
//            CopyFileToMediaDirectory(pathToSourceFile, GetPathToMediaFile<T>(fileName, typeOfMedia, assetIfAny));
//        }

//        /// <summary>
//        /// Copies the media file from a source location to a given destination full path.
//        /// </summary>
//        /// <param name="pathToSourceFile">Full path to the source media file.</param>
//        /// <param name="pathToDestinationFile">Full path to the destination media file.</param>
//        /// <param name="onComplete">Called once the process has completed. Contains whether or not the operation was successful.</param>
//        public virtual void CopyFileToMediaDirectory(string pathToSourceFile, string pathToDestinationFile, Action<bool> onComplete = null)
//        {
//            //Empty check
//            if (string.IsNullOrEmpty(pathToSourceFile) || string.IsNullOrEmpty(pathToDestinationFile))
//            {
//                AppearitionLogger.LogWarning("Empty source path provided when trying to copy a media file.");
//                if (onComplete != null)
//                    onComplete(false);
//                return;
//            }

//            //Copy!
//            try
//            {
//                File.Copy(pathToSourceFile, pathToDestinationFile, true);

//                if (onComplete != null)
//                    onComplete(true);
//            } catch
//            {
//                if (onComplete != null)
//                    onComplete(false);
//            }
//        }

//        #endregion

//        #region Checksum Check

//        /// <summary>
//        /// Whether or not the existing file's checksum matches a given checksum.
//        /// </summary>
//        /// <typeparam name="T">Handler used to access this method. If used externally, attached the handler relevant to the module you're accessing.</typeparam>
//        /// <param name="fileName">The name of the Media file (including extension).</param>
//        /// <param name="typeOfMedia">The type of media given. Mainly used to determine what folder this media should be placed in.</param>
//        /// <param name="checksum">The checksum of the media file's current version as it came down from the EMS.</param>
//        /// <param name="assetIfAny">If the FileGrouping type requires the AssetName, please provide it here.</param>
//        /// <returns>Whether or not the existing file's checksum matches a given checksum.</returns>
//        public bool IsThereExistingMediaWithMatchingChecksum<T>(string fileName, AppearitionConstants.SavedDataType typeOfMedia, string checksum, string assetIfAny = "")
//            where T : BaseHandler
//        {
//            return IsThereExistingMediaWithMatchingChecksum(GetPathToMediaFile<T>(fileName, typeOfMedia, assetIfAny), checksum);
//        }

//        /// <summary>
//        /// Whether or not the existing file's checksum matches a given checksum.
//        /// </summary>
//        /// <param name="fullPath">The full path to the media file.</param>
//        /// <param name="checksum">The checksum of the media file's current version as it came down from the EMS.</param>
//        /// <returns>Whether or not the existing file's checksum matches a given checksum.</returns>
//        public virtual bool IsThereExistingMediaWithMatchingChecksum(string fullPath, string checksum)
//        {
//            if (!File.Exists(fullPath))
//                return false;

//            string existingFileChecksum = "";
//            try
//            {
//                using (var file = new FileStream(fullPath, FileMode.Open, FileAccess.ReadWrite, FileShare.Read))
//                {
//                    existingFileChecksum = file.Checksum();
//                }
//            } catch (IOException)
//            {
//            }

//            return existingFileChecksum.Equals(checksum, StringComparison.OrdinalIgnoreCase);
//        }

//        #endregion

//        #endregion

//        #endregion
//    }
//}

