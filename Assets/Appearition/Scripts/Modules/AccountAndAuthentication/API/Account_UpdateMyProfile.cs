﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: Account_UpdateMyProfile.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using Appearition.API;

namespace Appearition.AccountAndAuthentication.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Account/UpdateMyProfile/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class Account_UpdateMyProfile : BaseApiPost
    {
        public override AuthenticationOverrideType AuthenticationOverride
        {
            get { return AuthenticationOverrideType.SessionToken; }
        }

        public override int ApiVersion
        {
            get { return 2; }
        }

        //Variables
        public ApiData Data;

        [System.Serializable]
        public class ApiData
        {
        }

        [System.Serializable]
        public class PostApi : Profile
        {
        }
    }
}