﻿using Appearition.API;

namespace Appearition.Packaging.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Package/SelectPackage/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class Package_SelectPackage : BaseApiPost
    {
        public override AuthenticationOverrideType AuthenticationOverride
        {
            get { return AuthenticationOverrideType.SessionToken; }
        }

        public override int ApiVersion
        {
            get { return 2; }
        }

        //Variables
        public ApiData Data;

        [System.Serializable]
        public class ApiData : PackageProcessingStatus
        {
        }

        [System.Serializable]
        public class PostApi : PackageProcessingStatus
        {
        }
    }
}