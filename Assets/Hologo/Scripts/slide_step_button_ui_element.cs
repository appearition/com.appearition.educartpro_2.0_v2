﻿using UnityEngine;
using UnityEngine.UI;

namespace Hologo2
{
    /// <summary>
    /// Created by: Hamid (hamidibrahim@gmail.com) - 19 july 2019
    /// Modified by: Shariz - 20th September 2020
    /// Slide step button ui element script
    /// This activates and in activates plus add listeners to the Slide Step buttons
    /// </summary>
    public class slide_step_button_ui_element : MonoBehaviour
    {
        public Button stepButton;
        public Text stepButtonText;
        public int id;


        // Set Slide step button names and add listeners
        public void fillUpButton(int myId, string text, CallbackForId callbackId = null)
        {
            id = myId;
            stepButtonText.text = text;

            stepButton.onClick.RemoveAllListeners();
            if (callbackId != null)
                stepButton.onClick.AddListener(()=> callbackId(id));
        }




        // Changing active step
        public void amIActive(int outId)
        {
            Color activeColor = new Color(0, 0, 0, 1f);
            Color inActiveColor = new Color(0, 0, 0, 0.4f);
            if (outId == id)
            {
                stepButtonText.color = activeColor;
            }
            else
            {
                stepButtonText.color = inActiveColor;

            }
        }

    }
}
