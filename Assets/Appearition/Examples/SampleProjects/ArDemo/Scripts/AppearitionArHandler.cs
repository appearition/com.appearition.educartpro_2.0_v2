﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using Appearition.Tenant;
using UnityEngine.Events;

namespace Appearition.Example
{
    /// <summary>
    /// Main handle which controls anything related to the AR implementation.
    /// It contains all events, states and settings related to the Ar implementation.
    /// </summary>
    [RequireComponent(typeof(IArProviderHandler))]
    public class AppearitionArHandler : MonoBehaviour
    {
        #region Enums

        /// <summary>
        /// Contains the different possible states for an Target being tracked.
        /// </summary>
        public enum TargetState
        {
            None = -1,
            TargetFound,
            TargetLost,
            Unknown
        }

        #endregion

        #region Events

        public delegate void InitializationComplete();

        /// <summary>
        /// Occurs whenever both Vuforia and the AppearitionArHandler are done initializing, and ready to do some AR.
        /// </summary>
        public static event InitializationComplete OnInitializationComplete;

        public delegate void ScanStateChanged(bool isScanning);

        /// <summary>
        /// Called anytime the scan value has been changed.
        /// </summary>
        public static event ScanStateChanged OnScanStateChanged;


        public delegate void TargetStateChanged(AppearitionExperience arAsset, TargetState newState);

        /// <summary>
        /// Event triggered whenever the given target has been 
        /// </summary>
        public static event TargetStateChanged OnTargetStateChanged;

        #endregion

        #region Singleton

        private static AppearitionArHandler _instance;

        /// <summary>
        /// Singleton of the Appearition ArHandler.
        /// </summary>
        public static AppearitionArHandler Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<AppearitionArHandler>();

                    if (_instance == null)
                        _instance = new GameObject("Appearition Ar Handler").AddComponent<AppearitionArHandler>();
                }

                return _instance;
            }
        }

        #endregion

        //References
        private IArProviderHandler _providerHandler;

        /// <summary>
        /// Locates the current Ar Provider handler attached on this object.
        /// </summary>
        public IArProviderHandler ProviderHandler
        {
            get
            {
                if (_providerHandler == null)
                {
                    //HACK for now, just use ArFoundationHandler
                    //_providerHandler = GetComponent<IArProviderHandler>();
                    _providerHandler = FindObjectOfType<VuforiaCloudProviderHandler>();
                }

                return _providerHandler;
            }
             set
            {
                _providerHandler = value;
                AppearitionLogger.LogDebug("Ar Provider has been set: " + _providerHandler?.GetType().Name);
            }
        }

        /// <summary>
        /// Returns whether or not the current ArProvider is currently scanning and looking for targets.
        /// </summary>
        /// <value><c>true</c> if is provider scanning; otherwise, <c>false</c>.</value>
        public bool IsProviderScanning
        {
            get { return ProviderHandler != null && ProviderHandler.IsScanning; }
        }

        public static bool UseExtendedTracking
        {
            get {
                if (Instance == null || Instance.ProviderHandler == null)
                    return false;
                return Instance.ProviderHandler.UseExtendedTracking;
            }
        }

        /// <summary>
        /// Returns the current state of the target being tracked, if any.
        /// </summary>
        /// <value>The state of the current target.</value>
        public TargetState CurrentTargetState { get; private set; }

        //Internal Variables
        [Tooltip("Whether all fullscreen medias should have a black transparent background behind them.")]
        public bool shouldFullscreenMediaHaveBlackBackground = true;
        [Tooltip("Whether or not the scan mode should timeout, using the delay set below.")]
        public bool disableScanningModeOverTime = true;
        [Tooltip("Delay before the scan mode automatically times out, if disableScanningModeOverTime is set to true.")]
        public float disableScanningModeDelay = 10;
        public bool enableArProviderOnceInitializationComplete = true;

        /// <summary>
        /// Defines whether or not the ArProvider is currently initialized or not.
        /// </summary>
        public bool IsArProviderInitialized { get; protected set; }

        private bool _isInitialized;

        /// <summary>
        /// Whether or not the Appearition Ar Handler's initialization is complete.
        /// Do note that it will not complete until Vuforia's Initialization is also complete.
        /// </summary>
        public bool IsInitialized
        {
            get { return _isInitialized; }
            protected set
            {
                bool previousValue = _isInitialized;

                _isInitialized = value;

                if (!previousValue && _isInitialized)
                {
                    if (OnInitializationComplete != null)
                        OnInitializationComplete();
                }
            }
        }

        //Storage ApiData
        /// <summary>
        /// Contains all the assets from the attached EMS.
        /// </summary>
        public List<Asset> CurrentDataRaw { get; private set; }

        void Awake()
        {
            //Initialize storages
            CurrentDataRaw = new List<Asset>();
        }

        IEnumerator Start()
        {
            //Get license first
            yield return ProviderHandler.SetupLicense();

            //Launch the Asset fetching request
            bool? hasRequestFinished = null;

            ArTargetHandler.GetChannelExperiences(CurrentDataRaw, false, false, true, onComplete: isSuccess => { hasRequestFinished = isSuccess; });

            while (!hasRequestFinished.HasValue)
                yield return null;

            if (hasRequestFinished.Value)
            {
                AppearitionLogger.LogInfo(string.Format("Assets successfully fetched from the EMS! Got {0} Assets to use within this channel.", CurrentDataRaw.Count));
            }
            else
                AppearitionLogger.LogError("An error occured when trying to fetch the assets from the EMS, and no ApiData is available.");

            IsInitialized = true;

            //Activate the Provider and let it do scanning.
            if (enableArProviderOnceInitializationComplete)
                ProviderHandler.ChangeArProviderActiveState(true);
        }

        private void EmsDataContainer_OnAssetsRefreshed(List<ArTarget> artargets, List<Asset> assets)
        {
            RefreshAugmentableAssets(assets);
        }


        #region Experience Handling 

        ///// <summary>
        ///// Initializes a single target using the AssetId of said target.
        ///// </summary>
        ///// <param name="assetId"></param>
        //public static AppearitionExperience CreateExperienceFromAsset(string assetId)
        //{
        //    if (string.IsNullOrEmpty(assetId))
        //        return null;

        //    return CreateExperienceFromAsset(Instance.CurrentDataRaw.Find(o => o.assetId.Equals(assetId)));
        //}

        /// <summary>
        /// Initializes a single target using the Asset container of said target.
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="onComplete"></param>
        public static AppearitionExperience CreateExperienceFromAsset(Asset asset, Action<bool> onComplete)
        {
            if (asset == null)
                return null;
            
            var tmpExperience = new GameObject(string.Format("Experience Name: {0}, Id: {1}", asset.name, asset.assetId)).AddComponent<AppearitionExperience>();
            tmpExperience.transform.SetParent(Instance.transform);
            //Init it
            if (asset.mediaFiles == null || asset.mediaFiles.Length == 0)
                Instance.StartCoroutine(GetMediafilesProcess(tmpExperience, asset, onComplete));
            else
            {
                tmpExperience.SetupExperience(asset);
                onComplete?.Invoke(true);
            }

            return tmpExperience;
        }
        
        static IEnumerator GetMediafilesProcess(AppearitionExperience experience, Asset asset, Action<bool> onComplete)
        {
            string assetId = asset.assetId;
            //yield return ArTargetHandler.GetSpecificMediasInAssetByQueryProcess(asset.assetId, null, success => asset.mediaFiles = success.ToArray());
            var assetQuery = ArTargetConstant.GetDefaultAssetListQuery();
            assetQuery.AssetId = asset.assetId;
            yield return ArTargetHandler.GetSpecificExperiencesByQueryProcess(0, assetQuery, false, true, false, success => asset = success.FirstOrDefault());

            //If the user is not logged in, no need to go further.
            if (!AppearitionGate.Instance.CurrentUser.IsUserLoggedIn)
            {
                if (asset != null)
                    experience.SetupExperience(asset);
                
                onComplete?.Invoke(asset != null);
                yield break;
            }
            
            if (asset == null)
            {
                var arTargetQuery = ArTargetConstant.GetDefaultArTargetListQuery();
                arTargetQuery.AssetId = assetId;
                yield return ArTargetHandler.GetSpecificArTargetByQueryProcess(0, arTargetQuery, false, true, success => asset = success.FirstOrDefault());
            }
            
            if(asset != null)
                experience.SetupExperience(asset);

            onComplete?.Invoke(asset != null);
        }
        #endregion

        #region Event Handling

        /// <summary>
        /// Informs the AppearitionArHandler about the current initialization state of the ArProvider.
        /// </summary>
        /// <param name="newValue"></param>
        public void ArProviderInitializationStateChanged(bool newValue)
        {
            IsArProviderInitialized = newValue;
        }

        /// <summary>
        /// Called only by ArProviers. Handles the OnScanStateChanged event.
        /// </summary>
        /// <param name="newValue"></param>
        public void ArProviderScanStateChanged(bool newValue)
        {
            //Clear current target being scanned. Good 8AR-like applications.
            CurrentTargetState = TargetState.None;

            if (OnScanStateChanged != null)
                OnScanStateChanged(newValue);
        }

        /// <summary>
        /// Should be called by the ArProvider whenever an ArTarget has been found or lost. Handles the events.
        /// </summary>
        /// <param name="experienceTracked"></param>
        /// <param name="newState"></param>
        public void ArProviderTargetStateChanged(AppearitionExperience experienceTracked, TargetState newState)
        {
            //Retardation test
            CurrentTargetState = newState;

            if (OnTargetStateChanged != null)
                OnTargetStateChanged(experienceTracked, newState);
        }

        /// <summary>
        /// Should be called by the ArProvider whenever an Target Image was picked up, and whether it successfully loaded, or if any error occurred.
        /// </summary>
        /// <param name="isSuccess"></param>
        public static void ArProviderTargetLoaded(bool isSuccess)
        {
            //If failed, do clean up the targets.
            if (!isSuccess)
                Instance.ClearCurrentTargetBeingTracked(true);
            else
            {
                //Change the scanning state accordingly. If successfully loaded, no need to scan.
                Instance.ChangeArProviderScanningState(!Instance.ProviderHandler.DisableScanningOnTargetFound);
            }
        }

        #endregion

        #region Ar Provider Handling

        /// <summary>
        /// Called by the script in charge of refreshing and storing all the assets which the user can use.
        /// Refreshes the library of the provider if it has offline capability (non-cloud reco).
        /// </summary>
        /// <param name="assets"></param>
        public void RefreshAugmentableAssets(List<Asset> assets)
        {
            if (ProviderHandler is IOfflineMarkerArProviderHandler handler)
                handler.UpdateTrackableAssetCollection(assets);
        }

        public void ChangeArProviderActiveState(bool desiredState)
        {
            if (ProviderHandler != null)
                ProviderHandler.ChangeArProviderActiveState(desiredState);
        }

        /// <summary>
        /// Changes the scanning state of the current Ar Provider, if setup properly..
        /// </summary>
        /// <param name="desiredState"></param>
        public void ChangeArProviderScanningState(bool desiredState)
        {
            if (ProviderHandler != null)
                ProviderHandler.ChangeScanningState(desiredState);
        }

        /// <summary>
        /// If any target is currently being tracked, removes it and clears it. Additionally, can start scanning again.
        /// </summary>
        public void ClearCurrentTargetBeingTracked(bool doScanAgain = true)
        {
            if (ProviderHandler != null)
                ProviderHandler.ClearTargetsBeingTracked(doScanAgain);
        }

        #endregion
    }
}