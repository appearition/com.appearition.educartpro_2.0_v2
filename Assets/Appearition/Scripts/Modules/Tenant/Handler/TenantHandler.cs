﻿using System;
using System.Collections;
using System.Collections.Generic;
using Appearition.Common;
using Appearition.Tenant.API;


namespace Appearition.Tenant
{
    public class TenantHandler : BaseHandler
    {
        public static void GetTenantSettings(Action<List<Setting>> onSuccess = null, Action<EmsError> onFailure = null, Action<bool> onComplete = null)
        {
            AppearitionGate.Instance.StartCoroutine(GetTenantSettingsProcess(onSuccess, onFailure, onComplete));
        }

        public static IEnumerator GetTenantSettingsProcess(Action<List<Setting>> onSuccess = null, Action<EmsError> onFailure = null, Action<bool> onComplete = null)
        {
            //Wait for internet check, or handle offline.
            while (!AppearitionGate.HasInternetAccessToEms.HasValue)
                yield return null;

            if (!AppearitionGate.HasInternetAccessToEms.Value)
            {
                TenantSettings_List offlineData = null;
                yield return HandleSimpleOfflineApiContentLoadingWithCallback<TenantSettings_List, TenantHandler>(success => offlineData = success, onFailure, onComplete);

                if (offlineData != null)
                {
                    AppearitionLogger.LogInfo(string.Format("Tenant settings for tenant of key {0} were loaded offline successfully.", AppearitionGate.Instance.CurrentUser.selectedTenant));
                    //Loaded!
                    if (onSuccess != null)
                        onSuccess(offlineData.Data);
                }
                else
                {
                    if (onFailure != null)
                        onFailure(new EmsError(AppearitionConstants.EMS_UNREACHABLE_NO_LOCAL_DATA_ERROR_MESSAGE));
                }

                if (onComplete != null)
                    onComplete(offlineData != null);
                yield break;

            }

            //Online request
            var getTenantSettingsRequest = AppearitionRequest<TenantSettings_List>.LaunchAPICall_GET(0, GetReusableApiRequest<TenantSettings_List>());

            while (!getTenantSettingsRequest.IsDone)
                yield return null;

            if (getTenantSettingsRequest.RequestResponseObject.IsSuccess)
            {
                AppearitionLogger.LogInfo(string.Format("Tenant settings for tenant of key {0} were successfully fetched", AppearitionGate.Instance.CurrentUser.selectedTenant));

                if (onSuccess != null)
                    onSuccess(getTenantSettingsRequest.RequestResponseObject.Data);
            }
            else
            {
                AppearitionLogger.LogError(string.Format("An error occured when trying to fetch the settings of tenant of key {0}", AppearitionGate.Instance.CurrentUser.selectedTenant));

                if (onFailure != null)
                    onFailure(new EmsError(getTenantSettingsRequest.RequestResponseObject.Errors));
            }

            if (onComplete != null)
                onComplete(getTenantSettingsRequest.RequestResponseObject.IsSuccess);
        }
    }
}