﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Appearition.ArTargetImageAndMedia;
using Appearition;

public class DisplayUserExperiences : MonoBehaviour
{
    public GameObject emptyState;

    public GameObject experienceCard;
    public GameObject contentParent;
    public GameObject fetchingMessage;

    List<GameObject> experiencesCache = new List<GameObject>();

    int expCount;
    public void OnDisable()
    {
        ClearExperiences();
    }

    public IEnumerator DispCoroutine()
    {
        expCount = 0;
        fetchingMessage.SetActive(true);
        yield return null;
        Debug.LogError("Fetching..." + fetchingMessage.activeSelf);
        DisplayFiltering dFilter = GetComponent<DisplayFiltering>();
        if (experiencesCache.Count > 0)
        {
            ClearExperiences();
        }


        if (GetComponent<MyProfileData>().userTargets.Count > 0)
        {
            emptyState.SetActive(false);
        }
        else
        {
            emptyState.SetActive(true);
        }

        DispCountLimitedExperiences(10);

        fetchingMessage.SetActive(false);
    }

    public void Disp()
    {
        StartCoroutine(DispCoroutine());
    }

    void DispCountLimitedExperiences(int count)
    {
        DisplayFiltering dFilter = GetComponent<DisplayFiltering>();
        List<ArTarget> userAssets = GetComponent<MyProfileData>().userTargets;
        while (count > 0)
        {
            if (expCount >= userAssets.Count)
            {
                break;
            }
            //if (dFilter.markerExp && userAssets[expCount].name.Contains("[MARKERLESS]"))
            //{
            //    continue;
            //}
            //if (dFilter.markerlessExp && !userAssets[expCount].name.Contains("[MARKERLESS]"))
            //{
            //    continue;
            //}
            
            count--;
            GameObject go = Instantiate(experienceCard, contentParent.transform);
            go.SetActive(true);

            go.GetComponent<UserExperienceCardSetup>().Setup(userAssets[expCount]);
            experiencesCache.Add(go);
            expCount++;
        }

    }

    public void ClearExperiences()
    {
        for (int i = experiencesCache.Count - 1; i >= 0; i--)
        {
            Destroy(experiencesCache[i]);
        }
        experiencesCache.Clear();
    }

    public void AddNextSet()
    {
        DispCountLimitedExperiences(10);
    }
}
