﻿using System;
using System.Collections;
using System.Collections.Generic;
using Appearition.Common;
using UnityEngine;

public class FilterTenantSettingsDataC
{
    /// <summary>
    /// All the filters which aren't channels. eg School.
    /// </summary>
    public List<string> tenantLevelFilters = new List<string>() { "School" };
    /// <summary>
    /// List of "filters" on a tenant level to be selected when the app starts. Channel names and tenant level filters only.
    /// </summary>
    public List<string> tenantLevelFirstSelectedTags = new List<string>() { "School" };
    /// <summary>
    /// First tags to select when selecting a channel. Not affected by tenant-level filters.
    /// </summary>
    public List<string> filterLevelFirstSelectedTags = new List<string>() { "Pinned" };
    /// <summary>
    /// Tags not to be included in the filter list, and to be included in the state list.
    /// </summary>
    public List<string> typeTags = new List<string>() { "Markerless", "Marker" };
    /// <summary>
    /// State tags, generally published or unpublished states. See ArTarget/List v2 doc for more info
    /// </summary>
    public List<string> stateTags = new List<string>() {"Unpublished", "Published"};

    /// <summary>
    /// Whether or not to allow the filtering of MARKER, MARKERLESS or both.
    /// </summary>
    public bool allowExperienceTypeFilter = false;

    public bool isSetup = false;
    
    public void UpdateData(List<Setting> tenantSettings)
    {
        for (int i = 0; i < tenantSettings.Count; i++)
        {
            switch (tenantSettings[i].key)
            {
                case "AllowExperienceTypeFilter":
                    if (bool.TryParse(tenantSettings[i].value, out bool tmp))
                        allowExperienceTypeFilter = tmp;
                    break;

                case "TenantLevelFiltersCsv":
                    tenantLevelFilters.Clear();
                    tenantLevelFilters.AddRange(CsvToList(tenantSettings[i].value));
                    break;

                case "TenantLevelFirstSelectedTagsCsv":
                    tenantLevelFirstSelectedTags.Clear();
                    tenantLevelFirstSelectedTags.AddRange(CsvToList(tenantSettings[i].value));
                    break;

                case "FilterLevelFirstSelectedTagsCsv":
                    filterLevelFirstSelectedTags.Clear();
                    filterLevelFirstSelectedTags.AddRange(CsvToList(tenantSettings[i].value));
                    break;

                case "TypeTagsCsv":
                    typeTags.Clear();
                    typeTags.AddRange(CsvToList(tenantSettings[i].value));
                    break;

                case "StateTagsCsv":
                    stateTags.Clear();
                    stateTags.AddRange(CsvToList(tenantSettings[i].value));
                    break;
            }
        }

        isSetup = true;
    }

    List<string> CsvToList(string csv)
    { 
        List<string> outcome = new List<string>();

        if (string.IsNullOrEmpty(csv))
            return outcome;
        
        string[] csvData = csv.Split(new []{','}, StringSplitOptions.RemoveEmptyEntries);

        for (int i = 0; i < csvData.Length; i++)
            outcome.Add(csvData[i].Trim());
        
        return outcome;
    }
}
