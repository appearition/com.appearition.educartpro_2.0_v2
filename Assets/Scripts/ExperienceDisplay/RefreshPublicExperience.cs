﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RefreshPublicExperience : MonoBehaviour
{
    public DisplayPublicExperiences dPubExp;
    Vector2 scrollViewPosition;
    public void OnScrollViewMoved(Vector2 value)
    {

        scrollViewPosition = value;
    }

    public void UserReleaseDrag()
    {

        if (scrollViewPosition.y > 1.02f)
        {
            dPubExp.RefreshData(true);
        }

        if(scrollViewPosition.y < 0f)
        {
            dPubExp.AddNextSet();
        }
    }
}
