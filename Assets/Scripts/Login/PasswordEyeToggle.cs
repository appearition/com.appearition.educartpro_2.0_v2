﻿using UnityEngine;
using UnityEngine.UI;

public class PasswordEyeToggle : MonoBehaviour
{
    public InputField passwordInput;

    public Toggle eyetoggleBtn;
    public GameObject showIcon;
    public GameObject hideicon;

    public void ToggleInputType()
    {
        if (this.passwordInput != null)
        {

            if (this.passwordInput.contentType == InputField.ContentType.Password)
            {
                this.passwordInput.contentType = InputField.ContentType.Standard;
                //eyetoggleBtn.image.sprite = showIcon;
                showIcon.SetActive(true);
                hideicon.SetActive(false);

            }
            else
            {
                this.passwordInput.contentType = InputField.ContentType.Password;
                //eyetoggleBtn.image.sprite = hideicon; 
                hideicon.SetActive(true);
                showIcon.SetActive(false);
            }

            this.passwordInput.ForceLabelUpdate();
        }
    }

}
