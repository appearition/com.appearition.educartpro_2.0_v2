﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using ICSharpCode.SharpZipLib.Zip;
using TriLib;
using UnityEngine;

namespace Appearition.Example
{
    public class ModelArMedia : MonoBehaviour, IArMedia
    {
        //ApiData
        /// <summary>
        /// Reference to the experience which this AssetBundle is part of.
        /// </summary>
        public AppearitionExperience AssociatedExperience { get; protected set; }

        /// <summary>
        /// Contains the ApiData of the current ArMedia.
        /// </summary>
        public MediaFile Data { get; protected set; }

        private bool? _hasLoadedModel;
        Coroutine _loadingDataProcess;

        public bool IsMediaReadyAndDownloaded => _hasLoadedModel.GetValueOrDefault();


        public virtual void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            //Store media
            AssociatedExperience = associatedExperience;
            Data = media;

            if (!_hasLoadedModel.HasValue)
            {
                _hasLoadedModel = false;

                //Fetch it from the files if available.
                ArTargetHandler.LoadMediaFileContent(associatedExperience.Data, media, OnModelDataLoaded);
            }
        }

        public virtual void ChangeDisplayState(bool state)
        {
            gameObject.SetActive(state);
        }

        /// <summary>
        /// Whether or not the current Media is visible.
        /// </summary>
        public bool IsVisible
        {
            get { return gameObject.activeInHierarchy; }
        }

        public virtual void OnExperienceTrackingStateChanged(bool isTracking)
        {
            //Check if the assetbundle has been fetched already.
            if (isTracking && !_hasLoadedModel.HasValue)
            {
                _hasLoadedModel = false;

                //Fetch it from the files if available. Do the download process manually.
                string fullPath = ArTargetHandler.GetPathToMedia(AssociatedExperience.Data, Data);

                if (!string.IsNullOrEmpty(Data.checksum) && ArTargetHandler.IsThereExistingMediaWithMatchingChecksum(fullPath, Data.checksum))
                {
                    //Same as EMS file. Go straight to loading it.
                    OnModelDataLoaded(null);
                }
                else
                    //Re-download the zip.
                    ArTargetHandler.LoadMediaFileContent(AssociatedExperience.Data, Data, OnModelDataLoaded);
            }

            //Finally enable/disable object
            ChangeDisplayState(isTracking);
        }

        /// <summary>
        /// Once downloaded, finds the content inside the assetbundle to spawn, if any.
        /// </summary>
        /// <param name="obj"></param>
        private void OnModelDataLoaded(Dictionary<string, byte[]> obj)
        {
            if (_loadingDataProcess != null)
                return;

            //Dump the coroutine on the gate, since it's always active.
            _loadingDataProcess = AppearitionGate.Instance.StartCoroutine(ModelDataLoadingProcess(obj));
        }

        IEnumerator ModelDataLoadingProcess(Dictionary<string, byte[]> downloadedFiles)
        {
            //Create extracted path based on zip path
            GameObject content = null;
            string mediaPath = ArTargetHandler.GetPathToMedia(AssociatedExperience.Data, Data);
            string extractPath = mediaPath.Substring(0, mediaPath.Length - 4); //Same but without ".zip".
            byte[] obj = null;

            if (downloadedFiles != null && downloadedFiles.Count > 0)
                obj = downloadedFiles.First().Value;

            if (Path.GetExtension(mediaPath) == ".zip")
            {
                //If the extract path exists, then the object might already have been extracted.
                //Try loading, and if fail, then unzip again.
                if (obj == null && Directory.Exists(extractPath))
                {
                    try
                    {
                        content = LoadZipContent(extractPath);
                    } catch
                    {
                    }
                }

                if (content == null)
                {
                    //Load the zip file
                    if (obj == null)
                        obj = File.ReadAllBytes(mediaPath);

                    yield return ExtractZipFile(obj, extractPath);
                    content = LoadZipContent(extractPath);
                }
            }
            else if (Path.GetExtension(mediaPath) == ".obj" || Path.GetExtension(mediaPath) == ".fbx")
            {
                content = LoadModelContent(mediaPath);
            }
            else if (Path.GetExtension(mediaPath) == ".mtl")
            {
                //If a MTL was found, try to find an OBJ at the same path
                string folderPath = Path.GetDirectoryName(mediaPath);

                if (!string.IsNullOrEmpty(folderPath))
                {
                    string objFile = Directory.GetFiles(folderPath, "*.obj").FirstOrDefault();

                    if (!string.IsNullOrEmpty(objFile))
                        content = LoadModelContent(objFile);
                }
            }
            else
            {
                Debug.LogError("Content not recognized. " + mediaPath);
                _hasLoadedModel = true;
                _loadingDataProcess = null;
                yield break;
            }

            //If the experience is already gone, it's too late. Delete it.
            if (AssociatedExperience == null)
            {
                Destroy(content.gameObject);
                yield break;
            }

            try
            {
                //Load the content
                content.transform.SetParent(transform);
                content.transform.localPosition = Data.GetPosition;
                content.transform.localRotation = Data.GetRotation;
                content.transform.localScale = Data.GetScale;
                //transform.localPosition = Data.GetPosition;
                //transform.localRotation = Data.GetRotation;
                //transform.localScale = Data.GetScale;
                //content.transform.localPosition = Vector3.zero;
                //content.transform.localRotation = Quaternion.identity;
                //content.transform.localScale = Vector3.one;

                //Also, create a directional light.
                if (content.GetComponentInChildren<Light>() == null)
                {
                    GameObject tmp = new GameObject("Model Light");
                    Light tmpLight = tmp.AddComponent<Light>();
                    tmpLight.type = LightType.Directional;
                    tmpLight.intensity = 1.0f;
                    tmpLight.transform.localRotation = Quaternion.Euler(30, 0, 0);

                    tmp.transform.SetParent(transform);
                }

                Debug.Log(content.name);
                Debug.Log(string.Format("Asset downloaded ! Name: {0}, Mediatype: {1}", Data.fileName, Data.mediaType));

                _hasLoadedModel = true;
                _loadingDataProcess = null;
            }
            //If the object is already gone and this callback came back, an error will be caused.
            catch (MissingReferenceException)
            {
            }
        }

        #region Loading 

        IEnumerator ExtractZipFile(byte[] zipFileData, string targetDirectory, int bufferSize = 256 * 1024)
        {
            if (Directory.Exists(targetDirectory))
                Directory.Delete(targetDirectory, true);

            Directory.CreateDirectory(targetDirectory);

            string filePath = null;
            using (MemoryStream fileStream = new MemoryStream())
            {
                fileStream.Write(zipFileData, 0, zipFileData.Length);
                fileStream.Flush();
                fileStream.Seek(0, SeekOrigin.Begin);

                ZipFile zipFile = new ZipFile(fileStream);

                foreach (ZipEntry entry in zipFile)
                {
                    string targetFile = Path.Combine(targetDirectory, entry.Name);
                    //if (entry.Name.Contains(".obj"))
                    //{
                    //    filePath = targetFile;
                    //}
                    using (FileStream outputFile = File.Create(targetFile))
                    {
                        if (entry.Size > 0)
                        {
                            Stream zippedStream = zipFile.GetInputStream(entry);
                            byte[] dataBuffer = new byte[bufferSize];

                            int readBytes;
                            while ((readBytes = zippedStream.Read(dataBuffer, 0, bufferSize)) > 0)
                            {
                                outputFile.Write(dataBuffer, 0, readBytes);
                                outputFile.Flush();
                                yield return null;
                            }
                        }
                    }
                }
            }
        }

        GameObject LoadZipContent(string extractedZipPath)
        {
            //Look for the OBJ file
            string[] allFiles = Directory.GetFiles(extractedZipPath);
            string objPath = "";
            //        string texturePath = "";

            for (int i = 0; i < allFiles.Length; i++)
            {
                if (Path.GetExtension(allFiles[i]) == ".obj")
                    objPath = allFiles[i];
            }

            return LoadModelContent(objPath);
        }

        GameObject LoadModelContent(string modelPath)
        {
            //No obj, no goodies.
            if (string.IsNullOrEmpty(modelPath))
            {
                Debug.LogError("No OBJ found.");
                return null;
            }

            GameObject loaded3dObject = null;

            using (var assetLoader = new AssetLoader())
            {
                var assetLoaderOptions = AssetLoaderOptions.CreateInstance(); //Creates the AssetLoaderOptions instance.
                //AssetLoaderOptions let you specify options to load your model.
                //(Optional) You can skip this object creation and it's parameter or pass null.

                //You can modify assetLoaderOptions before passing it to LoadFromFile method. You can check the AssetLoaderOptions API reference at:
                //https://ricardoreis.net/trilib/manual/html/class_tri_lib_1_1_asset_loader_options.html

                loaded3dObject = assetLoader.LoadFromFile(modelPath, assetLoaderOptions, null); //Loads the model synchronously and stores the reference in myGameObject.
            }

            if (loaded3dObject == null)
            {
                Debug.LogError($"Error occured when trying to load the 3DModel at path {modelPath}.");
                return null;
            }

            Transform[] allChildren = loaded3dObject.GetComponentsInChildren<Transform>();
            foreach (Transform go in allChildren)
            {
                if (go.GetComponent<MeshFilter>() != null)
                {
                    go.gameObject.GetComponent<MeshRenderer>().material.shader = Shader.Find("Diffuse"); //Also add Material
                }
            }

            return loaded3dObject;
        }

        #endregion

        void OnDestroy()
        {
            _loadingDataProcess = null;
        }
    }
}