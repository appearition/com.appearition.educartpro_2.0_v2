﻿namespace Appearition
{
    public static partial class UserRoleConstants
    {
        public const string ADMIN = "Admin";
        public const string USER = "User";
        public const string USER_ACTIVITY_LOGGER = "UserActivityLogger";

        public static bool IsAdmin => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(ADMIN);
        public static bool IsUser => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(USER);
    }
}