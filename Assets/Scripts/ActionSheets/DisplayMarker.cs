﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using System.IO;

public class DisplayMarker : MonoBehaviour
{
    [SerializeField] CanvasScaler _landscapeSizeRef;
    [SerializeField] Image _portraitImage;
    [SerializeField] Image _landscapeImage;

    public ActionSheetData data;
    public UserActionSheetData Ardata;
    public GameObject fullscreenMarkerViewer;
    //Storage
    [SerializeField] Sprite _placeholder;
    //Asset _data;

    public void DisplayTargetImage()
    {
        
        //Reset Data
        //_data = newData;
        _landscapeImage.rectTransform.sizeDelta = new Vector2(_landscapeSizeRef.referenceResolution.y, _landscapeSizeRef.referenceResolution.x);

        gameObject.SetActive(true);
        
        //Setup visuals
        if (data != null)
        {
            if (data.asset.targetImages?.Length > 0 && data.asset.targetImages[0] != null)
            {
                if (data.asset.targetImages[0].image != null)
                {
                    ApplyImage(data.asset.targetImages[0].image);
                    fullscreenMarkerViewer.SetActive(true);
                }

                else
                {
                    ApplyImage(_placeholder);
                    StartCoroutine(FetchTargetImage(data.asset));
                }
            }
            else
                ApplyImage(_placeholder);
        }
        else
        {
            if (Ardata.asset.targetImages?.Length > 0 && Ardata.asset.targetImages[0] != null)
            {
                if (Ardata.asset.targetImages[0].image != null)
                {
                    ApplyImage(Ardata.asset.targetImages[0].image);
                    fullscreenMarkerViewer.SetActive(true);
                }

                else
                {
                    ApplyImage(_placeholder);
                    StartCoroutine(FetchTargetImage(Ardata.asset));
                }
            }
            else
                ApplyImage(_placeholder);

        }

    }

    #region Handle Target Image 

    IEnumerator FetchTargetImage(Asset asset)
    {
        //Assume the targetimage has been downloaded already.
        string targetImagePath = ArTargetHandler.GetPathToTargetImage(asset, asset.targetImages[0]);

        if (!File.Exists(targetImagePath))
        {
            yield return ArTargetHandler.DownloadAssetsContent(new Asset[] {asset }, true, false, false);
            targetImagePath = ArTargetHandler.GetPathToTargetImage(asset, asset.targetImages[0]);
        }

        Sprite tmpTex = null;

        yield return ImageUtility.LoadOrCreateSpriteAsync(targetImagePath, asset.targetImages[0].checksum, sprite => tmpTex = sprite);

        asset.targetImages[0].image = tmpTex;

        ApplyImage(asset.targetImages[0].image != null ? asset.targetImages[0].image : null);
        
    }

    void ApplyImage(Texture2D image)
    {
        ApplyImage(image == null ? null : ImageUtility.LoadOrCreateSprite((Texture2D)image));
    }

    void ApplyImage(Sprite image)
    {
        if (image != null)
        {
            if (image.texture.width > image.texture.height)
            {
                _landscapeImage.sprite = image;
                _landscapeImage.gameObject.SetActive(true);
                _portraitImage.sprite = _placeholder;
                _portraitImage.gameObject.SetActive(false);
            }
            else
            {
                _portraitImage.sprite = image;
                _portraitImage.gameObject.SetActive(true);
                _landscapeImage.sprite = _placeholder;
                _landscapeImage.gameObject.SetActive(false);
            }
        }
    }

    #endregion

    void OnDisable()
    {
        _portraitImage.sprite = _placeholder;
        _landscapeImage.sprite = _placeholder;
        _portraitImage.gameObject.SetActive(false);
        _landscapeImage.gameObject.SetActive(false);
    }
}

