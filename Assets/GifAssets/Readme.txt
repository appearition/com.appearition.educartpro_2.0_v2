Hi! There are 3 different assets inside: GIF Replay, Power GIF, Unity Native Share. You will find a manual for each asset in its' folder.

If you want to share replays then you need to add the following line to your AndroidManifest.xml:
	<provider android:name="com.yasirkula.unity.UnitySSContentProvider" android:authorities="UnityNativeShare" android:exported="false" android:grantUriPermissions="true" />

If you have no AndroidManifest.xml yet, just copy "Assets\GifAssets\NativeShare\Plugins\Android\AndroidManifest.xml" to "Assets\Plugins\Android\AndroidManifest.xml" (example manifest will not be applied as it's located not in "Assets\Plugins" folder).