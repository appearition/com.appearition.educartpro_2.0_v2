﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;
using UnityEngine;
using UnityEngine.Networking;

namespace Appearition.Example
{
    public class AudioArMedia : MonoBehaviour, IArMedia
    {
        //ApiData
        /// <summary>
        /// Reference to the experience which this AssetBundle is part of.
        /// </summary>
        public AppearitionExperience AssociatedExperience { get; protected set; }

        /// <summary>
        /// Contains the ApiData of the current ArMedia.
        /// </summary>
        public MediaFile Data { get; protected set; }

        AudioSource _mediaAs;

        //Internal Variables
        Coroutine _loadAudioCoroutine;
        bool? _isAudioCreated = null;

        public bool IsMediaReadyAndDownloaded => _isAudioCreated.GetValueOrDefault();


        public bool IsVisible
        {
            get { return gameObject.activeInHierarchy; }
        }

        public void ChangeDisplayState(bool state)
        {
            //NEVER GOING AWAY, MUAAAAHAHAHAHAHHAHAHAHAHAHAHA
            gameObject.SetActive(true);
        }

        public void OnExperienceTrackingStateChanged(bool isTracking)
        {
            ChangeDisplayState(isTracking);
        }

        public void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            //Store media
            AssociatedExperience = associatedExperience;
            Data = media;

            if (FindObjectOfType<AudioListener>() == null)
                FindObjectOfType<Camera>().gameObject.AddComponent<AudioListener>();
            
            //Download the track
            ArTargetHandler.LoadMediaFileContent(AssociatedExperience.Data, Data, OnAudioDownloaded);
        }

        void OnAudioDownloaded(Dictionary<string, byte[]> audio)
        {
            //If enabled, then load it asap
            if (gameObject.activeInHierarchy && !_isAudioCreated.HasValue)
                LoadAudio();
        }

        void OnEnable()
        {
            if (!_isAudioCreated.HasValue && GetComponent<AudioSource>() == null)
                LoadAudio();
        }

        void LoadAudio()
        {
            if (_loadAudioCoroutine != null)
                return;

            _loadAudioCoroutine = AssociatedExperience.StartCoroutine(AudioLoadingProcess());
        }

        IEnumerator AudioLoadingProcess()
        {
            _isAudioCreated = false;
            string fileLocation = ArTargetHandler.GetPathToMedia(AssociatedExperience.Data, Data);

            if (!File.Exists(fileLocation))
                yield return ArTargetHandler.LoadMediaFileContentProcess(AssociatedExperience.Data, Data);

            AudioType audioType = AudioType.UNKNOWN;

            switch (Path.GetExtension(Data.fileName))
            {
                case ".ogg":
                    audioType = AudioType.OGGVORBIS;
                    break;
                case ".wav":
                    audioType = AudioType.WAV;
                    break;
                default:
                    Debug.LogError("Audio format not implemented.");
                    break;
            }

            string fullLocation = fileLocation;
            
            if(Application.platform == RuntimePlatform.Android)
                fullLocation = string.Format("file:///{0}", fullLocation);
            else
                fullLocation = string.Format("file://{0}", fullLocation);


            var loadRequest = UnityWebRequestMultimedia.GetAudioClip(fullLocation, audioType);

            yield return loadRequest.SendWebRequest();

            AudioClip tmp = DownloadHandlerAudioClip.GetContent(loadRequest);

            if (tmp == null)
                Debug.LogError($"An error occured when trying to load the Audioclip of name {Data.fileName}.");
            else
            {
                _mediaAs = gameObject.AddComponent<AudioSource>();
                _mediaAs.clip = tmp;
                _mediaAs.volume = 1.0f;
                _mediaAs.loop = true;
                _mediaAs.Play();
            }

            _isAudioCreated = true;
            _loadAudioCoroutine = null;
        }
    }
}