﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: "BillboardTransform.cs" 
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using UnityEngine;

namespace Appearition.Example.MarkerlessLearnDemo.Utilities
{
    public class BillboardTransform : MonoBehaviour
    {
        public bool invertZ = true;
        public bool onlyTargetCameraDirection = false;

        void LateUpdate()
        {
            if (Camera.main != null)
            {
                if (onlyTargetCameraDirection)
                    transform.rotation = Quaternion.LookRotation(Camera.main.transform.forward * (invertZ ? 1 : -1), Vector3.up);
                else
                    transform.LookAt(invertZ ? transform.position - Camera.main.transform.position : Camera.main.transform.position - transform.position);
            }
        }
    }
}