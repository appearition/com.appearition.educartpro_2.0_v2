﻿using Appearition;
using UnityEngine;
using UnityEngine.UI;

public class DisplayUserInfo : MonoBehaviour
{
    [SerializeField] InputField _firstNameInputField;
    //[SerializeField] InputField _lastNameInputField;

    public void OnEnable()
    {
        if (_firstNameInputField != null)
            _firstNameInputField.text = AppearitionGate.Instance.CurrentUser.firstName + " " + AppearitionGate.Instance.CurrentUser.lastName;

    }


}
