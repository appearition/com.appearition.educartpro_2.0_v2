﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class CreateSetup : MonoBehaviour
{
    public CreateData createData;

    public InputField expName;

    public GameObject markerObject;

    public GameObject mediaObject;

    public GameObject mediaParent;

    public Toggle isPublicToggle;

    public Toggle isPrivateToggle;

    public Toggle isMarkerToggle;

    public Toggle isMarkerlessToggle;

    public List<GameObject> cache;

    GameObject markerInstance;

    private void OnEnable()
    {
        for (int i = cache.Count - 1; i >= 0; i--)
        {
            Destroy(cache[i].gameObject);
        }
        cache.Clear();
        createData = GetComponent<CreateData>();
        createData.markerToUpload = new MarkerToUpload();
        createData.mediaToUpload = new MediaToUpload();
        createData.isMarker = true;
        //createData.isPublic = true;
        createData.expName = "";
        expName.text = "";
        SetupMarker();
        SetupMedia();
    }

    public void SetupMarker()
    {
        markerInstance = Instantiate(markerObject, mediaParent.transform);
        markerInstance.SetActive(true);
        markerInstance.transform.SetSiblingIndex(3);
        cache.Add(markerInstance);
    }

    public void AddMarker(bool value)
    {
        if (value)
        {
            markerInstance.SetActive(true);
        }
    }

    public void SetupMedia()
    {
        GameObject go = Instantiate(mediaObject, mediaParent.transform);
        go.SetActive(true);
        cache.Add(go);
    }

    public void AddMedia()
    {
        SetupMedia();
    }

    public void DeleteMarker(bool value)
    {
        if (value)
        {
            Debug.Log("DELETED");
            markerInstance.SetActive(false);
        }
    }
}
