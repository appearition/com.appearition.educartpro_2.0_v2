﻿namespace Appearition
{
    public static partial class UserRoleConstants
    {
        public const string TENANT_SETTINGS_VIEWER = "TenantSettingsViewer";

        //Handy Properties
        public static bool HasPermissionToViewTenantSettings => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(TENANT_SETTINGS_VIEWER);
    }
}