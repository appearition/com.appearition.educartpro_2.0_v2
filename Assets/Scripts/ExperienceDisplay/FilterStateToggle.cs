﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FilterStateToggle : MonoBehaviour
{
    public enum FilterType
    { 
        Misc,
        Class,
        Tag,
        State
    }
    
    //References
    public GameObject onState;
    public Text onText;
    public GameObject offSTate;
    public Text offText;
    RectTransform _rectTransform;
    Toggle _toggle; 

    //Data
    Action<string, bool, FilterType> _onValueChangedCallback;
    FilterType _filterType;


    //Handy Properties
    public bool FilterState => onState.activeSelf;

    public string GetName => onText?.text;

    public float GetTextWidth => _rectTransform.sizeDelta.x;//onText.gameObject.activeInHierarchy ? onText.rectTransform.rect.width + 40 : offText.rectTransform.rect.width + 40;
    
    public void Setup(string newName, bool state, FilterType filterType, Action<string, bool, FilterType> onValueChanged)
    {
        if (onText != null)
            onText.text = newName;
        if (offText != null)
            offText.text = newName;

        _rectTransform = transform as RectTransform;
        Resize();
        _toggle = GetComponent<Toggle>();

        ChangeValueWithoutEvent(state);
        _onValueChangedCallback = onValueChanged;
        _filterType = filterType;
    }

    void Resize()
    {
        if (_rectTransform != null && onText != null)
        {
            TextGenerator textGen = new TextGenerator();
            TextGenerationSettings textGenSettings = onText.GetGenerationSettings(onText.rectTransform.rect.size);
            float textWidth = textGen.GetPreferredWidth(onText.text, textGenSettings);

            //Add 3 to make sure the text fits, in case it is set to truncate.
            _rectTransform.sizeDelta = new Vector2(textWidth + 40 + 3, _rectTransform.sizeDelta.y);
        }
    }
    
    public void ChangeValueWithoutEvent(bool newValue)
    {
        onState.SetActive(newValue);
        offSTate.SetActive(!newValue);
        _toggle?.SetIsOnWithoutNotify(newValue);
    }

    public void OnStateChange(bool value)
    {
        onState.SetActive(value);
        offSTate.SetActive(!value);

        //Event up
        _onValueChangedCallback?.Invoke(GetName, value, _filterType);
    }
}