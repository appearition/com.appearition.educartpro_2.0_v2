﻿// // -----------------------------------------------------------------------
// // Company:"Appearition Pty Ltd"
// // File: LearnHandler.cs
// // Copyright (c) 2019. All rights reserved.
// // -----------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using Appearition.Common;
using UnityEngine;
using System;
using Appearition.Learn.API;

namespace Appearition.Learn
{
    /// <summary>
    /// Handler in charge of taking care of the Learn module as it is on the EMS.
    /// Provides the node tree as well as tracking functionality.
    /// </summary>
    public class LearnHandler : BaseHandler
    {
        /// <summary>
        /// Fetches the learn module's content.
        ///
        /// Offline Capability.
        /// </summary>
        /// <param name="onSuccess">Contains the Learn Data as on the EMS, as a hierarchy node tree. Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static void GetLearnContent(Action<List<LearnNode>> onSuccess = null, Action<EmsError> onFailure = null, Action<bool> onComplete = null)
        {
            GetLearnContent(AppearitionGate.Instance.CurrentUser.selectedChannel, new List<LearnNode>(), onSuccess, onFailure, onComplete);
        }

        /// <summary>
        /// Fetches the learn module's content.
        ///
        /// Offline Capability.
        /// </summary>
        /// <param name="onSuccess">Contains the Learn Data as on the EMS, as a hierarchy node tree. Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static IEnumerator GetLearnContentProcess(Action<List<LearnNode>> onSuccess = null, Action<EmsError> onFailure = null, Action<bool> onComplete = null)
        {
            yield return GetLearnContentProcess(AppearitionGate.Instance.CurrentUser.selectedChannel, new List<LearnNode>(), onSuccess, onFailure, onComplete);
        }

        /// <summary>
        /// Fetches the learn module's content.
        /// Optionally, can provide a channel Id.
        ///
        /// Offline Capability.
        /// </summary>
        /// <param name="channelId">The id of the targeted channel.</param>
        /// <param name="onSuccess">Contains the Learn Data as on the EMS, as a hierarchy node tree. Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static void GetLearnContent(int channelId, Action<List<LearnNode>> onSuccess = null, Action<EmsError> onFailure = null, Action<bool> onComplete = null)
        {
            GetLearnContent(channelId, new List<LearnNode>(), onSuccess, onFailure, onComplete);
        }

        /// <summary>
        /// Fetches the learn module's content.
        /// Optionally, can provide a channel Id.
        ///
        /// Offline Capability.
        /// </summary>
        /// <param name="channelId">The id of the targeted channel.</param>
        /// <param name="onSuccess">Contains the Learn Data as on the EMS, as a hierarchy node tree. Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static IEnumerator GetLearnContentProcess(int channelId, Action<List<LearnNode>> onSuccess = null, Action<EmsError> onFailure = null, Action<bool> onComplete = null)
        {
            yield return GetLearnContentProcess(channelId, new List<LearnNode>(), onSuccess, onFailure, onComplete);
        }

        /// <summary>
        /// Fetches the learn module's content.
        /// Optionally, can provide a reusable list.
        ///
        /// Offline Capability.
        /// </summary>
        /// <param name="reusableList">Reusable list of learn nodes.</param>
        /// <param name="onSuccess">Contains the Learn Data as on the EMS, as a hierarchy node tree. Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static void GetLearnContent(List<LearnNode> reusableList, Action<List<LearnNode>> onSuccess = null, Action<EmsError> onFailure = null, Action<bool> onComplete = null)
        {
            GetLearnContent(AppearitionGate.Instance.CurrentUser.selectedChannel, reusableList, onSuccess, onFailure, onComplete);
        }

        /// <summary>
        /// Fetches the learn module's content.
        /// Optionally, can provide a reusable list.
        ///
        /// Offline Capability.
        /// </summary>
        /// <param name="reusableList">Reusable list of learn nodes.</param>
        /// <param name="onSuccess">Contains the Learn Data as on the EMS, as a hierarchy node tree. Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static IEnumerator GetLearnContentProcess(List<LearnNode> reusableList, Action<List<LearnNode>> onSuccess = null, Action<EmsError> onFailure = null, Action<bool> onComplete = null)
        {
            yield return GetLearnContentProcess(AppearitionGate.Instance.CurrentUser.selectedChannel, reusableList, onSuccess, onFailure, onComplete);
        }

        /// <summary>
        /// Fetches the learn module's content.
        /// Optionally, can provide a reusable list.
        ///
        /// Offline Capability.
        /// </summary>
        /// <param name="channelId">The id of the targeted channel.</param>
        /// <param name="reusableList">Reusable list of learn nodes.</param>
        /// <param name="onSuccess">Contains the Learn Data as on the EMS, as a hierarchy node tree. Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static void GetLearnContent(int channelId, List<LearnNode> reusableList, Action<List<LearnNode>> onSuccess = null, Action<EmsError> onFailure = null, Action<bool> onComplete = null)
        {
            AppearitionGate.Instance.StartCoroutine(GetLearnContentProcess(channelId, reusableList, onSuccess, onFailure, onComplete));
        }

        /// <summary>
        /// Fetches the learn module's content.
        /// Optionally, can provide a reusable list.
        ///
        /// Offline Capability.
        /// </summary>
        /// <param name="channelId">The id of the targeted channel.</param>
        /// <param name="reusableList">Reusable list of learn nodes.</param>
        /// <param name="onSuccess">Contains the Learn Data as on the EMS, as a hierarchy node tree. Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static IEnumerator GetLearnContentProcess(int channelId, List<LearnNode> reusableList, Action<List<LearnNode>> onSuccess = null, Action<EmsError> onFailure = null,
            Action<bool> onComplete = null)
        {
            //Wait for internet check, or handle offline.
            while (!AppearitionGate.HasInternetAccessToEms.HasValue)
                yield return null;

            Learn_List reusableRequest = GetReusableApiRequest<Learn_List>();

            //Prepare the reusable list
            if (reusableList == null)
                reusableList = new List<LearnNode>();
            else
                reusableList.Clear();

            if (!AppearitionGate.HasInternetAccessToEms.Value)
            {
                //This process is offline-friendly. Handle offline if no internet.
                Learn_List offlineRequest = null;

                HandleSimpleOfflineApiContentLoadingWithCallback<Learn_List, LearnHandler>(
                    offlineDataLoaded => offlineRequest = offlineDataLoaded, onFailure, onComplete);

                if (offlineRequest != null)
                {
                    reusableList.AddRange(offlineRequest.Data);

                    AppearitionLogger.LogInfo(string.Format("Learn data of the channel {0} have been successfully fetched!", channelId));
                    if (onSuccess != null)
                        onSuccess(reusableList);
                }

                yield break;
            }

            //Launch request
            var learnListRequest =
                AppearitionRequest<Learn_List>.LaunchAPICall_GET(channelId, reusableRequest, null, obj => { reusableRequest = obj; });

            //Wait for request..
            while (!learnListRequest.IsDone)
                yield return null;

            //All done!
            if (reusableRequest != null && reusableRequest.IsSuccess)
            {
                //Save data
                SaveJsonData<Learn_List, LearnHandler>(learnListRequest);

                if (reusableRequest.Data != null && reusableRequest.Data.Count > 0)
                    reusableList.AddRange(reusableRequest.Data);

                AppearitionLogger.LogInfo(string.Format("Learn data of the channel {0} have been successfully fetched!", channelId));
                //Callback it out ~
                if (onSuccess != null)
                    onSuccess(reusableList);
            }
            else
            {
                AppearitionLogger.LogError(string.Format("An error occured when trying to fetch the learn data from the channel of id {0}", channelId));

                //Request failed =(
                if (onFailure != null)
                    onFailure(learnListRequest.Errors);
            }

            if (onComplete != null)
                onComplete(reusableRequest != null && reusableRequest.IsSuccess);
        }

        /// <summary>
        /// Submits a full learn session.
        /// </summary>
        /// <param name="session">The learn session container executed by the user. </param>
        /// <param name="onSuccess">Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static void SubmitLearnTracking(LearningSession session, Action onSuccess, Action<EmsError> onFailure, Action<bool> onComplete)
        {
            SubmitLearnTracking(AppearitionGate.Instance.CurrentUser.selectedChannel, session, onSuccess, onFailure, onComplete);
        }

        /// <summary>
        /// Submits a full learn session.
        /// </summary>
        /// <param name="session">The learn session container executed by the user. </param>
        /// <param name="onSuccess">Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static IEnumerator SubmitLearnTrackingProcess(LearningSession session, Action onSuccess, Action<EmsError> onFailure, Action<bool> onComplete)
        {
            yield return SubmitLearnTrackingProcess(AppearitionGate.Instance.CurrentUser.selectedChannel, session, onSuccess, onFailure, onComplete);
        }

        /// <summary>
        /// Submits a full learn session.
        /// Optionally, can provide a channel Id.
        /// </summary>
        /// <param name="channelId">The id of the targeted channel.</param>
        /// <param name="session">The learn session container executed by the user. </param>
        /// <param name="onSuccess">Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static void SubmitLearnTracking(int channelId, LearningSession session, Action onSuccess, Action<EmsError> onFailure, Action<bool> onComplete)
        {
            AppearitionGate.Instance.StartCoroutine(SubmitLearnTrackingProcess(channelId, session, onSuccess, onFailure, onComplete));
        }

        /// <summary>
        /// Submits a full learn session.
        /// Optionally, can provide a channel Id.
        /// </summary>
        /// <param name="channelId">The id of the targeted channel.</param>
        /// <param name="session">The learn session container executed by the user. </param>
        /// <param name="onSuccess">Only called if the request is successful.</param>
        /// <param name="onFailure">Contains any error obtained during the request. Only called if the request has failed.</param>
        /// <param name="onComplete">Always called at the end of the request, defines whether the request was successful or not.</param>
        public static IEnumerator SubmitLearnTrackingProcess(int channelId, LearningSession session, Action onSuccess, Action<EmsError> onFailure, Action<bool> onComplete)
        {
            //Wait for internet check, or handle offline.
            bool hasInternetAccess = false;
            yield return HandleSimpleOnlineOnlyApiInternetCheck(hasInternet => hasInternetAccess = hasInternet, onFailure, onComplete);

            if (!hasInternetAccess)
                yield break;

            //Online request
            var submitTrackingRequest = AppearitionRequest<Learn_Tracking>.LaunchAPICall_POST
                (channelId, GetReusableApiRequest<Learn_Tracking>(), session);

            while (!submitTrackingRequest.IsDone)
                yield return null;

            if (submitTrackingRequest.RequestResponseObject.IsSuccess)
            {
                AppearitionLogger.LogInfo("Learn tracking submitted successfully!");
                if (onSuccess != null)
                    onSuccess();
            }
            else
            {
                AppearitionLogger.LogError("An error happened when trying to upload the learning session.");
                if (onFailure != null)
                    onFailure(submitTrackingRequest.Errors);
            }

            if (onComplete != null)
                onComplete(submitTrackingRequest.RequestResponseObject.IsSuccess);
        }
    }
}