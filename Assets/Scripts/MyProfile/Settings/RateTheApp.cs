﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RateTheApp : MonoBehaviour
{
    public string androidUrl;
    public string iosUrl;
    public void OnRateTheAppPresses()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            Application.OpenURL(androidUrl);
        }
        else
        {
            Application.OpenURL(iosUrl);
        }

    }



}
