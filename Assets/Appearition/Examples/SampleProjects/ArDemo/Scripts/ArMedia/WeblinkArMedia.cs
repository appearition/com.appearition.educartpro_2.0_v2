﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition.Common;

namespace Appearition.Example
{
    public class WeblinkArMedia : MonoBehaviour, IArMedia
    {
        public MediaFile Data { get; private set; }

        public bool IsVisible { get { return gameObject.activeInHierarchy; } }
        public bool IsMediaReadyAndDownloaded { get; protected set; }

        public void ChangeDisplayState(bool state)
        {
            gameObject.SetActive(state);
        }

        public void OnExperienceTrackingStateChanged(bool isTracking)
        {
            ChangeDisplayState(isTracking);
        }

        public void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            if (media == null)
                return;

            Data = media;

            IsMediaReadyAndDownloaded = true;

            Invoke("Cleanse", 1.5f);
            Application.OpenURL(media.url);

        }

        void Cleanse()
        {
            Debug.LogError("THE CLEANSING IS HAPPENING");
            AppearitionArHandler.Instance.ClearCurrentTargetBeingTracked(true);
        }
        
    }
}