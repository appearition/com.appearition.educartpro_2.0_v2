﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition.ArTargetImageAndMedia;

public class MyProfileData : MonoBehaviour
{

    public string UserName;

    public int numberOfExperiences;

    public List<ArTarget> userTargets = new List<ArTarget>();

}
