﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Appearition.Common;
using UnityEngine.UI;
using Appearition.ArTargetImageAndMedia;


public class DownloadMarker : MonoBehaviour
{
    //References
    [SerializeField] GameObject _saveMessageContainer;
    [SerializeField] Text _saveMessage;
    public ActionSheetData data;
    public UserActionSheetData arData;
    //Internal Variables
    Coroutine _saveTargetImageProcess;

    public void OnSaveTargetImageButtonPressed()
    {

        if (_saveTargetImageProcess != null)
            return;
        if(data != null)
        {
            _saveTargetImageProcess = StartCoroutine(OnSaveTargetImageButtonPressedProcess(data.asset));
        }
        else
        {
            _saveTargetImageProcess = StartCoroutine(OnSaveTargetImageButtonPressedProcess(arData.asset));
        }
    }

    IEnumerator OnSaveTargetImageButtonPressedProcess(Asset experience)
    {
        //Find TargetImage path
        string targetImagePath = ArTargetHandler.GetPathToTargetImage(experience,
            experience.targetImages[0]);

        if (!File.Exists(targetImagePath))
            yield return ArTargetHandler.DownloadAssetsContent(new[] { experience }, true, false, false);

        Texture2D tex = ImageUtility.LoadOrCreateTexture(targetImagePath);
        NativeGallery.SaveImageToGallery(tex, "TargetMarker", experience.targetImages[0].fileName, error => Debug.LogError(error));

#if PLATFORM_ANDROID
        if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.ExternalStorageWrite))
        {
            UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.ExternalStorageWrite);
        }

        if (!UnityEngine.Android.Permission.HasUserAuthorizedPermission(UnityEngine.Android.Permission.ExternalStorageRead))
        {
            UnityEngine.Android.Permission.RequestUserPermission(UnityEngine.Android.Permission.ExternalStorageRead);
        }
#endif
//        bool? hasError = default;
        //Media.Gallery.SaveImage(tex, experience.targetImages[0].fileName, experience.targetImages[0].fileName.Contains(".jpg") ? ImageFormat.JPG : ImageFormat.PNG,
        //    error =>
        //    {
        //        hasError = !string.IsNullOrEmpty(error);
        //        if (hasError.Value)
        //        {
        //            if (_saveMessageContainer != null)
        //                _saveMessageContainer.SetActive(true);
        //            AppearitionLogger.LogError(error);
        //            if (_saveMessage != null)
        //                _saveMessage.text = error;
        //        }
        //    });

        //while (!hasError.HasValue)
        //    yield return null;

        //if (_saveMessage != null)
        //{
        //    if (!hasError.GetValueOrDefault())
        //        _saveMessage.text = "Yay Marker image was successfully saved!";
        //}

        if (_saveMessageContainer != null)
            _saveMessageContainer.SetActive(true);

        yield return new WaitForSeconds(3);
        if (_saveMessageContainer != null)
            _saveMessageContainer.SetActive(false);
        _saveTargetImageProcess = null;
    }

    void OnDisable()
    {
        if (_saveMessageContainer != null)
            _saveMessageContainer.SetActive(false);
        if (_saveMessage != null)
            _saveMessage.text = "";

        _saveTargetImageProcess = null;
    }
}

