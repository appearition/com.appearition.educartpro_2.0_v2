﻿using System.Collections;
using System.Collections.Generic;
using Appearition.Common;
using UnityEngine;

namespace Appearition.Example
{
    public class Assetbundle_androidArMedia : AssetBundleArMedia
    {
        public override bool IsPlatformAllowedForAssetbundle
        {
            get { return Application.platform == RuntimePlatform.Android || Application.platform == RuntimePlatform.WindowsEditor; }
        }

        public override void Setup(AppearitionExperience associatedExperience, MediaFile media)
        {
            if (!IsPlatformAllowedForAssetbundle)
            {
                //Store the data for future comparisons
                AppearitionLogger.LogInfo("Detecting an assetbundle from another platform. Handling it accordingly.");
                Data = media;
                return;
            }

            base.Setup(associatedExperience, media);
        }

        public override void ChangeDisplayState (bool state)
        {
            if (!IsPlatformAllowedForAssetbundle)
                return;

            base.ChangeDisplayState(state);
        }
    }
}