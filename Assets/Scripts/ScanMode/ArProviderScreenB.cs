﻿using System.Collections;
using System.Collections.Generic;
using Appearition.ArDemo.Vuforia;
using Appearition.Example;
using UnityEngine;
using UnityEngine.Events;
using Vuforia;

namespace Appearition.Rmitilp.UI
{
    public class ArProviderScreenB : MonoBehaviour
    {
        [SerializeField] GameObject _providerHolder;

        IArProviderHandler Provider;

        void Awake()
        {
            Provider = _providerHolder.GetComponent<IArProviderHandler>();
        }

        void OnEnable()
        {
            AppearitionArHandler.Instance.ProviderHandler = Provider;
            //Turn on vuforia
            //if (!Provider.IsArProviderInitialized && Provider is VuforiaCloudProviderHandler vuforiaHandler)
            //    vuforiaHandler.InitializeVuforia();
            //else if (!Provider.IsArProviderInitialized && Provider is VuforiaMarkerlessProviderHandler vuforiaMarkerless)
            //    vuforiaMarkerless.InitializeVuforia();
            //else
                Provider.ChangeArProviderActiveState(true);
            Provider.ChangeScanningState(true);

            Provider.RefocusCamera();
        }

        void OnDisable()
        {
            if (Provider != null && AppearitionArHandler.Instance != null && AppearitionArHandler.Instance.ProviderHandler == Provider)
                AppearitionArHandler.Instance.ProviderHandler = null;

            if (Provider != null)
            {
                //Destroy all AppearitionExperiences.
                Provider.ClearTargetsBeingTracked(false);

                //Turn off Vuforia
                Provider.ChangeArProviderActiveState(false);
            }
        }
    }
}