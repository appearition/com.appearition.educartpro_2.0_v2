﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition.Common;

public class MarkerObjectEdit : MonoBehaviour
{
    //public string markerPath;
    public Image markerImage;
    public GameObject uploadPanel;
    public EditData editData;
    Sprite emsMarkerCache;

    
    public void GetMarkerImage()
    {
        Debug.Log("True");
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                // Create Texture from selected image
                Texture2D texture = NativeGallery.LoadImageAtPath(path, 1024);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }
                editData.markerToUpload.path = path;
                //markerImage.texture = texture;
                markerImage.sprite = ImageUtility.LoadOrCreateSprite(texture);
                markerImage.gameObject.SetActive(true);
                //uploadPanel.SetActive(false);
            }

        }, "Select a PNG image", "image/png");

    }

    public void SetImage(Sprite sp)
    {
        markerImage.sprite = sp;
        markerImage.gameObject.SetActive(true);
        uploadPanel.SetActive(false);
    }

    public void Delete()
    {
        Debug.Log("Delete");
        if (editData.markerToUpload.path != "")
        {
            editData.mediaToUpload.path = "";
            uploadPanel.SetActive(true);
            markerImage.gameObject.SetActive(false);
        }
        else
        {
            editData.markerToUpload.isDeleted = true;
        }
    }
}
