﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenEditPanel : MonoBehaviour
{
    public UserExperienceCardData userExperienceCardData;
    public GameObject EditUIPanel;
    public GameObject previousPanel;

    public void OnEditButtonPressed()
    {
        EditUIPanel.SetActive(true);
        previousPanel.SetActive(false);
        EditUIPanel.GetComponent<EditSetup>().Setup(userExperienceCardData.data);
    }
 
}
