using System.Collections;
using System.Collections.Generic;
using Appearition.ArDemo.Vuforia;
using Appearition.ArTargetImageAndMedia;
using Appearition.Example;
using UnityEngine;
using UnityEngine.UI;

namespace Appearition.Rmitilp
{
    public class MarkerlessUiHandlerB : MonoBehaviour
    {
        const string MARKERLESS_ONBOARDING_MESSAGE_PLAYER_KEY = "MARKERLESS_ONBOARDING_MESSAGE_PLAYER_KEY";
        const string MARKERLESS_TIP_MESSAGE_PLAYER_KEY = "MARKERLESS_ONBOARDING_MESSAGE_PLAYER_KEY";

        enum TypeOfWarningMessage
        {
            SearchingForSurface,
            SurfaceDetected,
            LoadingContent,
        }

        [System.Serializable]
        public class WarningMessageContent
        {
            public string message;
            public string subMessage;
            public Color color = Color.white;
        }

        //Reference
        [SerializeField] GameObject _firstTimePanel;
        [SerializeField] GameObject _markerlessTipPanelHolder;
        [SerializeField] Toggle _markerlessTipAlwayShowTipToggle;
        [SerializeField] Image _warningMessageHolder;
        [SerializeField] Text _warningMessageText;
        [SerializeField] Text _warningMessageSubText;
        [SerializeField] VuforiaMarkerlessProviderHandler _vuforiaMarkerless;

        //Internal Variables
        [SerializeField] List<WarningMessageContent> _warningMessageContent = new List<WarningMessageContent>();
        Coroutine _loadingCheckProcess;

        void Awake()
        { 
            //MarkerlessAssetPlacer.OnAssetPlacerStateChanged += MarkerlessAssetPlacer_OnAssetPlacerStateChanged;
        }

        void Start()
        {
            //ArFoundationMarkerlessHandler.OnCurrentExperienceChanged += ArFoundationMarkerlessHandler_OnCurrentExperienceChanged;
        }

        void OnEnable()
        {
            //if (_loadingCheckProcess == null)
            //    _loadingCheckProcess = StartCoroutine(WaitingForMediaToBeLoaded(ArFoundationMarkerlessHandler.CurrentExperience));

            //ArFoundationMarkerlessHandler_OnCurrentExperienceChanged(null, ArFoundationMarkerlessHandler.CurrentExperience);

            if (!PlayerPrefs.HasKey(MARKERLESS_ONBOARDING_MESSAGE_PLAYER_KEY))
            {
                _firstTimePanel.SetActive(true);
                PlayerPrefs.SetInt(MARKERLESS_ONBOARDING_MESSAGE_PLAYER_KEY, 1);
            }
            else if (!PlayerPrefs.HasKey(MARKERLESS_TIP_MESSAGE_PLAYER_KEY) || PlayerPrefs.GetInt(MARKERLESS_TIP_MESSAGE_PLAYER_KEY) == 0)
            { 
                _markerlessTipAlwayShowTipToggle.SetIsOnWithoutNotify(true);
                _markerlessTipPanelHolder.SetActive(true);
            }
        }

        public void OnMarkerlessTipButtonPressed()
        { 
            PlayerPrefs.SetInt(MARKERLESS_TIP_MESSAGE_PLAYER_KEY, _markerlessTipAlwayShowTipToggle.isOn ? 0 : 1);
            _markerlessTipPanelHolder.SetActive(false);
        }


        //private void MarkerlessAssetPlacer_OnAssetPlacerStateChanged(MarkerlessAssetPlacer.AssetPlacerState newState)
        //{
        //    switch (newState)
        //    {
        //        case MarkerlessAssetPlacer.AssetPlacerState.LookingForGround:
        //            DisplayMessage(TypeOfWarningMessage.SearchingForSurface);
        //            break;

        //        case MarkerlessAssetPlacer.AssetPlacerState.GroundDetected:
        //            DisplayMessage(TypeOfWarningMessage.SurfaceDetected);
        //            break;

        //        //If the asset is placed but not downloaded yet, show the loading display.
        //        case MarkerlessAssetPlacer.AssetPlacerState.AssetPlaced:
        //            if(_loadingCheckProcess != null)
        //                DisplayMessage(TypeOfWarningMessage.LoadingContent);
        //            else
        //                DisplayMessage(null);
        //            break;
        //    }
        //}

        void Update()
        {
            if (_vuforiaMarkerless == null || !_vuforiaMarkerless.IsArProviderActive)
                return;

            switch (_vuforiaMarkerless.CurrentState)
            {
                case VuforiaMarkerlessProviderHandler.MarkerlessPlacementState.LookingForGround:
                    DisplayMessage(TypeOfWarningMessage.SearchingForSurface);
                    break;
                case VuforiaMarkerlessProviderHandler.MarkerlessPlacementState.Loading:
                    DisplayMessage(TypeOfWarningMessage.SurfaceDetected);
                    if(_loadingCheckProcess == null && _vuforiaMarkerless.LastSelectedExperience != null)
                        _loadingCheckProcess = StartCoroutine(WaitingForMediaToBeLoaded(_vuforiaMarkerless.LastSelectedExperience));
                    break;
                case VuforiaMarkerlessProviderHandler.MarkerlessPlacementState.Idle:
                    if (_loadingCheckProcess != null)
                        DisplayMessage(TypeOfWarningMessage.LoadingContent);
                    else
                        DisplayMessage(null);
                    break;
            }
        }
        
        IEnumerator WaitingForMediaToBeLoaded(AppearitionExperience experience)
        {
            if (experience == null)
            {
                yield return new WaitForSeconds(0.3f);
                //experience = ArFoundationMarkerlessHandler.CurrentExperience;
            }

            AppearitionExperience exp = experience;

            while (true)
            {
                //Checks
                if (exp == null || exp.Data == null)
                    break;

                //Check to make sure that all medias are loaded
                bool areAllLoaded = true;

                try
                {
                    for (int i = 0; i < exp.Medias.Count; i++)
                    {
                        if (exp.Medias[i] == null)
                            break;

                        if (!exp.Medias[i].IsMediaReadyAndDownloaded)
                        {
                            areAllLoaded = false;
                            break;
                        }
                    }
                } catch (MissingReferenceException)
                {
                    areAllLoaded = true;
                }

                //All loaded? Turn UI off!
                if (areAllLoaded)
                    break;

                yield return null;
            }

            //if (_loadingUi != null)
            //    _loadingUi.SetActive(false);

            _loadingCheckProcess = null;
            
            //Hide display only if the current display is the loading one. Otherwise, it'll turn off itself.
            if(IsDisplayDisplayActive(TypeOfWarningMessage.LoadingContent))
                DisplayMessage(null);
        }

        void OnDisable()
        {
            if (_loadingCheckProcess != null)
                StopCoroutine(_loadingCheckProcess);
            _loadingCheckProcess = null;

            _firstTimePanel.SetActive(false);
            _markerlessTipPanelHolder.SetActive(false);
            DisplayMessage(null);
        }

        void OnDestroy()
        {

            //MarkerlessAssetPlacer.OnAssetPlacerStateChanged -= MarkerlessAssetPlacer_OnAssetPlacerStateChanged;
        }

        /// <summary>
        /// Display a warning message on the footer UI.
        /// </summary>
        /// <param name="messageType"></param>
        void DisplayMessage(TypeOfWarningMessage? messageType)
        {
            if (_warningMessageHolder == null || _warningMessageText == null)
                return;

            if (!messageType.HasValue)
            {
                _warningMessageHolder.gameObject.SetActive(false);
                _warningMessageText.text = "";
                _warningMessageSubText.text = "";
            }
            else
            {
                if ((int) messageType.Value < 0 || (int) messageType.Value >= _warningMessageContent.Count)
                {
                    _warningMessageHolder.gameObject.SetActive(false);
                    _warningMessageText.text = "";
                    _warningMessageSubText.text = "";
                }
                else
                {
                    _warningMessageText.text = _warningMessageContent[(int) messageType.Value].message;
                    _warningMessageSubText.text = _warningMessageContent[(int) messageType.Value].subMessage;
                    _warningMessageHolder.color = _warningMessageContent[(int) messageType.Value].color;
                    _warningMessageHolder.gameObject.SetActive(true);
                }
            }
        }

        bool IsDisplayDisplayActive(TypeOfWarningMessage messageType)
        { 
            return _warningMessageText.text.Equals(_warningMessageContent[(int)messageType].message);
        }
    }
}