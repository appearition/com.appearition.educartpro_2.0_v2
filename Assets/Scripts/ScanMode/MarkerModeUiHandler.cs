﻿using System.Collections;
using System.Collections.Generic;
using Appearition.Example;
using UnityEngine;
using UnityEngine.UI;

public class MarkerModeUiHandler : MonoBehaviour
{
    const string MARKER_ONBOARDING_MESSAGE_PLAYER_KEY = "MARKER_ONBOARDING_MESSAGE_PLAYER_KEY";

    public enum TypeOfWarningMessage
    {
        SearchingForTarget,
        TargetDetected,
        LoadingContent,
        UnknownStatus,
        Hide
    }

    [System.Serializable]
    public class WarningMessageContent
    {
        public string message;
        public Color color = Color.white;
    }

    //References
    [SerializeField] GameObject _firstTimePanel;
    [SerializeField] Image _warningMessageHolder;
    [SerializeField] Text _warningMessageText;

    //Internal Variables
    [SerializeField] List<WarningMessageContent> _warningMessageContent = new List<WarningMessageContent>();
    Coroutine _mainExperienceDownloadProcess;
    public float delayBeforeShowingUnknownState = 1f;
    float _timerBeforeShowingUnknownState;
    bool _shouldCountdownToShowUnknownState = false;
    public float delayBeforeShowingLoadingState = 2f;
    float _timerBeforeShowingLoadingState;

    void Awake()
    {
        AppearitionArHandler.OnScanStateChanged += AppearitionArHandler_OnScanStateChanged;
        AppearitionArHandler.OnTargetStateChanged += AppearitionArHandler_OnTargetStateChanged;
    }

    void OnDestroy()
    {
        AppearitionArHandler.OnScanStateChanged -= AppearitionArHandler_OnScanStateChanged;
        AppearitionArHandler.OnTargetStateChanged -= AppearitionArHandler_OnTargetStateChanged;
    }

    void OnEnable()
    {
        _timerBeforeShowingUnknownState = delayBeforeShowingUnknownState;
        _shouldCountdownToShowUnknownState = false;

        if (!PlayerPrefs.HasKey(MARKER_ONBOARDING_MESSAGE_PLAYER_KEY))
        {
            _firstTimePanel.SetActive(true);
            PlayerPrefs.SetInt(MARKER_ONBOARDING_MESSAGE_PLAYER_KEY, 1);
        }
        else
        {
            _firstTimePanel.SetActive(false);
        }
    }

    void Update()
    {
        //Unknown state
        if (_shouldCountdownToShowUnknownState)
        {
            _timerBeforeShowingUnknownState -= Time.deltaTime;

            if (_timerBeforeShowingUnknownState < 0)
            {
                DisplayMessage(TypeOfWarningMessage.UnknownStatus);

                _timerBeforeShowingUnknownState = delayBeforeShowingUnknownState;
                _shouldCountdownToShowUnknownState = false;
            }
        }

        //Loading State
        if (_mainExperienceDownloadProcess != null)
        {
            _timerBeforeShowingLoadingState -= Time.deltaTime;

            if (_timerBeforeShowingLoadingState < 0)
            {
                //DisplayMessage(TypeOfWarningMessage.LoadingContent);
                _timerBeforeShowingLoadingState = 9999;
            }
        }
    }

    private void AppearitionArHandler_OnTargetStateChanged(AppearitionExperience arAsset, AppearitionArHandler.TargetState newstate)
    {
        if (arAsset != null && newstate == AppearitionArHandler.TargetState.TargetFound)
        {
            _timerBeforeShowingLoadingState = delayBeforeShowingLoadingState;
            _timerBeforeShowingUnknownState = delayBeforeShowingUnknownState;
            _mainExperienceDownloadProcess = StartCoroutine(WaitingForMediaToBeLoaded(arAsset));
        }

        if (newstate == AppearitionArHandler.TargetState.Unknown)
        {
            _shouldCountdownToShowUnknownState = true;
            _timerBeforeShowingUnknownState = delayBeforeShowingUnknownState;
        }
        else
        {
            _shouldCountdownToShowUnknownState = false;
            if (_mainExperienceDownloadProcess == null && !AppearitionArHandler.Instance.IsProviderScanning)
                DisplayMessage(null);
        }
    }

    private void AppearitionArHandler_OnScanStateChanged(bool isScanning)
    {
        if (isScanning)
            DisplayMessage(TypeOfWarningMessage.SearchingForTarget);
    }

    IEnumerator WaitingForMediaToBeLoaded(AppearitionExperience experience)
    {
        AppearitionExperience exp = experience;

        DisplayMessage(TypeOfWarningMessage.TargetDetected);


        while (true)
        {
            //Checks
            if (exp == null || exp.Data == null)
                break;

            //Check to make sure that all medias are loaded
            bool areAllLoaded = true;

            try
            {
                for (int i = 0; i < exp.Medias.Count; i++)
                {
                    if (exp.Medias[i] == null)
                        break;

                    if (!exp.Medias[i].IsMediaReadyAndDownloaded)
                    {
                        areAllLoaded = false;
                        break;
                    }
                }
            }
            catch (MissingReferenceException)
            {
                areAllLoaded = true;
            }

            //All loaded? Turn UI off!
            if (areAllLoaded)
                break;

            yield return null;
        }

        DisplayMessage(null);
        _mainExperienceDownloadProcess = null;
    }

    void OnDisable()
    {
        DisplayMessage(null);
        _firstTimePanel.SetActive(false);
        _mainExperienceDownloadProcess = null;
    }


    /// <summary>
    /// Display a warning message on the footer UI.
    /// </summary>
    /// <param name="messageType"></param>
    public void DisplayMessage(TypeOfWarningMessage? messageType)
    {
        if (_warningMessageHolder == null || _warningMessageText == null)
            return;

        if (!messageType.HasValue)
        {
            _warningMessageHolder.gameObject.SetActive(false);
            _warningMessageText.text = "";
        }
        else
        {
            if ((int)messageType.Value < 0 || (int)messageType.Value >= _warningMessageContent.Count)
            {
                _warningMessageHolder.gameObject.SetActive(false);
                _warningMessageText.text = "";
            }
            else
            {
                _warningMessageText.text = _warningMessageContent[(int)messageType.Value].message;
                _warningMessageHolder.color = _warningMessageContent[(int)messageType.Value].color;
                _warningMessageHolder.gameObject.SetActive(true);
            }
        }
    }
}
