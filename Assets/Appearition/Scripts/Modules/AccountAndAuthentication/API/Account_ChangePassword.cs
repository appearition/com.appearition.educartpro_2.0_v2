﻿using Appearition.API;

namespace Appearition.AccountAndAuthentication.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Account/ChangePassword/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class Account_ChangePassword : BaseApiPost
    {
        public override AuthenticationOverrideType AuthenticationOverride
        {
            get { return AuthenticationOverrideType.SessionToken; }
        }

        public override int ApiVersion
        {
            get { return 2; }
        }

        //Variables
        public ApiData Data;

        [System.Serializable]
        public class ApiData
        {
        }

        [System.Serializable]
        public class PostApi
        {
            public string OldPassword;
            public string NewPassword;
            public string ConfirmNewPassword;
        }
    }
}