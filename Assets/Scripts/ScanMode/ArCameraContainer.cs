﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArCameraContainer : MonoBehaviour
{
    #region Internal Singleton

    static ArCameraContainer _instance;

    public static ArCameraContainer Instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<ArCameraContainer>();
            return _instance;
        }
    }

    #endregion

    public static Camera CurrentCamera
    {
        get
        {
            if (Instance == null)
                return null;

            Camera[] allCameras = Instance.GetComponentsInChildren<Camera>(false);

            Camera selectedCam = null;
            for (int i = 0; i < allCameras.Length; i++)
            {
                if (allCameras[i] != null && allCameras[i].enabled && allCameras[i].gameObject.activeInHierarchy)
                {
                    if (selectedCam == null || allCameras[i].depth > selectedCam.depth)
                        selectedCam = allCameras[i];
                }
            }

            return selectedCam;
        }
    }
}
