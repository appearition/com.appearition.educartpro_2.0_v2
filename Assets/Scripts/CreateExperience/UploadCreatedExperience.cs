﻿using System.Collections;
using UnityEngine;
using System;
using Appearition;
using Appearition.ArTargetImageAndMedia;
using System.Collections.Generic;
using Appearition.Common;
using System.Linq;

public class UploadCreatedExperience : MonoBehaviour
{

    public CreateData createData;
    ArTarget arTarget;
    public void OnEnable()
    {
    }

    public void UploadExperience(bool shouldPublish)
    {
        StartCoroutine(UploadExperienceCoroutine(shouldPublish));
    }

    public void PublishExp()
    {
        StartCoroutine(PublishExperience());
    }

    public IEnumerator UploadExperienceCoroutine(bool publishOnComplete)
    {
        createData = GetComponent<CreateData>();
        LoadingPanel.Instance.DisplayMessage("Your experience is being created, please wait..", Color.white);

        Texture targetImageTexture = null;
        string markerPath = createData.markerToUpload.path;
        if (!string.IsNullOrWhiteSpace(markerPath))
        {
            Debug.Log(markerPath);
            targetImageTexture = ImageUtility.LoadOrCreateTexture(markerPath);
        }
        Debug.Log(markerPath);

        //Upload the target image!
        ArTarget tmpArTarget = null;
        bool? isArTargetCreationSuccess = null;
        if (createData.isMarker)
        {
            ArTargetHandler.CreateExperience(createData.expName, targetImageTexture, "tmpTarget.jpg", false, onSuccess: tmpOutcome => { tmpArTarget = tmpOutcome; },
                onComplete: isSuccess => { isArTargetCreationSuccess = isSuccess; });
        }
        else
        {
            ArTargetHandler.CreateExperience(createData.expName, false, onSuccess: tmpOutcome => { tmpArTarget = tmpOutcome; },
    onComplete: isSuccess => { isArTargetCreationSuccess = isSuccess; });
        }
        while (!isArTargetCreationSuccess.HasValue)
        {
            // dotAnim.UpdateDisplay();
            yield return null;
        }

        //Check if it didn't manage uploading it..
        if (!isArTargetCreationSuccess.Value)
        {
            AppearitionLogger.LogDebug("Failed creating basic experience");
            DisplayCreationFailedMessage("Failed creating basic experience");
            //Add Failed UX flow
            yield break;
        }

        arTarget = tmpArTarget;
        if (createData.mediaToUpload.path != null)
        {
            string mediaType = createData.mediaToUpload.mediaType;
            if (mediaType.Equals("weblink", StringComparison.InvariantCultureIgnoreCase))
            {
                MediaFile wbMediaFile = new MediaFile();
                wbMediaFile.mediaType = mediaType;
                if (!createData.mediaToUpload.path.Contains(@"https://"))
                {
                    if (createData.mediaToUpload.path.Contains(@"http://"))
                    {
                        createData.mediaToUpload.path.Replace(@"http://", @"https://");
                    }
                    else
                    {
                        createData.mediaToUpload.path = @"https://" + createData.mediaToUpload.path;
                    }
                }
                wbMediaFile.url = createData.mediaToUpload?.path;
                bool? isExperienceCreationSuccess = null;

                Debug.Log("Launching upload media !");
                ArTargetHandler.AddMediaToExistingExperience(tmpArTarget, wbMediaFile, false, (arTargetOutcome, mediaOutcome) => { wbMediaFile = mediaOutcome; },
                    onComplete: isSuccess => { isExperienceCreationSuccess = isSuccess; });

                while (!isExperienceCreationSuccess.HasValue)
                {
                    yield return null;
                }

                if (!isExperienceCreationSuccess.Value)
                {
                    AppearitionLogger.LogDebug("Failed creating weblink experience");

                    yield return DisplayCreationFailedMessage("Failed creating weblink experience");

                    //Delete experience
                    yield return ArTargetHandler.DeleteExperienceProcess(tmpArTarget);
                    yield break;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(mediaType))
                {
                    object mediaToUpload = null;
                    string mediaPath = createData.mediaToUpload.path;
                    if (!string.IsNullOrEmpty(mediaPath))
                    {
                        LoadingPanel.Instance.DisplayMessage("Loading Media", Color.white);
                        //Fetch the file!
                        bool isMediaDoneFetching = false;

                        yield return FileHandler.LoadBytesFromFileProcess(mediaPath, tmpOutcome =>
                        //AppearitionGate.LoadObjectFromPath(mediaPath, tmpOutcome =>
                        {
                            mediaToUpload = tmpOutcome;
                            isMediaDoneFetching = true;
                        });

                        while (!isMediaDoneFetching)
                        {
                            yield return null;
                        }

                        LoadingPanel.Instance.DisplayMessage("Media done loading !" , Color.white);
                        Debug.Log("Media done loading !" + mediaToUpload);
                    }
                    else
                    {
                        yield return DisplayCreationFailedMessage("Media Not Found");
                        //Delete experience
                        yield return ArTargetHandler.DeleteExperienceProcess(tmpArTarget);
                        yield break;
                    }

                    //Make a media to upload
                    MediaFile tmpMediaToUpload = new MediaFile();
                    string mediaExtension = "";

                    //Ignore None for mediatype
                    MediaType tmpMediaType = MediaTypeHandler.ActiveMediaTypes.First(o => o.Name.Equals(mediaType, StringComparison.InvariantCultureIgnoreCase));

                    if (mediaToUpload != null && tmpMediaType != null)
                    {
                        mediaExtension = System.IO.Path.GetExtension(mediaPath);
                        tmpMediaToUpload.mimeType = tmpMediaType.GetMimeTypeForGivenExtension(mediaExtension);
                    }

                    tmpMediaToUpload.fileName = "TmpFile" + mediaExtension;
                    if (tmpMediaType != null)
                        tmpMediaToUpload.mediaType = tmpMediaType.Name;
                    tmpMediaToUpload.isPrivate = false;

                    //Upload the experience!
                    bool? isExperienceCreationSuccess = null;

                    Debug.Log("Launching upload media !");
                    ArTargetHandler.AddMediaToExistingExperience(tmpArTarget, tmpMediaToUpload, mediaToUpload, false, (arTargetOutcome, mediaOutcome) => { tmpMediaToUpload = mediaOutcome; },
                        onComplete: isSuccess => { isExperienceCreationSuccess = isSuccess; });

                    while (!isExperienceCreationSuccess.HasValue)
                    {
                        yield return null;
                    }

                    if (isExperienceCreationSuccess.Value)
                    {
                        LoadingPanel.Instance.DisplayMessage("Media Successfully Uploaded!", Color.white);
                        Debug.Log("Media successfully uploaded !");
                        createData.currentTarget = tmpArTarget;
                    }
                    else
                    {
                        AppearitionLogger.LogDebug("Failed uploading media");
                        yield return DisplayCreationFailedMessage("Failed uploading media");
                        //Delete experience
                        yield return ArTargetHandler.DeleteExperienceProcess(tmpArTarget);
                        yield break;
                    }

                }
            }
        }
        else
        {
            //The media being dealt with is empty, Publish it anyway!
            bool? isPublishRequestComplete = null;

            ArTargetHandler.PublishExperience(tmpArTarget, onComplete: isSuccess => { isPublishRequestComplete = isSuccess; });

            //while (!isPublishRequestComplete.HasValue)
            //{
            //    yield return null;
            //}

            //if (isPublishRequestComplete.Value)
            //{
            LoadingPanel.Instance.HideMessage();
            ExperienceSuccessPanel.Instance.Activate(3f);
            gameObject.SetActive(false);
            Debug.Log("Empty target successfully saved !");
            //}
            //else
            //{
            //    AppearitionLogger.LogDebug("Failed save");
            //    yield return DisplayCreationFailedMessage("Failed save");
            //    //Delete experience
            //    //yield return ArTargetHandler.DeleteExperienceProcess(tmpArTarget);
            //    yield break;
            //}

        }

        int arTargetId = tmpArTarget.arTargetId;

        LoadingPanel.Instance.HideMessage();
        ExperienceSuccessPanel.Instance.Activate(3f);
        if (publishOnComplete)
        {
            PublishExp();
        }
        else
        {
            gameObject.SetActive(false);
        }

    }

    IEnumerator PublishExperience()
    {
        bool? isPublishRequestComplete = null;

        ArTargetHandler.PublishExperience(arTarget, onComplete: isSuccess => { isPublishRequestComplete = isSuccess; });

        while (!isPublishRequestComplete.HasValue)
        {
            yield return null;
        }

        if (isPublishRequestComplete.Value)
        {
            LoadingPanel.Instance.HideMessage();
            //ExperienceSuccessPanel.Instance.Activate(3f);
            gameObject.SetActive(false);
            Debug.Log("Experience successfully Published !");
        }
        else
        {
            AppearitionLogger.LogDebug("Failed publishing");
            yield return DisplayCreationFailedMessage("Failed publishing");
            yield break;
        }
    }
    IEnumerator DisplayCreationFailedMessage(string message)
    {
        ExperienceFailPanel.Instance.Activate(3f);
        yield return null;
        gameObject.SetActive(false);
    }

}
