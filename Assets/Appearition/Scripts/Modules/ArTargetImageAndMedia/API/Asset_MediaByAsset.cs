﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: Asset_MediaByAsset.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Appearition.API;
using Appearition.Common;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Asset/MediaByAsset/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class Asset_MediaByAsset : BaseApiGet
    {
        public override int ApiVersion
        {
            get { return 2; }
        }

        public List<MediaFile> Data;

        /// <summary>
        /// Post ApiData
        /// </summary>
        [System.Serializable]
        public class PostData
        {
            public string assetId;
            public List<Parameter> parameters;
        }

        public override Dictionary<string, string> JsonReplaceKvp { get; } = new Dictionary<string, string>() { { "parameters", "params" } };
    }
}