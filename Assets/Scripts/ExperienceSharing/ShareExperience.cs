﻿using System.Linq;
using Appearition.ArTargetImageAndMedia;
using UnityEngine;
using ImaginationOverflow.UniversalDeepLinking;
using Appearition;
using System;
using System.Collections;
using System.IO;

public class ShareExperience : MonoBehaviour
{

    public ActionSheetData data;
    public UserActionSheetData Ardata;

    public void Share()
    {
        Asset asset;
        if (data != null)
        {
            asset = data.asset;
        }
        else
        {
            asset = Ardata.asset;
        }

        string pathToTargetImage = "";

        if (asset.targetImages != null && asset.targetImages.Length > 0)
            pathToTargetImage = ArTargetHandler.GetPathToTargetImage(asset, asset.targetImages.First());

        string deepLink = GenerateLink(asset);

        StartCoroutine(Share(deepLink, pathToTargetImage));
    }

    string GenerateLink(Asset asset)
    {
        AppLinkingConfiguration deepLinkConfig = ImaginationOverflow.UniversalDeepLinking.Storage.ConfigurationStorage.Load();
        LinkInformation selectedConfig = null;


#if UNITY_IOS
            if (deepLinkConfig.DeepLinkingProtocols == null || deepLinkConfig.DeepLinkingProtocols.Count == 0)
            {
                return "";
            }

            //Just pick the first entry.
            selectedConfig = deepLinkConfig.DeepLinkingProtocols.First();
#else
        if (deepLinkConfig.DomainProtocols == null || deepLinkConfig.DomainProtocols.Count == 0)
        {
            return "";
        }

        //Look for a HTTPS scheme first
        selectedConfig =
            deepLinkConfig.DomainProtocols.FirstOrDefault(o => o.Scheme.Equals("https", StringComparison.InvariantCultureIgnoreCase));

        if (selectedConfig == null)
            selectedConfig = deepLinkConfig.DomainProtocols.First();
#endif

        return $"{selectedConfig.Scheme}://" +
               $"{selectedConfig.Host}/" +
               $"{(string.IsNullOrWhiteSpace(selectedConfig.Path) ? "" : selectedConfig.Path + "/")}" +
               $"Share?TenantKey={AppearitionGate.Instance.CurrentUser.selectedTenant}&AssetId={asset.assetId}";
    }

    private IEnumerator Share(string link, string imagePath)
    {
        yield return new WaitForEndOfFrame();

        if (imagePath != null)
        {
            new NativeShare().AddFile(imagePath).SetSubject(link).SetText(link).Share();
        }
        else
        {
            new NativeShare().SetSubject(link).SetText(link).Share();
        }



        // Share on WhatsApp only, if installed (Android only)
        //if( NativeShare.TargetExists( "com.whatsapp" ) )
        //	new NativeShare().AddFile( filePath ).SetText( "Hello world!" ).SetTarget( "com.whatsapp" ).Share();
    }
}
