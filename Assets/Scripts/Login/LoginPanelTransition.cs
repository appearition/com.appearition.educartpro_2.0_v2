﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoginPanelTransition : MonoBehaviour
{

    public GameObject tenantSelection;

    //Added to Unity Event in Login_Panel
    public void DisableCanvas()
    {
        gameObject.GetComponent<Canvas>().enabled = false;
    }
}
