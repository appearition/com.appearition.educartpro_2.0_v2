﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Appearition.ArTargetImageAndMedia;
using Appearition.ChannelManagement;
using UnityEngine.UI;

public class DisplayPublicExperiences : MonoBehaviour
{
    public GameObject experienceCard;
    public GameObject contentParent;
    public Text fetchingMessageText;
    public GameObject fetchingMessage;
    public DisplayFiltering dFilter;
    List<Asset> userAssets = new List<Asset>();
    public InputField searchInput;

    int expCount = -1;
    List<GameObject> experiencesCache = new List<GameObject>();
    int page = 0;

    private void OnEnable()
    {
        RefreshData(true);
        searchInput.onValueChanged.AddListener(delegate { SearchData(searchInput); });
    }



    private void OnDisable()
    {
        ClearExperiences();
        searchInput.onValueChanged.RemoveListener(delegate { SearchData(searchInput); });
    }

    public void ClearSearch()
    {
        Debug.LogError("Clear");
        searchInput.text = "";
        RefreshData(true);
    }

    public void SearchData(InputField data)
    {
        RefreshData(true);
    }

    public void RefreshData(bool resetPaging)
    {
        if (resetPaging)
        {
            expCount = 0;
            page = 0;
            userAssets.Clear();
        }
        StopAllCoroutines();
        StartCoroutine(GetData());
    }

    public void AddNextSet()
    {
        //DispCountLimitedExperiences(10);
        RefreshData(false);
    }

    public IEnumerator GetData()
    {
        fetchingMessageText.text = "Fetching Experiences...";
        fetchingMessage.SetActive(true);
        Channel channelIfAny = null;
        List<string> allowedTags = new List<string>();
        yield return dFilter.GetChannelAndFiltersForQueryProcess((c, t) =>
        {
            channelIfAny = c.FirstOrDefault();
            allowedTags.AddRange(t);
        });
        CoroutineWithData cd = new CoroutineWithData(this, FetchExperiences.Instance.GetPublicAndDefaultAssetProcess(channelIfAny, allowedTags, page, searchInput.text));
        yield return cd.coroutine;
        if (page == 0)
        {
            userAssets.Clear();
            userAssets = (List<Asset>)cd.result;
            ClearExperiences();
        }
        else
            userAssets.AddRange((List<Asset>)cd.result);
        page++;

        Debug.Log("result is " + userAssets.Count); //  'success' or 'fail'

        //if (experiencesCache.Count > 0)
        //{
        //    ClearExperiences();
        //}

        DispCountLimitedExperiences(10);
        if (userAssets.Count == 0)
        {
            fetchingMessageText.text = "No experiences found!";
        }
        else
        {
            fetchingMessage.SetActive(false);
        }
    }


    void DispCountLimitedExperiences(int count)
    {
        while (count > 0)
        {
            if (expCount >= userAssets.Count)
            {
                break;
            }

            //if (dFilter.defaultExp && !userAssets[expCount].name.Contains("[DEFAULT]"))
            //{
            //    continue;
            //}

            //if (dFilter.markerExp && !userAssets[expCount].IsMarker)
            //{
            //    continue;
            //}

            //if (dFilter.markerlessExp && !userAssets[expCount].IsMarkerless)
            //{
            //    continue;
            //}

            count--;
            GameObject go = Instantiate(experienceCard, contentParent.transform);
            go.SetActive(true);

            go.GetComponent<ExperienceCardSetup>().Setup(userAssets[expCount]);
            experiencesCache.Add(go);
            expCount++;
        }
    }

    public void ClearExperiences()
    {
        for (int i = experiencesCache.Count - 1; i >= 0; i--)
        {
            Destroy(experiencesCache[i]);
        }

        experiencesCache.Clear();
    }
}