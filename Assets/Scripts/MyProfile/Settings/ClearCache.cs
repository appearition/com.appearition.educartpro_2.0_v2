﻿using System.Collections;
using System.Collections.Generic;
using Appearition.ArTargetImageAndMedia;
using UnityEngine;

public class ClearCache : MonoBehaviour
{
    [SerializeField] GameObject _clearingCacheProgress;
    [SerializeField] GameObject _clearingCacheSuccess;
    Coroutine _clearCacheProcess;
    public float timeToDisplayCacheClearingSuccessPanel = 3.0f;

    public void ClearAppCache()
    {
        if (_clearCacheProcess == null)
            _clearCacheProcess = StartCoroutine(ClearCacheProcess());
    }

    IEnumerator ClearCacheProcess()
    {
        if (_clearingCacheProgress != null)
            _clearingCacheProgress.SetActive(true);

        //First, clear cache.
        bool isSuccess = false;
        yield return ArTargetHandler.ClearHandlerCache<ArTargetHandler>(b => isSuccess = b);

        //Show success panel either way, I guess
        if (_clearingCacheProgress != null)
            _clearingCacheProgress.SetActive(false);
        if (_clearingCacheSuccess != null)
            _clearingCacheSuccess.SetActive(true);

        //Wait a bit before closing the last panel
        yield return new WaitForSeconds(timeToDisplayCacheClearingSuccessPanel);
        if (_clearingCacheSuccess != null)
            _clearingCacheSuccess.SetActive(false);

        _clearCacheProcess = null;
    }

    void OnDisable()
    {
        if (_clearingCacheProgress != null)
            _clearingCacheProgress.SetActive(false);
        if (_clearingCacheSuccess != null)
            _clearingCacheSuccess.SetActive(false);

        _clearCacheProcess = null;
    }
}
