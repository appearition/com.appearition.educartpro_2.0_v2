﻿using UnityEngine;
using Vuforia;

namespace Appearition.Example
{
    [RequireComponent(typeof(ImageTargetBehaviour))]
    public class VuforiaTargetEventHandler : MonoBehaviour//, ITrackableEventHandler
    {
        //References
        public AppearitionExperience ExperienceRef { get; private set; }
        public TrackableBehaviour ImageTargetRef { get; private set; }

        /// <summary>
        /// Setup the TargetImage Tracker event handler.
        /// </summary>
        /// <param name="attachedExperience"></param>
        /// <param name="newImageTarget"></param>
        public void Setup(AppearitionExperience attachedExperience, TrackableBehaviour newImageTarget)
        {
            ExperienceRef = attachedExperience;
            ImageTargetRef = newImageTarget;
            //ImageTargetRef.RegisterTrackableEventHandler(this);
            ImageTargetRef.RegisterOnTrackableStatusChanged(OnTrackableStatusChangedAction);
        }

        private void OnTrackableStatusChangedAction(TrackableBehaviour.StatusChangeResult obj)
        {
            Debug.LogError(obj.NewStatus);
            if (ExperienceRef != null)
            {
                if (obj.NewStatus == TrackableBehaviour.Status.NO_POSE && ImageTargetRef.CurrentStatusInfo == TrackableBehaviour.StatusInfo.UNKNOWN)
                    ExperienceRef.ArProviderTrackingStateChanged(null);
                else
                    ExperienceRef.ArProviderTrackingStateChanged(obj.NewStatus == TrackableBehaviour.Status.TRACKED ||
                                                                 (obj.NewStatus == TrackableBehaviour.Status.EXTENDED_TRACKED && AppearitionArHandler.UseExtendedTracking));
            }
        }

        public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus)
        {
            Debug.LogError(newStatus);
            if (ExperienceRef != null)
            {
                if (newStatus == TrackableBehaviour.Status.NO_POSE && ImageTargetRef.CurrentStatusInfo == TrackableBehaviour.StatusInfo.UNKNOWN)
                    ExperienceRef.ArProviderTrackingStateChanged(null);
                else
                    ExperienceRef.ArProviderTrackingStateChanged(newStatus == TrackableBehaviour.Status.TRACKED ||
                                                                 (newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED && AppearitionArHandler.UseExtendedTracking));
            }
        }
    }
}