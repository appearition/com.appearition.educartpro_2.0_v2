﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NavigationHistory : Singleton<NavigationHistory>
{
    public Stack<GameObject> history = new Stack<GameObject>();

    public void SetPrevMenu(GameObject go)
    {
        history.Push(go);
    }

    public void GetPrevMenu()
    {
        history.Pop().SetActive(true);
    }
}
