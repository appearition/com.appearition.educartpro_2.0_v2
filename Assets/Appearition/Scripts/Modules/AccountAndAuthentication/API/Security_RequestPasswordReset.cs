﻿using Appearition.API;

namespace Appearition.AccountAndAuthentication.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Security/RequestResetPassword/0?username=1 ,
    /// where 0 is Channel ID and 1 is the username.
    /// </summary>
    public class Security_RequestPasswordReset : BaseApiPost
    {
        public override int ApiVersion
        {
            get { return 2; }
        }

        //Variables
        public string Data;

        //Request Params
        [System.Serializable]
        public class RequestContent : BaseRequestContent
        {
            public string username;
        }
    }
}