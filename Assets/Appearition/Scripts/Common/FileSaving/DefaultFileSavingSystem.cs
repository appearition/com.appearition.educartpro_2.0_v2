﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: DefaultFileSavingSystem.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

//using UnityEngine;

//namespace Appearition
//{
//    /// <summary>
//    /// A sample FileSavingSystem settings with the basic recommended settings.
//    /// Feel free to create your own by creating a new class which derives from BaseFileSavingSystem, and override properties as needed.
//    /// </summary>
//    public class DefaultFileSavingSystem : BaseFileSavingSystem
//    {
//        #region Settings

//        public override string RootLocation
//        {
//            get { return Application.streamingAssetsPath; }
//        }

//        #region Json

//        /// <summary>
//        /// Whether or not the JSON files should be encrypted or left as UTF8 text. If true, remember to override JsonEncryptionKey to customize the key.
//        /// </summary>
//        protected override bool EncryptJsonFiles
//        {
//            get { return false; }
//        }

//        #endregion

//        #endregion
//    }
//}

