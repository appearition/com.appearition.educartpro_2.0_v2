﻿using System.Collections.Generic;
using UnityEngine;


namespace Hologo2
{
    /// <summary>
    /// Created by: Hamid (hamidibrahim@gmail.com) - 17 july 2019
    /// Modified by: Shariz - 20th September 2020
    /// this class contains data object for labels and audio clip
    /// </summary>
    [System.Serializable]
    public class experienceLabelNarrationDataClass
    {
        // slide name in english, for ease in translation
        public string slideName;
        // the actual slide that will be shown in app
        public string localizedName;
        // the description window that comes up for some experiences. For example, the equation and workings for a Math experience
        [Header("DESCRIPTION")]
        public string desciption;
        public bool isInfoDisplayed;
        public Vector3 descriptionPosition;
        public labelAlignment descriptionAlignment;
        // label list and narration
        [Header("LABELS")]
        public bool labelsExist = false;
        public List<localizedSlide> labelList;
        [Header("NARRATION")]
        public bool isNarrationEnabled;
        public AudioClip Narration;

    }

    [System.Serializable]
    public class localizedSlide
    {
        // for english title . ease in translation
        public string labelName;
        // actual label. that needs to be translated. if this is left out empty
        // then label wont be generated
        public string label;
        // description
        public string description;
        // label alignment
        public labelAlignment labelAlignment;
    }

}
