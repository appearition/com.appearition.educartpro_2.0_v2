﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition.Common;

public class ImageMediaObject : MonoBehaviour
{
    //public string markerPath;
    public Image markerImage;
    public GameObject uploadPanel;
    public CreateData createData;
    Sprite emsMarkerCache;

    

    
    public void GetMediaImage()
    {
        Debug.Log("True");
        NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        {
            Debug.Log("Image path: " + path);
            if (path != null)
            {
                //Create Texture from selected image

               Texture2D texture = NativeGallery.LoadImageAtPath(path, 1024);
                if (texture == null)
                {
                    Debug.Log("Couldn't load texture from " + path);
                    return;
                }
                createData.mediaToUpload.path = path;
                createData.mediaToUpload.mediaType = "image";
                //markerImage.texture = texture;
                markerImage.sprite = ImageUtility.LoadOrCreateSprite(texture);
                markerImage.gameObject.SetActive(true);
                uploadPanel.SetActive(false);
            }

        }, "Select a PNG image", "image/png");

    }

    public void Delete()
    {
        createData.mediaToUpload.mediaType = "";
        createData.mediaToUpload.path = "";
        uploadPanel.SetActive(true);
        markerImage.gameObject.SetActive(false);
    }
}
