﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition.AccountAndAuthentication;
using UnityEngine.UI;

public class TenantListElement : MonoBehaviour
{
    public void Setup(TenantData td)
    {
        GetComponent<TenantElementData>().elementTenant = td;
        GetComponentInChildren<Text>().text = td.tenantName;
    }
}
