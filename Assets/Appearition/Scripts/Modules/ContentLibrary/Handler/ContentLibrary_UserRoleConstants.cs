﻿namespace Appearition
{
    public static partial class UserRoleConstants
    {
        public const string CONTENT_LIBRARY_MANAGER = "ContentLibraryManager";

        //Handy Properties
        public static bool HasPermissionToManageContentLibrary => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(CONTENT_LIBRARY_MANAGER);
    }
}