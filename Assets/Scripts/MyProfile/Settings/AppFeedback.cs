﻿using System.Collections;
using System.Collections.Generic;
using Appearition.ChannelManagement;
using UnityEngine;
using UnityEngine.UI;

public class AppFeedback : MonoBehaviour
{
    //References
    [SerializeField] InputField _mainFeedbackInput;
    [SerializeField] GameObject _feedbackFormPanel;
    [SerializeField] GameObject _feedbackSuccessPanel;
    [SerializeField] Button _sendButton;
    public float timeAfterSubmission = 2.0f;

    public void OnSendAnotherFeedbackButtonPressed()
    {
        OnDisable();
    }

    public void OnInputFieldTextEntered(string text)
    {
        _sendButton.interactable = !string.IsNullOrWhiteSpace(text);
    }

    public void OnCloseButtonPressed()
    {
        //Always close the success panel, just in case
        _feedbackSuccessPanel.SetActive(false);
        gameObject.SetActive(false);
    }

    public void OnSendButtonPressed()
    {
        //No text, no goodies.
        if (string.IsNullOrWhiteSpace(_mainFeedbackInput.text))
            return;

        _feedbackFormPanel.SetActive(false);

        //Fire the Feedback API. Whether it completes now or later isn't a worry.
        FeedbackContent feedback = new FeedbackContent()
        {
            comments = _mainFeedbackInput.text,
            rating = 100 //Because we're just that good.
        };

        ChannelHandler.SendFeedback(feedback);

        //Change panel, whether success or failure.
        _feedbackSuccessPanel.SetActive(true);

        Invoke("ClosePanelAfterCompletion", timeAfterSubmission);
    }

    void ClosePanelAfterCompletion()
    {
        _feedbackSuccessPanel.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }

    void OnDisable()
    {
        //Empty text
        _mainFeedbackInput.text = "";
        _feedbackSuccessPanel.SetActive(false);
        _feedbackFormPanel.SetActive(true);
    }
}
