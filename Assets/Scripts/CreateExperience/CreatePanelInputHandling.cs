﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition;
using UnityEngine.UI;

public class CreatePanelInputHandling : MonoBehaviour
{
    public CreateData createData;
    public InputField nameHeader;
    public Button publishButton;
    public Button saveButton;
    public void OnEnable()
    {
        nameHeader.Select();
        nameHeader.ActivateInputField();
    }
    public void EditExperienceName()
    {
        nameHeader.interactable = true;
        nameHeader.Select();
    }

    public void OnEditExperienceNameDone()
    {
        //nameHeader.interactable = false;
        createData.expName = nameHeader.text;
        if (nameHeader.text != "Untitled Experience" || nameHeader.text != null)
        {
            saveButton.interactable = true; 
            if (UserRoleConstants.HasPermissionToPublishExperiences)
            {
                publishButton.interactable = true;
            }
            else
            {
                publishButton.interactable = false;
            }
        }


    }

    public void isMarkerAR(bool value)
    {
        if (value)
        {
            createData.isMarker = true;
        }
    }

    public void isMarkerlessAR(bool value)
    {
        if (value)
        {
            Debug.Log("MARKERLESSAR");
            createData.isMarker = false;
        }
    }

    public void isPublicExp(bool value)
    {

        if (value)
        {
            createData.isPublic = true;
        }
    }

    public void isPrivateExp(bool value)
    {
        if (value)
        {
            createData.isPublic = false;
        }
    }

}
