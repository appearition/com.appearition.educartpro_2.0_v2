﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition;
using Appearition.AccountAndAuthentication;
using UnityEngine.UI;


public class TenantListDisplay : MonoBehaviour
{
    public GameObject tenantListElement;    
    public GameObject UIParent;

    List<GameObject> tenantElementsCache = new List<GameObject>();

    private void OnEnable()
    {
        for(int i = tenantElementsCache.Count-1; i >= 0; i--)
        {
            Destroy(tenantElementsCache[i]);
        }
        tenantElementsCache.Clear();
        foreach(TenantData tenantData in AppearitionGate.Instance.CurrentUser.allTenantsAvailable)
        {
            GameObject go = Instantiate(tenantListElement, UIParent.transform);
            go.SetActive(true);
            go.GetComponent<TenantListElement>().Setup(tenantData);
            tenantElementsCache.Add(go);

        }
    }

}
