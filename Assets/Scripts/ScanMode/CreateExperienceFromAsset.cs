﻿using Appearition.ArTargetImageAndMedia;
using Appearition.Example;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateExperienceFromAsset : Singleton<CreateExperienceFromAsset>
{
    public AppearitionExperience Create(Asset asset)
    {
        if (asset == null)
            return null;

        var tmpExperience = new GameObject(string.Format("Experience Name: {0}, Id: {1}", asset.name, asset.assetId)).AddComponent<AppearitionExperience>();
        tmpExperience.transform.SetParent(transform);

        //Init it
        tmpExperience.SetupExperience(asset);

        return tmpExperience;
    }
}
