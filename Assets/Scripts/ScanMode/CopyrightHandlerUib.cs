﻿using System.Collections;
using System.Collections.Generic;
using Appearition.ArTargetImageAndMedia;
using Appearition.Example;
using UnityEngine;

public class CopyrightHandlerUib : MonoBehaviour
{
    //References
    [SerializeField] CopyrightInfoTextParserUib _copyrightPanel;
    [SerializeField] GameObject _copyrightButton;
    
    //Internal Variables
    AppearitionExperience _currentExperience;
    Coroutine _currentProcess;
    
    void OnEnable()
    {
        AppearitionArHandler.OnTargetStateChanged += AppearitionArHandler_OnTargetStateChanged;
    }

    private void AppearitionArHandler_OnTargetStateChanged(AppearitionExperience experience, AppearitionArHandler.TargetState newState)
    {
        if (experience == null)
        {
            _copyrightButton.SetActive(false);
            return;
        }

        //If the experience is different, take action.
        if (_currentExperience == null || _currentExperience != experience)
        {
            //Something changed? Disable the copyright button for the time being.
            _copyrightButton.SetActive(false);
            
            if (experience.Data == null)
                return;
            
            _currentExperience = experience;

            //If the copyright info is already loaded, display it!
            if (!string.IsNullOrEmpty(experience.Data.copyrightInfo))
                DisplayCopyrightInfo(experience.Data.copyrightInfo);
            else
            { 
                //Otherwise, fetch the copyright info if any!
                if(_currentProcess != null)
                    StopCoroutine(_currentProcess);
                _currentProcess = StartCoroutine(RefreshCopyrightInfo(experience.Data));
            }
        }
    }

    IEnumerator RefreshCopyrightInfo(Asset data)
    { 
        //If the data is an ArTarget, handle it differently.
        if(data is ArTarget arTarget && !arTarget.isPublished)
            yield return ArTargetHandler.GetArTargetCopyrightInfoProcess(arTarget);
        else
            yield return ArTargetHandler.GetExperienceCopyrightInfoProcess(data);
        
        if(!string.IsNullOrEmpty(data.copyrightInfo))
            DisplayCopyrightInfo(data.copyrightInfo);
    }

    void DisplayCopyrightInfo(string copyrightInfo)
    { 
        _copyrightButton.SetActive(true);
        _copyrightPanel.DisplayHtmlText(copyrightInfo);
    }

    void OnDisable()
    {
        AppearitionArHandler.OnTargetStateChanged -= AppearitionArHandler_OnTargetStateChanged;
        _copyrightButton.SetActive(false);
        _copyrightPanel.gameObject.SetActive(false);
        _currentExperience = null;
        _currentProcess = null;
    }
}
