﻿namespace Assets.GifAssets.GifReplay
{
	using UnityEngine;
	using UnityEngine.UI;

	namespace Assets.SimpleColorPicker.Scripts
	{
		/// <summary>
		/// Generates rainbow gradient for testing image conversion to 8-bit colors.
		/// </summary>
		public class Rainbow : MonoBehaviour
		{
			public void Start()
			{
				var texture = new Texture2D(1024, 1);

				for (var i = 0; i < texture.width; i++)
				{
					texture.SetPixel(i, 0, Color.HSVToRGB((float) i / (texture.width - 1), 1f, 1f));
				}

				texture.Apply();
				GetComponent<Image>().sprite = Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100f);
			}
		}
	}
}