﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingPanel : Singleton<LoadingPanel>
{
    public Text messageText;
    // public List<GameObject> objectsToToggle;
    public Canvas canvasToToggle;

    public void DisplayMessage(string message, Color col)
    {
        messageText.text = message;
        messageText.color = col;

        canvasToToggle.enabled = true;
    }

    public void HideMessage()
    {
        canvasToToggle.enabled = false;
    }
}
