﻿using Appearition.API;

namespace Appearition.AccountAndAuthentication.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Registration/Register/0 , where 0 is Channel ID 
    /// </summary>
    [System.Serializable]
    public class Registration_Register : BaseApiPost
    {
        public override int ApiVersion
        {
            get { return 2; }
        }

        //Variables
        public ApiData Data;

        [System.Serializable]
        public class ApiData : AccountStatus
        {
        }

        [System.Serializable]
        public class PostApi : RegistrationForm
        {
        }
    }
}