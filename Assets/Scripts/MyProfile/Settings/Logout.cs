﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Appearition.AccountAndAuthentication;
using UnityEngine;

public class Logout : MonoBehaviour
{
    public delegate void LogoutOccured();

    public static event LogoutOccured OnLogoutOccured;

    public GameObject loginPanel;

    public void OnLogoutButtonPressed()
    {
        AccountHandler.Logout(true);
        //PlayerPrefs.DeleteAll();
        OnLogoutOccured?.Invoke();

        // opens login panel
        loginPanel.SetActive(true);
        this.gameObject.SetActive(false);
    }
}