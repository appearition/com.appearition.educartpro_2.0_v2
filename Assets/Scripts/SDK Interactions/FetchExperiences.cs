﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text.RegularExpressions;
using Appearition.ArTargetImageAndMedia;
using Appearition.Example;
using UnityEngine.UI;
using System.Linq;
using Appearition;
using Appearition.ChannelManagement;
using Appearition.Common;
using Appearition.Tenant;
using UnityEngine.UIElements;

public class FetchExperiences : Singleton<FetchExperiences>
{
    //Filter Settings
    public FilterTenantSettingsDataC filterTenantSettings = new FilterTenantSettingsDataC();

    #region Event Handling

    void Awake()
    {
        Login.OnLoginOccured += Logout_OnLogoutOccured;
        Logout.OnLogoutOccured += Logout_OnLogoutOccured;
    }

    private void Logout_OnLogoutOccured()
    {
        if(filterTenantSettings != null)
            filterTenantSettings.isSetup = false;
    }

    protected override void OnDestroyOccured()
    {
        Login.OnLoginOccured -= Logout_OnLogoutOccured;
        Logout.OnLogoutOccured -= Logout_OnLogoutOccured;
    }

    #endregion

    public IEnumerator GetUserContent(List<Channel> channels, List<string> filters)
    {
        //Check tenant settings
        if (!filterTenantSettings.isSetup)
            yield return RefreshFilterTenantSettings();

        List<ArTarget> arTargets = new List<ArTarget>();
        //Get all accessible channel IDs.
        //List<int> channelIds = new List<int>();

        //yield return ChannelHandler.GetAllChannelsProcess(successChannels =>
        //{
        //    for (int i = 0; i < successChannels.Count; i++)
        //    {
        //        Debug.Log($"Access to channel of id {successChannels[i].channelId}.");
        //        channelIds.Add(successChannels[i].channelId);
        //    }
        //}, error => { Debug.LogError("Error fetching assets"); });

        //MediaTypes
        bool areMediaTypeRefreshed = false;
        MediaTypeHandler.GetActiveMediaTypes(list => areMediaTypeRefreshed = true);

        while (!areMediaTypeRefreshed)
            yield return null;

        //Setup query
        var query = ArTargetConstant.GetDefaultArTargetListQuery();
        query.FilterTagsUsingAnd = false;
        //if(!UserRoleConstants.IsAdmin)
        //query.CreatedByUsername = AppearitionGate.Instance.CurrentUser.username;
        List<string> givenStatus = new List<string>();

        for (int k = 0; k < filters.Count; k++)
        {
            if (filterTenantSettings.stateTags.Contains(filters[k]))
                givenStatus.Add(filters[k]);
            else
                query.Tags.Add(filters[k]);
        }

        //Status
        if (givenStatus.Count == filterTenantSettings.stateTags.Count)
            query.Status = "";
        else if (givenStatus.Count >= 1)
            query.Status = givenStatus.First();

        //User
        for (int i = 0; i < channels.Count; i++)
        {
            AppearitionLogger.LogDebug(channels[i].name + " - " + AppearitionConstants.SerializeJson(query));

            yield return ArTargetHandler.GetSpecificArTargetByQueryProcess(channels[i].channelId, query, false, false, successAssets =>
            {
                //Add them to the User asset list, but 
                for (int k = 0; k < successAssets.Count; k++)
                {
                    arTargets.Add(successAssets[k]);
                }
            });
        }
        
        yield return arTargets;
    }


    public IEnumerator GetPublicAndDefaultAssetProcess(Channel channel, List<string> filters, int page, string searchName)
    {
        if (!filterTenantSettings.isSetup)
            yield return RefreshFilterTenantSettings();

        List<Asset> assets = new List<Asset>();

        var query = ArTargetConstant.GetDefaultAssetListQuery();
        query.Name = searchName;
        query.FilterTagsUsingAnd = false;

        for (int i = 0; i < filters.Count; i++)
        {
            if (filterTenantSettings.stateTags.Contains(filters[i]))
                continue;
            query.Tags.Add(filters[i]);
        }

        query.RecordsPerPage = 10;
        query.Page = page;

        AppearitionLogger.LogDebug(AppearitionConstants.SerializeJson(query));

        yield return ArTargetHandler.GetSpecificExperiencesByQueryProcess(channel == null ? 0 : channel.channelId, query, false, false, false,
            successAssets => { assets.AddRange(successAssets); });


        yield return assets;
    }

    public IEnumerator RefreshFilterTenantSettings()
    {
        yield return TenantHandler.GetTenantSettingsProcess(success => filterTenantSettings.UpdateData(success));
    }
}