﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteExpConfirmationPopup : MonoBehaviour
{
    public bool? canProceed = null;

    public void Display()
    {

        canProceed = null;
    }

    public void UserHasConfirmed(bool value)
    {
        canProceed = value;
    }

    public void Hide()
    {
        canProceed = null;

    }


}
