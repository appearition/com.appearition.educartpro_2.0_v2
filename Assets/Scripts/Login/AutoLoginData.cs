﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AutoLoginData : MonoBehaviour
{
    public bool isAutoLoginEnabled = true;

    //TODO : Write Custom inspector to hide data
    public bool isLoginSuccess = false;
    public bool isTenantSelectionSuccess = false;


    public UnityEvent loginProcessCompleteEvent;


    private void OnDisable()
    {
        //Reset();
    }

    private void Reset()
    {
        isLoginSuccess = false;
        isTenantSelectionSuccess = false;
    }
}
