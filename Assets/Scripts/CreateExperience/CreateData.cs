﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition.ArTargetImageAndMedia;
using Appearition.Common;

public class MediaToUpload
{
    public int currentIndex;
    public string mediaType;
    public string path;
    public bool isDeleted;
}

public class MarkerToUpload
{
    public bool isDeleted;
    public string path;
}

public class CreateData : MonoBehaviour
{
    public string expName;

    public bool isMarker;

    public bool isPublic;

    public MarkerToUpload markerToUpload = new MarkerToUpload();

    public MediaToUpload mediaToUpload = new MediaToUpload();

    public ArTarget currentTarget;
}
