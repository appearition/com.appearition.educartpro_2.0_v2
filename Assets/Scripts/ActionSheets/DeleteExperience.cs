﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Appearition.ArTargetImageAndMedia;
using UnityEngine;


public class DeleteExperience : MonoBehaviour
{
    public DeleteExpConfirmationPopup decPopup;
    public SetupMyProfile sumProfile;
    public GameObject actionSheet;
    public GameObject deletesuccess;
    Coroutine _mainProcess;

    /// <summary>
    /// To be called after a confirmation popup window. Tries to delete the given experience. Make sure the popup panel blocks all inputs.
    /// </summary>
    /// <param name="experience"></param>
    public void DeleteUserExperience()
    {
        if (_mainProcess != null)
        {
            StopCoroutine(_mainProcess);
            _mainProcess = null;
        }
        if (_mainProcess == null)
            _mainProcess = StartCoroutine(DeleteExperienceProcess(GetComponent<UserActionSheetData>().asset));
    }

    IEnumerator DeleteExperienceProcess(ArTarget experience)
    {
        decPopup.Display();
        while (!decPopup.canProceed.HasValue)
        {
            yield return null;
        }

        if (decPopup.canProceed == true)
        {
            decPopup.Hide();
            bool isDeletedProperly = false;

            yield return ArTargetHandler.DeleteExperienceProcess(experience, null, null, b => isDeletedProperly = b);

            if (isDeletedProperly)
            {

                //Success Here
                actionSheet.SetActive(false);
                sumProfile.RefreshUserData();
                //deletesuccess.gameObject.SetActive(true);
                yield return new WaitForSeconds(0f);
            }
            else
            {

                //Failure Here
                yield return new WaitForSeconds(1f);
            }

        }
        sumProfile.RefreshUserData();
        _mainProcess = null;
    }

}
