﻿using UnityEngine;
using UnityEngine.Android;


[RequireComponent(typeof(AutoLoginData))]
public class SplashScreenPanelTransition : MonoBehaviour
{

    public GameObject loginPanel;
    public GameObject mainMenu;
    public GameObject tenantSelection;
    public GameObject onboarding;
    public GameObject termAndConditions;

    AutoLoginData aLoginData;
    private void Start()
    {
        if(!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
        }
        if(!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageRead);
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        }
        aLoginData = GetComponent<AutoLoginData>();
        if (!aLoginData.isAutoLoginEnabled)
        {
            Invoke("TransitionUI", 2f);
        }

    }

    public void TermsAndConditionsAccepted()
    {
        PlayerPrefs.SetInt("TermsAndConditions", 1);
        termAndConditions.SetActive(false);
    }

    //TODO : Change to use UnityEvents

    public void TransitionUI()
    {
        if (!PlayerPrefs.HasKey("TermsAndConditions"))
        {
            termAndConditions.SetActive(true);
        }
        Debug.Log("Transition");
        if (!aLoginData.isLoginSuccess)
        {
            loginPanel.SetActive(true);
        }
        else
        {
            if (!aLoginData.isTenantSelectionSuccess)
            {
                tenantSelection.GetComponent<HandleTenantSelection>().Setup();
            }
            else
            {
                mainMenu.SetActive(true);
            }
        }
        if (!UserPrefs.Instance.SkipOnboarding)
        {
            onboarding.SetActive(true);
            UserPrefs.Instance.SkipOnboarding = true;
        }
        gameObject.GetComponent<Canvas>().enabled = false;
    }


}

