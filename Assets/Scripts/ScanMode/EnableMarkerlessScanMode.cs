﻿using Appearition.Example;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableMarkerlessScanMode : MonoBehaviour
{
    public GameObject MarkerlessObject;
    public GameObject closePanel;
    public void SwitchToMarkerless()
    {
        if (GetComponent<UserExperienceCardData>() != null)
        {
            MarkerlessObject.GetComponent<MarkerlessData>().markerLessAsset = GetComponent<UserExperienceCardData>().data;
        }
        else
        {
            MarkerlessObject.GetComponent<MarkerlessData>().markerLessAsset = GetComponent<ExperienceCardData>().data;
        }
        MarkerlessObject.SetActive(true);
        closePanel.SetActive(false);
    }
}
