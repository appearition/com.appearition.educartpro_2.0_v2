﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DeletePlayerPrefs : MonoBehaviour
{
    [MenuItem("EditorDebug/DeletePrefs")]
    public static void DeletePrefs()
    {
        PlayerPrefs.DeleteAll();
    }    
}
