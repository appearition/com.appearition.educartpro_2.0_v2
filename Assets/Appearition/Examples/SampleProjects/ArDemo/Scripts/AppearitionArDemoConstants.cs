﻿using System;
using System.Reflection;
using System.Linq;
using UnityEngine;

namespace Appearition.Example
{
    /// <summary>
    /// Contains all the settings for the ArDemo Example.
    /// </summary>
    public static class AppearitionArDemoConstants 
    {
        public const string ARPREFAB_FOLDER_NAME_IN_RESOURCES = "ArPrefabs";
        public const bool CLEAR_TRACKER_ON_TARGET_LOST = false;
        public const bool ENABLE_EXTENDED_TRACKING = false;
        public const bool DISABLE_TRACKING_ON_TARGET_FOUND = true;
        public const bool USING_CLOUD_RECO = true;
        public const float ASSETBUNDLE_SCALE_MULTIPLIER = 0.125f;

        /// <summary>
        /// Fetches the name of the ArMedia handling class using the MediaType.
        /// </summary>
        /// <param name="mediaType"></param>
        /// <returns></returns>
        public static Type GetArMediaClassFromMediaType(string mediaType)
        {
            if (string.IsNullOrEmpty(mediaType))
                return null;

            string typeName = string.Format("{0}{1}ArMedia", char.ToUpper(mediaType[0]).ToString(), mediaType.Substring(1)).Trim();

            typeName = typeName.Replace('-','_');
            
            //Punch the assembly and get the type regardless of its namespace.
            return (from tmpType in Assembly.GetExecutingAssembly().GetTypes()
                    where tmpType.Name.Equals(typeName)
                    select tmpType).FirstOrDefault();
        }
        
        /// <summary>
        /// Fetches a placeholder sprite which can be used while waiting for an image to load.
        /// </summary>
        /// <returns></returns>
        public static Sprite GetImagePlaceholder ()
        {
           //return Resources.Load("ArDemo_Placeholder", typeof(Sprite)) as Sprite;
           return Resources.Load("Transparent_Empty", typeof(Sprite)) as Sprite;
        }

        public static Sprite GetTargetImagePlaceholder()
        {
            return Resources.Load("EducART_MarkerlessTargetImage", typeof(Sprite)) as Sprite;
        }
    }
}