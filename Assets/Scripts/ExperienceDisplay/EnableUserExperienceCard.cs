﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition.ArTargetImageAndMedia;

public class EnableUserExperienceCard : MonoBehaviour
{
    public GameObject ActionSheetMarker;
    public GameObject ActionSheetMarkerless;

    ArTarget data;

    public void Start()
    {
        data = GetComponent<UserExperienceCardData>().data;
    }
    public void OnKebabMenuPressed()
    {

        if (data.IsMarkerless)
        {

            ActionSheetMarker.SetActive(false);
            ActionSheetMarkerless.SetActive(true);
            ActionSheetMarkerless.GetComponent<UserActionSheetData>().asset = data;
        }
        else
        {

            ActionSheetMarker.SetActive(true);
            ActionSheetMarkerless.SetActive(false);
            ActionSheetMarker.GetComponent<UserActionSheetData>().asset = data;
        }
    }

}
