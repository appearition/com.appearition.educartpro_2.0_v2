﻿using Appearition;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PostTenantSelectionTransition : MonoBehaviour
{
    public GameObject HomePanel;
    public void GoToNextPanel()
    {
        HomePanel.SetActive(true);
        GetComponent<Canvas>().enabled = false;
        GetComponent<TenantListDisplay>().enabled = false;
    }

}
