﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Assets.GifAssets.PowerGif;
using SimpleGif.Enums;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.GifAssets.GifReplay
{
	/// <summary>
	/// Recorder class. Captures a frame sequence and encodes it to GIF.
	/// </summary>
    public class Recorder : MonoBehaviour
	{
		[Header("Input")]
		public Camera Camera;
		public Canvas Canvas;
		public Button gifButton;
		public GameObject previewGameobject;

		[Header("Settings")]
		public int Width = 320;
	    public int Height = 240;
		public int FramesPerSecond = 10;
		public float MaxDuration = 2;
		public bool CaptureUI;
		public MasterPalette MasterPalette = MasterPalette.DontApply; // https://en.wikipedia.org/wiki/List_of_software_palettes#RGB_arrangements

		[Header("Optional")]
		public Text Status;
		public Image Progress;
		public AnimatedImage AnimatedImage;

		private float _duration;
	    private readonly List<Texture2D> _textures = new List<Texture2D>();

		bool isRecording = false;
		public GameObject recordingButton;
		public GameObject startRecordingButton;
		public Image recordDurationSlider;

		public byte[] Binary { get; private set; }


		public void ToggleRecordingState()
        {
			if(isRecording)
            {
				EncodeToGif();
				startRecordingButton.SetActive(true);
				recordingButton.SetActive(false);
            }
			else
            {
				StartRecording();
				startRecordingButton.SetActive(false) ;
				recordingButton.SetActive(true);
			}

			isRecording = !isRecording;
		}

        public void OnDisable()
        {
			StopRecording();
			startRecordingButton.SetActive(true);
			recordingButton.SetActive(false);
			_textures.Clear();

		}

        public void OnEnable()
        {
			Camera = GetActiveCamera.currentCamera;
        }
        /// <summary>
        /// Start recording.
        /// </summary>
        public void StartRecording()
	    {
            if (CaptureUI)
            {
                Canvas.renderMode = RenderMode.ScreenSpaceCamera;
                Canvas.worldCamera = Camera;
            }
            else
            {
                Canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            }
			SetRecordProgress(0, Color.white);
			StartCoroutine(Record());
	    }

		/// <summary>
		/// Stop recording.
		/// </summary>
	    public void StopRecording()
	    {
		    SetStatus("Stopped: {0} frames, {1:N1} secodns", _textures.Count, _duration);
		    SetRecordProgress(1, Color.white);
			StopAllCoroutines();
	    }

		/// <summary>
		/// Encode a recorded sequence to GIF.
		/// </summary>
		public void EncodeToGif()
		{
			StopRecording();
			StartCoroutine(Encode());
		}

		/// <summary>
		/// Save GIF to file.
		/// </summary>
		public void Save()
		{
			if (Binary == null) throw new Exception("Binary == null");

			string path;

			#if UNITY_EDITOR

			path = UnityEditor.EditorUtility.SaveFilePanel("Save GIF", "", "Example" + System.DateTime.Now.ToString("ddmmyyhhss"), "gif");

			if (path != null)
			{
				System.IO.File.WriteAllBytes(path, Binary);
			}

#else
			
			path = Application.persistentDataPath + "/Example.gif";
			//System.IO.File.WriteAllBytes(path, Binary);
			NativeGallery.SaveImageToGallery(Binary, "EducartPro", "EducartGIF_" + System.DateTime.Now.ToString("ddMMyyyyHHmmss") + ".gif", error => { UnityEngine.Debug.LogError("Gif Save Failed");});
			RefreshMedia(path);
			

#endif
			SetStatus("Saved to {0}", path);
			previewGameobject.SetActive(false);
		}

		/// <summary>
		/// Share GIF with native OS dialog (Android and iOS only).
		/// </summary>
		public void Share()
		{
			if (Binary == null) throw new Exception("Binary == null");

			#if UNITY_ANDROID || UNITY_IOS

			var path = System.IO.Path.Combine(Application.temporaryCachePath, "Replay.gif");

			System.IO.File.Delete(path);
			System.IO.File.WriteAllBytes(path, Binary);
			new NativeShare().SetSubject("Replay").SetTitle("Sharing").SetText("EducartPro").AddFile(path, "image/gif").Share();

			#else

			throw new Exception("Not supported on this platform. Only Android and iOS are supported.");

			#endif
		}

		/// <summary>
		/// Write a review on the Asset Store.
		/// </summary>
		public void Review()
		{
			Application.OpenURL("https://assetstore.unity.com/packages/slug/122757");
		}

		/// <summary>
		/// Recording coroutine.
		/// </summary>
		private IEnumerator Record()
		{
			_duration = 0;
			_textures.Clear();

			var startTime = Time.time;
			var interval = 1f / FramesPerSecond;

			do
			{
				_duration += interval;
				Capture();
				SetStatus("Recording: {0} frames, {1:N1} secodns", _textures.Count, Time.time - startTime);
				SetRecordProgress(_duration / MaxDuration, Color.white);
				UnityEngine.Debug.LogError(_duration + "\t" + MaxDuration);
				yield return new WaitForSeconds(interval);
			}
			while (_duration <= MaxDuration);

			StopRecording();
			ToggleRecordingState();
		}

		/// <summary>
		/// Capture screenshot.
		/// </summary>
		private void Capture()
		{
			var renderTexture = new RenderTexture(Width, Height, 16);
			var texture = new Texture2D(Width, Height, TextureFormat.ARGB32, false);

			Camera.targetTexture = renderTexture;
			Camera.Render();
			RenderTexture.active = renderTexture;
			texture.ReadPixels(new Rect(0, 0, Width, Height), 0, 0); // TODO: Change Rect if you want crop the image
			Camera.targetTexture = null;
			RenderTexture.active = null;
			Destroy(renderTexture);
			texture.Apply();
			_textures.Add(texture);
		}

		/// <summary>
		/// Encoding coroutine.
		/// </summary>
		private IEnumerator Encode()
		{
			if (_textures.Count == 0) throw new Exception("_textures.Count == 0");

			var stopwatch = Stopwatch.StartNew();
			var frames = _textures.Select(i => new GifFrame(i, 1f / FramesPerSecond)).ToList();
			var gif = new Gif(frames);
			var encodeProgress = new SimpleGif.Data.EncodeProgress { FrameCount = frames.Count };
			
			SetStatus("Encoding using {0} threads...", SystemInfo.processorCount);
			
			yield return null;

			gif.EncodeParallel(progress => { encodeProgress = progress; }, MasterPalette); // Threads are started here.

			while (!encodeProgress.Completed)
			{
				UnityEngine.Debug.Log("Encoding");
				SetStatus("Encoding: {0}/{1}", encodeProgress.Progress, encodeProgress.FrameCount);
				SetProgress((float) encodeProgress.Progress / encodeProgress.FrameCount);

				yield return null;
			}

			if (encodeProgress.Exception != null)
			{
				UnityEngine.Debug.Log(encodeProgress.Exception.Message);
				SetStatus(encodeProgress.Exception.Message);
				Progress.color = new Color(1, 0, 0);

				yield break;
			}

			Binary = encodeProgress.Bytes;

			SetStatus("Encoding completed in {0:N1} seconds, GIF size is {1:N1} kB, resolution is {2}x{3} px, frame rate is {4}",
				stopwatch.Elapsed.TotalSeconds, encodeProgress.Bytes.Length / 1024f, Width, Height, FramesPerSecond);
			SetProgress(1, Color.green);

			if (AnimatedImage != null)
			{
				previewGameobject.SetActive(true);
				AnimatedImage.Play(gif);
				
			}
		}

		private void SetStatus(string pattern, params object[] args)
		{
			if (Status == null) return;

			Status.text = string.Format(pattern, args);
		}

		private void SetProgress(float value, Color? color = null)
		{
			if (Progress == null) return;

			Progress.fillAmount = value;

			if (color != null)
			{
				Progress.color = color.Value;
			}
		}
		
		
		private void SetRecordProgress(float value, Color? color = null)
		{
			if (Progress == null) return;

			recordDurationSlider.fillAmount = value;

			if (color != null)
			{
				recordDurationSlider.color = color.Value;
			}
		}

		/// <summary>
		/// Refresh android media to get new files available.
		/// </summary>
		private static void RefreshMedia(string path)
		{
			#if UNITY_ANDROID && !UNITY_EDITOR

			using (var player = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
			using (var activity = player.GetStatic<AndroidJavaObject>("currentActivity"))
			using (var context = activity.Call<AndroidJavaObject>("getApplicationContext"))
			using (var scanner = new AndroidJavaClass("android.media.MediaScannerConnection"))
			using (var environment = new AndroidJavaClass("android.os.Environment"))
			using (environment.CallStatic<AndroidJavaObject>("getExternalStorageDirectory"))
			{
				scanner.CallStatic("scanFile", context, new[] { path }, null, null);
			}

			#endif
		}
	}
}