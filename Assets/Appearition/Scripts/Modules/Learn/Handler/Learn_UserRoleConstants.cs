﻿namespace Appearition
{
    public static partial class UserRoleConstants
    {
        public const string LEARN_VIEWER = "LearnViewer";
        public const string LEARN_EDITOR = "LearnEditor";
        public const string LEARN_TRACKING_SUBMITTER = "LearnTrackingSubmitter";
        public const string LEARN_TRACKING_VIEWER_EVERYONE = "LearnTrackingViewerEveryone";
        public const string LEARN_TRACKING_VIEWER_MINE_ONLY = "LearnTrackingViewerMineOnly";

        //Handy Properties
        public static bool HasPermissionToViewLearnContent => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(LEARN_VIEWER);
        public static bool HasPermissionToEditLearnContent => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(LEARN_EDITOR);
        public static bool HasPermissionToSubmitLearnTracking => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(LEARN_TRACKING_SUBMITTER);
        public static bool HasPermissionToViewAllLearnTracking => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(LEARN_TRACKING_VIEWER_EVERYONE);
        public static bool HasPermissionToViewMyLearnTracking => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(LEARN_TRACKING_VIEWER_MINE_ONLY);
    }
}