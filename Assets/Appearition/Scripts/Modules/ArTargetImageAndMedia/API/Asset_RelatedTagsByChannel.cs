﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: Asset_MediaByAsset.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using System.Collections.Generic;
using Appearition.API;

namespace Appearition.ArTargetImageAndMedia.API
{
    /// <summary>
    /// https://api.appearition.com/TenantName/api/Asset/RelatedTagsByChannel/0?tag={1} , where 0 is Channel ID and 1 is the given tag.
    /// </summary>
    [System.Serializable]
    public class Asset_RelatedTagsByChannel : BaseApiGet
    {
        public override int ApiVersion
        {
            get { return 2; }
        }

        public List<string> Data;

        //Request Params
        [System.Serializable]
        public class RequestContent : BaseRequestContent
        {
            public string tag;
        }
    }
}