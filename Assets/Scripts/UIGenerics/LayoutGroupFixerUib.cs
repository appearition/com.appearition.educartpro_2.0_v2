﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LayoutGroupFixerUib : MonoBehaviour
{
    void OnEnable()
    { 
        StartCoroutine(Refresh());
    }
    IEnumerator Refresh()
    {
        var lg = gameObject.GetComponent<LayoutGroup>();
        yield return new WaitForSeconds(0.01f);
        lg.enabled = false;
        lg.enabled = true;
    }
    
    
}
