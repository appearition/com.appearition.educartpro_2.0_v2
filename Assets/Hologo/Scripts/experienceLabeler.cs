﻿using System.Collections.Generic;
using UnityEngine;


namespace Hologo2
{
    /// <summary>
    /// Created by: Hamid (hamidibrahim@gmail.com) - 17 july 2019
    /// Modified by: Shariz - 20th September 2020
    /// Experience label generator
    /// This generate the labels for the experience depending on the data
    /// </summary>
    public class experienceLabeler : MonoBehaviour
    {
        // list of labels
        public List<labelclass> MyLabels;
        // label line width
        public float LineWidth = 1;
        // label line color
        public Color LineColor = Color.white;
        // hiding these public fields since they will be populated at run time by ARExperienceMainScript
        [HideInInspector]
        public bool ShowLables = true;
        // parent transform where all label gameobjects will be parented under
        [HideInInspector]
        public Transform LabelParent;
        // main camera
        [HideInInspector]
        public Camera MyCamera;
        // line renderer material
        [HideInInspector]
        public Material LineMaterial;



        // list to store the instantiated line renderers
        List<LineRenderer> lineList;
        // start updating the labels in update method
        bool startUpdating = false;
        // bool for firing each labels haptic method once
        List<bool> isHapticList;


        // Show labels and draw lines
        void Update()
        {
            if (startUpdating)
            {
                if (ShowLables)
                {
                    showlabelsAlways();
                    DrawLines();
                }

            }
        }



        // for showing labels always 
        void showlabelsAlways()
        {
            if (MyLabels.Count > 0)
            {
                for (int i = 0; i < MyLabels.Count; i++)
                {
                    showLabel(MyLabels[i], i);
                }
            }
        }

        // turn on labels
        void showLabel(labelclass mylabel, int i)
        {
            mylabel.LabelsActiveState(true);
            showLine(i);
        }




        // intialize labels 
        public void InitMe(GameObject originPrefab, List<GameObject> labels, Camera camera, Transform labelsParent, Transform scaleParent, Material material)
        {
            // set camera
            MyCamera = camera;
            // set line material
            LineMaterial = material;
            // set label parent
            LabelParent = labelsParent;
            // set labels
            setLabels(labels);
            // set origin prefabs
            setOriginPrefabs(originPrefab);

            // assign a camera
            AssignMeACamera();

            // set labels as children of label parent
            ChangeMyParent();

            // make list for lines for labels
            MakeMyLines();

            // draw lines for labels
            DrawLines();


            startUpdating = true;
            isHapticList = new List<bool>();

            // hide after generating everything
            if (MyLabels.Count > 0)
            {
                for (int i = 0; i < MyLabels.Count; i++)
                {
                    MyLabels[i].LabelsActiveState(false);
                    isHapticList.Add(false);
                }
            }
        }

        // assiging the instantiated labels to label points and
        void setLabels(List<GameObject> labels)
        {
            for (int i = 0; i < MyLabels.Count; i++)
            {
                // get label element component for each label set label point, transform position and active/inactive for labels
                labelElement le = labels[i].GetComponent<labelElement>();
                MyLabels[i].LabelPoint = labels[i];
                MyLabels[i].LabelPoint.transform.position = MyLabels[i].Target.transform.position;
                MyLabels[i].isNeverShown = le.isLabelActive();
                if (!le.isLabelActive())
                    MyLabels[i].hideLabel();

            }
        }

        // instantiate the label origin and set the parent, position and scale for it
        void setOriginPrefabs(GameObject originPrefab)
        {
            for (int i = 0; i < MyLabels.Count; i++)
            {
                GameObject go = Instantiate(originPrefab) as GameObject;
                go.transform.SetParent(MyLabels[i].Origin.transform);
                go.transform.localPosition = Vector3.zero;
                go.transform.localScale = Vector3.one;
                go.transform.rotation = Quaternion.Euler(90f, 0, 0);
            }
        }


        // make a list of line renderers, a line for each label present;
        void MakeMyLines()
        {
            lineList = new List<LineRenderer>();
            if (MyLabels.Count > 0)
            {
                for (int i = 0; i < MyLabels.Count; i++)
                {
                    lineList.Add(createALine(MyLabels[i].Origin)); 
                }
            }
        }




        // drawing line between label origin and label point.
        void DrawLines()
        {
            if (MyLabels.Count > 0)
            {
                for (int i = 0; i < MyLabels.Count; i++)
                {
                    lineList[i].SetPosition(0, MyLabels[i].Origin.transform.position);
                    lineList[i].SetPosition(1, MyLabels[i].LabelPoint.transform.position);
                    lineList[i].widthCurve =new AnimationCurve(new Keyframe(0,LineWidth/5f), new Keyframe(1,LineWidth));
                }
            }
        }


        // show label line
        void showLine(int id)
        {
            lineList[id].enabled = true;
        }
        
        // creating one line renderer and returning it
        LineRenderer createALine(GameObject go)
        {
            // Add a Line Renderer to the GameObject
            LineRenderer line = go.AddComponent<LineRenderer>();
            // Set the width of the Line Renderer
            if(Camera.allCameras.Length > 1)
            {
                LineWidth = 0.001f;
            }
            else
            {

                LineWidth = 4f;
            }
            line.startWidth = LineWidth;
            line.endWidth = LineWidth;
            line.positionCount = 2;
            line.startColor = LineColor;
            line.material = LineMaterial;
            return line;
        }


        // assigning camera to lables world space canvas event camera
        void AssignMeACamera()
        {
            if (MyLabels.Count > 0)
            {
                for (int i = 0; i < MyLabels.Count; i++)
                {
                    Canvas canvas = MyLabels[i].LabelPoint.GetComponentInChildren<Canvas>();
                    canvas.worldCamera = MyCamera;
                }
            }
        }

        // changing the parent of the label so it can follow the target
        void ChangeMyParent()
        {
            if (MyLabels.Count > 0)
            {
                for (int i = 0; i < MyLabels.Count; i++)
                {
                  
                        MyLabels[i].LabelPoint.transform.SetParent(LabelParent);
               
                }
            }
        }


        // Enable and disable Label
        public void MyLabelsEnable(bool enable)
        {
            if (MyLabels.Count > 0)
            {
                for (int i = 0; i < MyLabels.Count; i++)
                {
                    MyLabels[i].LabelsActiveState(enable);
                    lineList[i].gameObject.SetActive(enable);

                    if(MyLabels[i].isNeverShown == false){
                        MyLabels[i].LabelsActiveState(false);
                        lineList[i].gameObject.SetActive(false);
                    }
                }
            }
            ShowLables = enable;
            

        }

        // Set label objects to inactive on disable
        private void OnDisable()
        {
            if (MyLabels.Count > 0)
            {
                for (int i = 0; i < MyLabels.Count; i++)
                {
                    MyLabels[i].LabelsActiveState(false);
                }
            }
        }
    }

    // struct to contain a label and its origin and target to follow gameobjects
    [System.Serializable]
    public class labelclass
    {
        public string LabelName;
        // the point where the origin of the label is . ie part of the model
        public GameObject Origin;
        // the target that the lable point should follow
        public GameObject Target;
        // label point where the line renderer connects 
        public GameObject LabelPoint;
        // is omini
        public bool OmniDirectional = true;

        // bool for toggling labels on and off
        public bool showLabel = true;
        [HideInInspector]
        // bool for not showing the label .. for localization purpose
        public bool isNeverShown = false;


        // show hide label gameobjects
        public void LabelsActiveState(bool enable)
        {
            if (showLabel)
            {
                LabelPoint.SetActive(enable);
            }
        }

        // never show label
        public void labelIsNeverShown(bool state)
        {
            isNeverShown = state;
        }

        // hide label
        public void hideLabel()
        {
            Origin.SetActive(false);
            Target.SetActive(false);
            LabelPoint.SetActive(false);
            showLabel = false;
            
        }
    }
}