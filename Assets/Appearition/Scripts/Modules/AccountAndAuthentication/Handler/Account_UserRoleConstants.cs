﻿namespace Appearition
{
    public static partial class UserRoleConstants
    {
        public const string ACCOUNT_PASSWORD_RESET = "AccountPasswordReset";
        public const string ACCOUNT_REGISTRATION = "AccountRegistration";

        public static bool HasPermissionToResetPassword => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(ACCOUNT_PASSWORD_RESET);
        public static bool HasPermissionToRegister => AppearitionGate.Instance.CurrentUser.ContainsRoleForSelectedTenant(ACCOUNT_REGISTRATION);
    }
}