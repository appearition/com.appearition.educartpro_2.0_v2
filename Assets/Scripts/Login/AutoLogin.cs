﻿using System.Collections;
using System.Linq;
using Appearition.AccountAndAuthentication;
using Appearition.Common;
using UnityEngine;
using Appearition;
using System;

[RequireComponent(typeof(AutoLoginData))]
public class AutoLogin : MonoBehaviour
{

    AutoLoginData autoLoginData;

    private void Start()
    {
        autoLoginData = GetComponent<AutoLoginData>();
        if (autoLoginData.isAutoLoginEnabled)
        {
            StartCoroutine(ValidateUser());
        }
    }

    IEnumerator ValidateUser()
    {
        //Wait for initialization
        yield return new WaitForSeconds(1.0f);
        //Handle login pickup
        string tenantKey = "";
        if (PlayerPrefs.HasKey(AppearitionConstants.PROFILE_PREFERRED_TENANT))
            tenantKey = PlayerPrefs.GetString(AppearitionConstants.PROFILE_PREFERRED_TENANT);
        Debug.Log(tenantKey);
        yield return AutoLoginProcess(true, tenantKey, isSuccess =>
        {
            if(isSuccess)
            {
                //Get AR Provider credentials
            }
            autoLoginData.loginProcessCompleteEvent.Invoke();
        });


    }

    #region SDK Handling
    public IEnumerator AutoLoginProcess(bool allowTenantSelection, string targetTenantKey, Action<bool> isAutoLoggingSuccess)
    {
        bool isProcessSuccess = false;

        if (PlayerPrefs.HasKey(AppearitionConstants.PROFILE_SESSION_TOKEN_NAME))
        {
            string token = PlayerPrefs.GetString(AppearitionConstants.PROFILE_SESSION_TOKEN_NAME);
            AppearitionLogger.LogInfo("Existing token : " + token);
            AppearitionGate.Instance.CurrentUser.AddOrModifyAuthenticationToken(AppearitionConstants.PROFILE_SESSION_TOKEN_NAME, token);

            bool isSuccess = false;
            yield return AccountHandler.GetProfileDataProcess(onComplete: success => isSuccess = success);

            //All good?
            if (isSuccess)
            {
                autoLoginData.isLoginSuccess = true;
                AppearitionLogger.LogInfo("Successfully loaded valid existing user token. Loading content.");
                bool isCurrentKeyValid = false;

                //Set main tenant. If multiple, check for entered entries. Otherwise, display tenant selection.
                //if (AppearitionGate.Instance.CurrentUser.allTenantsAvailable.Count > 1)
                //{
                if (!string.IsNullOrEmpty(targetTenantKey))
                {
                    if (AppearitionGate.Instance.CurrentUser.allTenantsAvailable.Any(o => o.tenantKey.Equals(targetTenantKey)))
                    {
                        AppearitionGate.Instance.CurrentUser.selectedTenant = targetTenantKey;
                        isCurrentKeyValid = true;
                    }
                    else if (PlayerPrefs.HasKey(AppearitionConstants.PROFILE_PREFERRED_TENANT) &&
                             PlayerPrefs.GetString(AppearitionConstants.PROFILE_PREFERRED_TENANT).Equals(targetTenantKey))
                    {
                        //Delete the key.
                        AppearitionLogger.LogInfo("Tenant key found but the user doesn't own it anymore. Deleting it locally.");
                        PlayerPrefs.DeleteKey(AppearitionConstants.PROFILE_PREFERRED_TENANT);
                    }
                }

                //Check if the tenant key was found and loaded properly.
                //}
                //1 tenant? All good
                //else
                //   isCurrentKeyValid = true;

                if (isCurrentKeyValid)
                {
                    //Set the user main channel
                    SetUserChannelAsCurrentProcess setUserChannelProcess = new SetUserChannelAsCurrentProcess();
                    yield return setUserChannelProcess.ExecuteMainProcess();

                    //Get the AR Provider credentials
                    Debug.LogError("Fetching");
                    yield return VuforiaLicenseFetcherB.FetchVuforiaLicense();
                    yield return MediaTypeHandler.GetActiveMediaTypesProcess(null, true);
                    autoLoginData.isTenantSelectionSuccess = true;
                    isProcessSuccess = true;
                }
            }
            else
            {
                AppearitionLogger.LogInfo("User token has been expired. Switching to login.");

                //Cleanup and proceed
                AppearitionGate.Instance.CurrentUser.RemoveTokenByNameIfExisting(AppearitionConstants.PROFILE_SESSION_TOKEN_NAME);
                PlayerPrefs.DeleteKey(AppearitionConstants.PROFILE_SESSION_TOKEN_NAME);
            }
        }

        isAutoLoggingSuccess?.Invoke(isProcessSuccess);
    }
    #endregion
}
