﻿using UnityEngine;

namespace Assets.GifAssets.GifReplay
{
	/// <summary>
	/// Sample object for creating GIF animation.
	/// </summary>
	public class Cube : MonoBehaviour
	{
		public MeshRenderer Renderer;

		/// <summary>
		/// Rotates the cube and changes its' color.
		/// </summary>
		public void Update()
		{
			transform.Rotate(1, 1, 1);

			float h, s, v;

			Color.RGBToHSV(Renderer.material.color, out h, out s, out v);

			Renderer.material.color = Color.HSVToRGB(h + Time.deltaTime / 10f, 1, 1);
		}
	}
}