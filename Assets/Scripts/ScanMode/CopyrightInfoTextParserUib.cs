﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CopyrightInfoTextParserUib : MonoBehaviour
{
    //References
    [SerializeField] Text _displayText;
    GraphicRaycaster _raycaster;

    //Internal Variables
    int _h1FontSizeGain = 5;
    int _h2FontSizeGain = 4;
    int _h3FontSizeGain = 3;
    int _h4FontSizeGain = 2;
    int _h5FontSizeGain = 1;
    public Color hyperlinkColor = Color.blue;


    void Start()
    {
        _raycaster = GetComponentInParent<GraphicRaycaster>();
        //DisplayHtmlText(debugHtmlText);
    }

    public void DisplayHtmlText(string text)
    {
        string outputText = "";

        var htmlDoc = new HtmlDocument();
        htmlDoc.LoadHtml(text);

        foreach (var tmpNode in htmlDoc.DocumentNode.ChildNodes)
        {
            //Debug.Log($"{tmpNode.Name} - {tmpNode.InnerText} - {tmpNode.HasChildNodes}");
            outputText += HandleHtmlNode(tmpNode);
        }

        _displayText.text = outputText;
    }

    string HandleHtmlNode(HtmlNode tmpNode)
    {
        switch (tmpNode.Name)
        {
            //Headers
            case "h1":
                return $"<size={_displayText.fontSize + _h1FontSizeGain}>{tmpNode.InnerText}</size>";
            case "h2":
                return $"<size={_displayText.fontSize + _h2FontSizeGain}>{tmpNode.InnerText}</size>";
            case "h3":
                return $"<size={_displayText.fontSize + _h3FontSizeGain}>{tmpNode.InnerText}</size>";
            case "h4":
                return $"<size={_displayText.fontSize + _h4FontSizeGain}>{tmpNode.InnerText}</size>";
            case "h5":
                return $"<size={_displayText.fontSize + _h5FontSizeGain}>{tmpNode.InnerText}</size>";


            //Handle text stuff
            case "#text":
            case "a":
                if (HasHyperlink(tmpNode.InnerText))
                {
                    string[] split = tmpNode.InnerText.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

                    string tmpText = "";
                    for (int i = 0; i < split.Length; i++)
                    {
                        if (HasHyperlink(split[i]))
                            tmpText += $"<color=#{ColorUtility.ToHtmlStringRGB(hyperlinkColor)}>{split[i]}</color>";
                        else
                            tmpText += split[i];
                        if (i + 1 < split.Length)
                            tmpText += " ";
                    }

                    return tmpText.Trim();
                }

                return $"{tmpNode.InnerText.Trim()}";
            
            case "i":
                return $"<i>{tmpNode.InnerText}</i>";
            case "b":
                return $"<b>{tmpNode.InnerText}</b>";

            //Handle main keywords
            case "hr":
                return "\n--------------------------------------\n";
            case "br":
                return "\n";


            //Ignore unrecognized nodes.
            default:
                return "";
        }
    }

    public void OnPointerClick(BaseEventData data)
    {
        var pointerData = data as PointerEventData;

        if (pointerData == null)
            return;

        //Raycast using click event
        List<RaycastResult> results = new List<RaycastResult>();
        _raycaster.Raycast(pointerData, results);

        RaycastResult textResult = default;
        for (int i = 0; i < results.Count; i++)
        {
            if (results[i].gameObject == _displayText.gameObject)
            {
                textResult = results[i];
                break;
            }
        }

        if (textResult.gameObject == null)
            return;

        //Get text-space position
        if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(_displayText.rectTransform, textResult.screenPosition, pointerData.enterEventCamera,
            out Vector2 textSpacePos))
            return;

        //Try to find what text was clicked..
        var lines = _displayText.cachedTextGenerator.GetLinesArray();
        int textStartIndex = -1;
        int nextLineStartIndex = -1;

        for (int i = 0; i < lines.Length; i++)
        {
            if (lines[i].topY - lines[i].height <= textSpacePos.y)
            {
                textStartIndex = lines[i].startCharIdx;

                if (i + 1 < lines.Length)
                    nextLineStartIndex = lines[i + 1].startCharIdx;
                break;
            }
        }

        //Get the word at said index
        if (textStartIndex >= 0 && textStartIndex < _displayText.text.Length)
        {
            //Get the whole sentence
            string sentence = "";
            string word = "";

            if (nextLineStartIndex == -1)
                nextLineStartIndex = _displayText.text.Length;

            for (int i = textStartIndex; i < nextLineStartIndex; i++)
            {
                sentence += _displayText.text[i];
            }

            //Split it and try to find a URL
            string[] split = sentence.Split(new[] {' '}, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < split.Length; i++)
            {
                if (HasHyperlink(split[i]))
                {
                    word = split[i];
                    break;
                }
            }

            //Check if it's a hyperlink. If so, open it up!
            if (!string.IsNullOrEmpty(word))
            {
                string plainText = Regex.Replace(word.Trim(), "<.*?>", string.Empty);

                if (plainText.IndexOf("http", StringComparison.InvariantCultureIgnoreCase) >= 0)
                    plainText = $"https://{plainText}";

                Debug.LogError("Clicking on copyright URL " + plainText);
                Application.OpenURL(plainText);
            }
        }
    }

    #region Utilities 

    bool HasHyperlink(string text)
    {
        return text.IndexOf("http", StringComparison.InvariantCultureIgnoreCase) >= 0 ||
               text.IndexOf("www.", StringComparison.InvariantCultureIgnoreCase) >= 0;
    }

    #endregion
}