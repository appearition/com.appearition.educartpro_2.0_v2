﻿using UnityEngine;
using Appearition;

public class OpenMyProfilePanel : MonoBehaviour
{
    public GameObject myProfilePanel;
    public GameObject loginPrompt;
    public void SwitchToMyProfilePanel()
    {
        if (AppearitionGate.Instance.CurrentUser.IsUserLoggedIn)
        {
            myProfilePanel.SetActive(true);
            gameObject.SetActive(false);
        }
        else
        {
            loginPrompt.SetActive(true);
        }
    }

}
