﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Appearition;

public class EditPanelInputHandling : MonoBehaviour
{
    public EditData editData;
    public InputField nameHeader;
    public Button publishButton;
    public Button saveButton;

    public void OnEnable()
    {
        nameHeader.Select();
        nameHeader.ActivateInputField();
    }
    public void EditExperienceName()
    {
        nameHeader.interactable = true;
        nameHeader.Select();
    }

    public void OnEditExperienceNameDone()
    {
        //nameHeader.interactable = false;
        editData.expName = nameHeader.text;
        if (nameHeader.text != "Untitled Experience" || nameHeader.text != null)
        {
            publishButton.interactable = true; 
            if (UserRoleConstants.HasPermissionToPublishExperiences)
            {
                publishButton.interactable = true;
            }
            else
            {
                publishButton.interactable = false;
            }
        }
 
    }

    public void isMarkerAR(bool value)
    {
        if (value)
        {
            editData.isMarker = true;
        }
    }

    public void isMarkerlessAR(bool value)
    {
        Debug.Log("MARKERLESSAR");
        if (value)
        {
            editData.isMarker = false;
        }
    }

    public void isPublicExp(bool value)
    {
        Debug.Log(value + "isPublicExp");
        if (value)
        {
            editData.isPublic = true;
        }
    }

    public void isPrivateExp(bool value)
    {
        Debug.Log(value + "isPrivateExp");
        if (value)
        {
            editData.isPublic = false;
        }
    }

}
