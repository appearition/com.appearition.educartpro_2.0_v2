﻿using UnityEngine;
using UnityEngine.UI;

namespace Appearition.Example
{
    public class ArDemoUIHandler : MonoBehaviour
    {
        //References
        public Button scanAgainButton;
        public Text scanAgainButtonText;

        //Internal Variables
        public string scanAgainButtonIdleText = "Scan Again";
        public string scanAgainScanningText = "Scanning..";

        void Start()
        {
            AppearitionArHandler.OnScanStateChanged += AppearitionArHandlerOnScanStateChanged;
        }

        void OnDestroy()
        {
            AppearitionArHandler.OnScanStateChanged -= AppearitionArHandlerOnScanStateChanged;
        }

        private void AppearitionArHandlerOnScanStateChanged(bool isScanning)
        {
            if (scanAgainButtonText != null)
                scanAgainButtonText.text = isScanning ? scanAgainScanningText : scanAgainButtonIdleText;
            if (scanAgainButton != null)
                scanAgainButton.interactable = !isScanning;
        }

        /// <summary>
        /// Called by the Scan Again UI button. Turns on the Ar Provider's recognition to be scanning.
        /// The main reason why it shouldn't be constantly scanning is because it is a good practice to have a bit of control over the requests sent to Vuforia. There is no need to be scanning a target once it's either already being tracked, or when the user isn't even trying to scan.
        /// </summary>
        public void OnScanAgainButtonPressed()
        {
            AppearitionArHandler.Instance.ChangeArProviderScanningState(true);
        }

        /// <summary>
        /// Called by the UI toggle. Toggles the Ar Provider's active state: turn on or turn off the Ar Provider as a whole.
        /// It is recommended to have the Ar Provider off while you are expecting your users to not be able to scan; AR takes a lot of performance, heats up the device and consumes battery quite a fair bit.
        /// </summary>
        /// <param name="state"></param>
        public void OnToggleArProviderStatePressed(bool state)
        {
            AppearitionArHandler.Instance.ChangeArProviderActiveState(state);
        }
    }
}