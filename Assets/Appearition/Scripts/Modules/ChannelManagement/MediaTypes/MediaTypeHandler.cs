﻿// -----------------------------------------------------------------------
// Company:"Appearition Pty Ltd"
// File: MediaTypeHandler.cs
// Copyright (c) 2019. All rights reserved.
// -----------------------------------------------------------------------

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Appearition.Common.ListExtensions;
using System;
using System.Linq;
using System.Reflection;
using Appearition.ChannelManagement;

namespace Appearition.Common
{
    /// <inheritdoc />
    /// <summary>
    /// Static class in charge of containing any definition regarding MediaType, as well as providing utilities to make MediaTypes easier to work with.
    /// </summary>
    public class MediaTypeHandler : BaseHandler
    {
        #region ApiHandler Implementation

        public static string ModuleVersion
        {
            get { return "1.0.0"; }
        }

        #endregion

        static List<MediaType> _activeMediaTypes;

        /// <summary>
        /// Contains all the MediaTypes loaded and in use.
        /// </summary>
        /// <value>The active media types.</value>
        public static List<MediaType> ActiveMediaTypes
        {
            get
            {
                if (_activeMediaTypes == null)
                {
                    _activeMediaTypes = new List<MediaType>();
                    GetActiveMediaTypes(null);
                }
                return _activeMediaTypes;
            }
        }
        
        /// <summary>
        /// Gets all the MediaTypes currently loaded instantly (if any), or refresh them if either required/requested.
        /// </summary>
        /// <param name="onMediaTypesLoaded"></param>
        /// <param name="forceRefresh"></param>
        /// <returns></returns>
        public static List<MediaType> GetActiveMediaTypes (Action<List<MediaType>> onMediaTypesLoaded, bool forceRefresh = false)
        {
            if (!forceRefresh && _activeMediaTypes != null)
            {
                if (onMediaTypesLoaded != null)
                    onMediaTypesLoaded(_activeMediaTypes);
                return _activeMediaTypes;
            }

            AppearitionGate.Instance.StartCoroutine(GetActiveMediaTypesProcess(onMediaTypesLoaded, forceRefresh));
            return null;
        }

        /// <summary>
        /// Gets all the loaded MediaTypes, or refresh them if required/requested.
        /// </summary>
        /// <param name="onMediaTypesLoaded"></param>
        /// <param name="forceRefresh"></param>
        /// <returns></returns>
        public static IEnumerator GetActiveMediaTypesProcess(Action<List<MediaType>> onMediaTypesLoaded = null, bool forceRefresh = false)
        {
            if (!forceRefresh && _activeMediaTypes != null)
            {
                if (onMediaTypesLoaded != null)
                    onMediaTypesLoaded(_activeMediaTypes);
                yield break;
            }

            yield return ChannelHandler.GetMediaTypesFromEmsProcess(success =>
            {
                _activeMediaTypes = new List<MediaType>(success);
                if (onMediaTypesLoaded != null)
                    onMediaTypesLoaded.Invoke(_activeMediaTypes);
            }, emsError => onMediaTypesLoaded?.Invoke(_activeMediaTypes));
        }

//        /// <summary>
//        /// Updates the list of all the MediaTypes that can be used 
//        /// </summary>
//        [System.Obsolete]
//        public static void RefreshMediaTypes(Action<List<MediaType>> callback = null)
//        {
//            //First, clean the MediaTypes and reset the container.
//            if (_activeMediaTypes != null)
//                _activeMediaTypes.Clear();
//            else
//                _activeMediaTypes = new List<MediaType>();

//            //Fetch the ones from internet if there's internet access!
////			GetMediaTypesFromEMS (_activeMediaTypes, (List<MediaType> obj) => {
////				//Failed to get from the EMS? Load them locally from the assembly.
////				if (obj == null || obj.Count == 0) {

//            ////Fetch all the types media types .
//            //Type[] allTypesOfMediaTypes = (from tmpType in Assembly.GetExecutingAssembly().GetTypes()
//            //    where tmpType.BaseType != null && tmpType.BaseType == typeof(MediaType)
//            //    select tmpType).ToArray();


//            ////Create instances for each type, and store them.
//            //if (allTypesOfMediaTypes.Length > 0)
//            //{
//            //    for (int i = 0; i < allTypesOfMediaTypes.Length; i++)
//            //    {
//            //        MediaType tmpMediaType = (MediaType) Activator.CreateInstance(allTypesOfMediaTypes[i]);

//            //        //Debug.Log (string.Format ("added the {0} mediatype to the handler!", tmpMediaType.displayName));
//            //        _activeMediaTypes.Add(tmpMediaType);
//            //    }
//            //}
//            //else
//            //    AppearitionLogger.LogError("No MediaTypes were found.");

//            if (callback != null)
//                callback(_activeMediaTypes);
//        }


        #region Queries

        #region Find MediaType

        /// <summary>
        /// Returns a list containing the display names of all the active MediaTypes.
        /// </summary>
        /// <returns>The all media types by display name.</returns>
        public static List<string> GetAllMediaTypesByDisplayName()
        {
            return (from tmpMediaType in ActiveMediaTypes
                select tmpMediaType.DisplayName).ToList();
        }

        /// <summary>
        /// Returns a list containing the names (as on the EMS) of all the active MediaTypes.
        /// </summary>
        /// <returns>The all media types by name as on EM.</returns>
        public static List<string> GetAllMediaTypesByNameAsOnEms()
        {
            return (from tmpMediaType in ActiveMediaTypes
                select tmpMediaType.DisplayName).ToList();
        }

        /// <summary>
        /// Fetches the MediaType corresponding to a given display name.
        /// </summary>
        /// <returns>The media type from display name.</returns>
        /// <param name="displayName">Display name.</param>
        public static MediaType FindMediaTypeFromDisplayName(string displayName)
        {
            return ActiveMediaTypes.FirstOrDefault(o => o.DisplayName == displayName);
        }

        /// <summary>
        /// Fetches the MediaType corresponding to a given name as displayed on the EMS.
        /// </summary>
        /// <returns>The media type from EMS name.</returns>
        /// <param name="nameAsOnEms">Name as on EM.</param>
        public static MediaType FindMediaTypeFromEmsName(string nameAsOnEms)
        {
            return ActiveMediaTypes.FirstOrDefault(o => o.Name == nameAsOnEms);
        }

        /// <summary>
        /// From a given extension (which includes the dot, ie ".png"), fetches all MediaTypes that allow that extension.
        /// </summary>
        /// <returns>The media types from extension.</returns>
        /// <param name="extension">Extension.</param>
        public static List<MediaType> FindMediaTypesFromExtension(string extension)
        {
            return (from tmpType in ActiveMediaTypes
                from tmpFileExtension in tmpType.FileValidations
                where tmpFileExtension.ContainsExtension(extension)
                select tmpType).ToList();
        }

        #endregion

        #region Extensions

        /// <summary>
        /// Given a name, whether it's the name as on the EMS or a display name, fetches all the extensions for this MediaType.
        /// By default, the given name is the display name.
        /// </summary>
        /// <returns>Lists all extensions from this mediatype.</returns>
        /// <param name="mediaTypeGivenName">Media type given name.</param>
        /// <param name="isGivenStringDisplayName">If set to <c>true</c> is given string display name.</param>
        public static List<string> FindAllExtensionsFromMediaType(string mediaTypeGivenName, bool isGivenStringDisplayName = true)
        {
            //Handle for display name
            if (isGivenStringDisplayName)
                return FindAllExtensionsFromMediaType(FindMediaTypeFromDisplayName(mediaTypeGivenName));

            //Handle for EMS name
            return FindAllExtensionsFromMediaType(FindMediaTypeFromEmsName(mediaTypeGivenName));
        }

        /// <summary>
        /// For a given MediaType, fetches all the associated extensions, with the dot before the extension.
        /// Can provide a list which will be filled with those new values.
        /// </summary>
        /// <returns>The all extensions from media type.</returns>
        /// <param name="mediaType">Media type.</param>
        /// <param name="allExtensionsContainer">All extensions container.</param>
        public static List<string> FindAllExtensionsFromMediaType(MediaType mediaType, List<string> allExtensionsContainer = null)
        {
            if (mediaType == null || mediaType.FileValidations == null)
                return new List<string>();

            if (allExtensionsContainer == null)
                allExtensionsContainer = new List<string>();
            else
                allExtensionsContainer.Clear();

            for (int i = 0; i < mediaType.FileValidations.Count; i++)
            {
                string[] tmpExtensions = mediaType.FileValidations[i].GetFileExtensions(true);
                if (tmpExtensions != null && tmpExtensions.Length > 0)
                    allExtensionsContainer.AddRange(tmpExtensions);
            }

            //Remove duplicates

            allExtensionsContainer.HiziRemoveDuplicates();
            return allExtensionsContainer;
        }

        #endregion

        #region Mime Types

        /// <summary>
        /// From a given MediaType and an extension, finds the MimeType as on the EMS.
        /// </summary>
        /// <returns>The MIME type from extension.</returns>
        /// <param name="mediaType">Media type.</param>
        /// <param name="extension">Extension.</param>
        public static string FindMimeTypeFromExtension(MediaType mediaType, string extension)
        {
            if (mediaType == null)
                return "";

            return mediaType.GetMimeTypeForGivenExtension(extension);
        }

        #endregion

        #endregion
    }
}